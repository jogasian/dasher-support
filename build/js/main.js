(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/Users/jasonogasian/projects/dasher-tools/public/support/node_modules/process/browser.js":[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/App.js":[function(require,module,exports){
(function (process){
'use strict';

var NavWrapper = require('./components/NavWrapper');

sessionStorage.env = "stage";
sessionStorage.api = "stageclientapi/";
sessionStorage.menuapi = process.env.menuapi;
sessionStorage.menuviewapi = process.env.menuviewapi;

var e = sessionStorage.env ? sessionStorage.env : 'prod';
var u = sessionStorage.user ? JSON.parse(sessionStorage.user) : {};
window.App = {
	environment: e,
	user: u,

	api: function api() {
		switch (window.App.environment) {
			case 'dev2':
				return 'dev2clientapi/';
			case 'stage':
				return 'stageclientapi/';
			case 'local':
				return 'localclientapi/';
			default:
				return 'prodclientapi/';
		}
	},

	menuapi: function menuapi() {
		switch (window.App.environment) {
			case 'dev2':
				return 'menudev2api/';
			case 'stage':
				return 'menustageapi/';
			case 'local':
				return 'menulocalapi/';
			default:
				return 'menuprodapi/';
		}
	},

	utcToLocal: function utcToLocal(utcTime) {
		if (!utcTime.match(/Z$/)) {
			utcTime = utcTime + ' Z';
		}
		var d = new Date(utcTime);
		return d.toString().replace(/\sGMT-\d\d\d\d/, '');
	},

	auth: function auth() {
		return window.App.user.dasher_user_id !== undefined;
	},

	notify: function notify() {
		// Overload this function
	},

	warn: function warn() {
		// Overload this function
	},

	closeMessage: function closeMessage() {
		// Overload this funciton
	}
};

React.render(React.createElement(NavWrapper, null), document.getElementById('app'));

}).call(this,require('_process'))

},{"./components/NavWrapper":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/NavWrapper.js","_process":"/Users/jasonogasian/projects/dasher-tools/public/support/node_modules/process/browser.js"}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Dashboard.js":[function(require,module,exports){
'use strict';

var Logout = require('./Logout');
var Dashboard = require('./Dashboard');

var Dashboard = React.createClass({
  displayName: 'Dashboard',

  getInitialState: function getInitialState() {
    return {
      appState: 'login'
    };
  },

  render: function render() {
    return React.createElement(
      'div',
      { className: 'wrapper' },
      React.createElement(Logout, null)
    );
  }
});

module.exports = Dashboard;

},{"./Dashboard":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Dashboard.js","./Logout":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Logout.js"}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Header.js":[function(require,module,exports){
'use strict';

var Header = React.createClass({
  displayName: 'Header',

  getInitialState: function getInitialState() {
    return {
      mobileActive: false
    };
  },

  renderOption: function renderOption(option) {
    var that = this;
    return React.createElement(
      'li',
      {
        onClick: function onClick() {
          that.props.changeView(option);
          that.setState({ mobileActive: false });
        },
        key: option },
      option.toUpperCase()
    );
  },

  render: function render() {
    var that = this;
    var options = [];
    options = this.props.menuOptions.map(this.renderOption);

    return React.createElement(
      'header',
      { className: that.state.mobileActive ? 'mobile-active' : '' },
      React.createElement(
        'div',
        { className: 'menu-link', onClick: function onClick() {
            that.setState({ mobileActive: !that.state.mobileActive });
          } },
        'MENU'
      ),
      React.createElement(
        'ul',
        null,
        options
      ),
      React.createElement(
        'div',
        { className: 'env-right' },
        '[',
        App.environment,
        ']'
      )
    );
  }
});

module.exports = Header;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Logout.js":[function(require,module,exports){
"use strict";

var Logout = React.createClass({
		displayName: "Logout",


		componentWillMount: function componentWillMount() {
				this.ref = new Firebase('https://dasher-app-staging.firebaseio.com');
		},

		unauthenticate: function unauthenticate() {
				this.ref.unauth();
				sessionStorage.clear();
				window.location = "/";
		},

		render: function render() {
				return React.createElement(
						"div",
						{ className: "logout-wrapper" },
						React.createElement(
								"button",
								{ onClick: this.unauthenticate },
								"Log Out"
						)
				);
		}
});

module.exports = Logout;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Messaging.js":[function(require,module,exports){
'use strict';

var Messaging = React.createClass({
  displayName: 'Messaging',

  getInitialState: function getInitialState() {
    return {
      active: false,
      type: 'notify',
      message: ''
    };
  },

  componentWillMount: function componentWillMount() {
    App.notify = this.notify;
    App.warn = this.warn;
    App.closeMessage = this.close;
  },

  notify: function notify(msg) {
    this.setState({
      active: true,
      message: msg,
      type: 'notify'
    });
  },

  warn: function warn(msg) {
    this.setState({
      active: true,
      message: msg,
      type: 'warn'
    });
  },

  close: function close() {
    this.setState({
      active: false
    });
  },

  ignore: function ignore(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  },

  render: function render() {
    var className = this.state.active ? '' : ' invisible';
    return React.createElement(
      'div',
      { onClick: this.close, className: "messaging " + this.state.type + className },
      React.createElement(
        'div',
        { className: 'message', onClick: this.ignore },
        this.state.message,
        React.createElement(
          'div',
          { className: 'close', onClick: this.close },
          'x'
        )
      )
    );
  }
});

module.exports = Messaging;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/NavWrapper.js":[function(require,module,exports){
'use strict';

var Header = require('./Header');
var Messaging = require('./Messaging');
var Dashboard = require('./Dashboard');
var Refund = require('./Refund');
var Resolution = require('./Resolution');
var Users = require('./Users');
var Promotion = require('./Promotion');

var NavWrapper = React.createClass({
	displayName: 'NavWrapper',

	getInitialState: function getInitialState() {
		return {
			appState: 'dashboard'
		};
	},

	menuOptions: ['dashboard', 'users', 'refunds', 'resolution', 'promotions'],

	showView: function showView(view) {
		this.setState({ appState: view });
	},

	renderView: function renderView() {
		var view;
		switch (this.state.appState) {
			case 'dashboard':
				view = React.createElement(Dashboard, null);
				break;
			case 'users':
				view = React.createElement(Users, null);
				break;
			case 'refunds':
				view = React.createElement(Refund, null);
				break;
			case 'resolution':
				view = React.createElement(Resolution, null);
				break;
			case 'promotions':
				view = React.createElement(Promotion, null);
				break;
		}
		return view;
	},

	render: function render() {
		var header = React.createElement(
			'div',
			null,
			React.createElement(Header, { menuOptions: this.menuOptions, changeView: this.showView }),
			React.createElement(Messaging, null)
		);
		return React.createElement(
			'div',
			{ className: 'wrapper' },
			header,
			this.renderView()
		);
	}
});

module.exports = NavWrapper;

},{"./Dashboard":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Dashboard.js","./Header":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Header.js","./Messaging":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Messaging.js","./Promotion":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Promotion.js","./Refund":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Refund.js","./Resolution":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Resolution.js","./Users":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Users.js"}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Promotion.js":[function(require,module,exports){
'use strict';

var Promotion = React.createClass({
  displayName: 'Promotion',


  basePath: function basePath() {
    return App.api() + 'v2/';
  },

  componentWillUpdate: function componentWillUpdate() {
    App.closeMessage();
  },

  getInitialState: function getInitialState() {
    return {
      action: 'givePromo', // Match function name
      promoCode: '',
      userId: encodeURI(App.user.dasher_user_id),
      beneficiaryUserId: '',
      benefitValue: ''
    };
  },

  //
  // Network fetch functions
  //
  fetch: function fetch(apiPath) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'POST',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Promotion succeeded');
      $('section.promotions input').val('');
    }
    console.log(data);
  },

  fetchError: function fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Promotion failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Promotion failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Promotion request failed.");
      console.log(err);
    }
  },

  //
  // Form update funcitons
  //
  updatePromoCode: function updatePromoCode(evt) {
    this.setState({
      promoCode: evt.target.value
    });
  },

  updateUserId: function updateUserId(evt) {
    this.setState({
      beneficiaryUserId: encodeURI(evt.target.value)
    });
  },

  updateBenefit: function updateBenefit(evt) {
    this.setState({
      benefitValue: evt.target.value
    });
  },

  setAction: function setAction(evt) {
    var actionFunciton = evt.target.value;
    this.setState({
      action: actionFunciton
    });
  },

  issuePromo: function issuePromo() {
    this[this.state.action]();
  },

  render: function render() {
    var promoCash = this.state.action === 'givePromoCash';

    return React.createElement(
      'section',
      { className: 'promotions' },
      React.createElement(
        'h1',
        null,
        'Give a Promotion'
      ),
      React.createElement(
        'p',
        null,
        'You can give users promotions using the forms below'
      ),
      React.createElement(
        'div',
        { className: 'form' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Choose a refund type:'
          ),
          React.createElement(
            'div',
            { className: 'select' },
            React.createElement(
              'select',
              { onChange: this.setAction },
              React.createElement(
                'option',
                { value: 'givePromo' },
                'Give user promo code'
              ),
              React.createElement(
                'option',
                { value: 'givePromoCash' },
                'Give user promo cash'
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: promoCash ? 'hidden' : '' },
          React.createElement(
            'label',
            null,
            'Promo Code: ',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          React.createElement('input', { type: 'text',
            name: 'promo-code',
            onChange: this.updatePromoCode })
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'User ID:',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          React.createElement('input', { type: 'text',
            name: 'user-id',
            onChange: this.updateUserId })
        ),
        React.createElement(
          'div',
          { className: promoCash ? '' : 'hidden' },
          React.createElement(
            'label',
            null,
            'Benefit Value: ',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          React.createElement('input', { type: 'text',
            name: 'benefit-value',
            onChange: this.updateBenefit })
        ),
        React.createElement(
          'div',
          { className: 'button', onClick: this.issuePromo },
          'Issue Promo'
        )
      )
    );
  },

  //
  // Available actions
  //
  givePromo: function givePromo() {
    if (!this.state.promoCode) {
      return App.warn('You must ender a valid promo code.');
    }
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must ender a User ID.');
    }

    var path = 'promotion/couponCode/' + this.state.promoCode + '/apply?dasherUserId=' + this.state.userId + '&beneficiaryUserId=' + this.state.beneficiaryUserId;
    this.fetch(path);
  },

  givePromoCash: function givePromoCash() {
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must ender a user ID.');
    }
    if (!this.state.benefitValue) {
      return App.warn('You must ender a benefit value.');
    }

    var path = 'promotion/couponCode/DYNAMIC_CASH/apply?dasherUserId=' + this.state.userId + '&benefitValue=' + this.state.benefitValue + '&beneficiaryUserId=' + this.state.beneficiaryUserId;
    this.fetch(path);
  }
});

module.exports = Promotion;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Refund.js":[function(require,module,exports){
'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Refund = React.createClass({
  displayName: 'Refund',

  basePath: function basePath() {
    return App.api() + 'v1/run/';
  },

  componentWillUpdate: function componentWillUpdate() {
    // App.closeMessage();
  },

  getInitialState: function getInitialState() {
    return this.resetState();
  },

  resetState: function resetState() {
    return {
      action: 'OnlyRefundBuyer', // Match function name
      runId: '',
      dasherId: App.user.dasher_user_id, // Default to Admin ID
      orderId: '',
      runnerMsg: '',
      buyerMsg: '',
      description: '',
      refundAmount: 0,
      partial: false
    };
  },

  //
  // Network fetch functions
  //
  fetch: function fetch(apiPath) {
    var data = this.buildFetchData();
    if (data === null) {
      return;
    }
    console.log('Fetch: ', this.basePath() + apiPath);
    console.log(data);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'PUT',
      contentType: 'application/json;charset=utf-8',
      // dataType: 'json',
      data: JSON.stringify(data),
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Refund succeeded');
      $("section.refunds select")[0].selectedIndex = 0;
      $('section.refunds input').val('');
      this.setState(this.resetState());
    }
    console.log(data);
  },

  fetchError: function fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Refund failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Refund failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Refund request failed.");
      console.log(err);
    }
  },

  buildFetchData: function buildFetchData() {
    if (!this.state.runId) {
      App.warn('You must enter a Run ID.');
      return null;
    }
    if (!this.state.orderId) {
      App.warn('You must enter an Order ID.');
      return null;
    }
    if (!this.state.description) {
      App.warn('You must enter a description for internal Joyrun use.');
      return null;
    }

    var data = {
      action_taken: this.state.action.replace(/^Partial/, ''),
      dasher_user_id: this.state.dasherId,
      description: this.state.description,
      params: {
        runner_message: this.state.runnerMsg,
        buyer_message: this.state.buyerMsg
      }
    };
    if (this.state.partial) {
      data.params.refund_amount = Number(this.state.refundAmount);
      if (!data.params.refund_amount) {
        App.warn('You must enter a refund amount.');
        return null;
      }
    }
    return data;
  },

  //
  // Form update funcitons
  //
  updateRunId: function updateRunId(evt) {
    this.setState({
      runId: evt.target.value
    });
  },

  updateOrderId: function updateOrderId(evt) {
    this.setState({
      orderId: evt.target.value
    });
  },

  updateDesc: function updateDesc(evt) {
    this.setState({
      description: evt.target.value
    });
  },

  updateRunMsg: function updateRunMsg(evt) {
    this.setState({
      runnerMsg: evt.target.value
    });
  },

  updateBuyMsg: function updateBuyMsg(evt) {
    this.setState({
      buyerMsg: evt.target.value
    });
  },

  updateRefundAmt: function updateRefundAmt(evt) {
    this.setState({
      refundAmount: evt.target.value
    });
  },

  setAction: function setAction(evt) {
    var actionFunciton = evt.target.value;
    var partial = actionFunciton.match('Partial') != null;
    this.setState({
      action: actionFunciton,
      partial: partial
    });
  },

  issueRefund: function issueRefund() {
    this[this.state.action]();
  },

  render: function render() {
    var _React$createElement;

    return React.createElement(
      'section',
      { className: 'refunds' },
      React.createElement(
        'h1',
        null,
        'Issue a refund'
      ),
      React.createElement(
        'p',
        null,
        'You can issue full or partial refunds using the form below'
      ),
      React.createElement(
        'div',
        { className: 'form' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Choose a refund type:'
          ),
          React.createElement(
            'div',
            { className: 'select' },
            React.createElement(
              'select',
              { onChange: this.setAction },
              React.createElement(
                'option',
                { value: 'OnlyRefundBuyer' },
                'Full refund to buyer'
              ),
              React.createElement(
                'option',
                { value: 'RefundBuyerFromRunner' },
                'Full refund to buyer from runner'
              ),
              React.createElement(
                'option',
                { value: 'PartialOnlyRefundBuyer' },
                'Partial refund to buyer'
              ),
              React.createElement(
                'option',
                { value: 'PartialRefundBuyerFromRunner' },
                'Partial refund to buyer from runner'
              )
            )
          )
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Run ID:',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          React.createElement('input', { type: 'text', name: 'run-id', onChange: this.updateRunId })
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Order ID:',
            React.createElement(
              'span',
              { className: 'req' },
              ' * '
            )
          ),
          React.createElement('input', { type: 'text',
            name: 'order-id',
            onChange: this.updateOrderId })
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Refund Description(',
            React.createElement(
              'i',
              null,
              'internal use only'
            ),
            '):',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          React.createElement('input', { type: 'text', name: 'description', onChange: this.updateDesc })
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Notification Message to Runner:'
          ),
          React.createElement('input', { type: 'text',
            name: 'runner-message',
            onChange: this.updateRunMsg })
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Notification Message to Buyer:'
          ),
          React.createElement('input', { type: 'text', name: 'buyer-message', onChange: this.updateBuyMsg })
        ),
        React.createElement(
          'div',
          { className: this.state.partial ? '' : 'hidden' },
          React.createElement(
            'label',
            null,
            'Refund Amount:',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          '$',
          React.createElement('input', (_React$createElement = {
            type: 'text',
            name: 'refund-amount'
          }, _defineProperty(_React$createElement, 'type', 'number'), _defineProperty(_React$createElement, 'min', '0'), _defineProperty(_React$createElement, 'step', '0.01'), _defineProperty(_React$createElement, 'onChange', this.updateRefundAmt), _React$createElement))
        ),
        React.createElement(
          'div',
          { className: 'button', onClick: this.issueRefund },
          'Refund'
        )
      )
    );
  },

  //
  // Available actions (function name matches request param excluding "Partial")
  //
  OnlyRefundBuyer: function OnlyRefundBuyer() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },

  RefundBuyerFromRunner: function RefundBuyerFromRunner() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },

  PartialOnlyRefundBuyer: function PartialOnlyRefundBuyer() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },

  PartialRefundBuyerFromRunner: function PartialRefundBuyerFromRunner() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  }
});

module.exports = Refund;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Resolution.js":[function(require,module,exports){
'use strict';

var Resolution = React.createClass({
  displayName: 'Resolution',

  basePath: function basePath() {
    return App.api() + 'v1/run/';
  },

  componentWillUpdate: function componentWillUpdate() {
    App.closeMessage();
  },

  getInitialState: function getInitialState() {
    return this.resetState();
  },

  resetState: function resetState() {
    return {
      action: 'ResolveProblem', // Match function name
      runId: '',
      dasherId: App.user.dasher_user_id || '', // Default to Admin ID
      orderId: '',
      description: ''
    };
  },

  //
  // Network fetch functions
  //
  fetch: function fetch(apiPath) {
    var data = this.buildFetchData();
    if (data === null) {
      return;
    }
    console.log('Fetch: ', this.basePath() + apiPath);
    console.log(data);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'PUT',
      contentType: 'application/json;charset=utf-8',
      // dataType: 'json',
      data: JSON.stringify(data),
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function fetchSuccess(data) {
    if (data && data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Resolution succeeded');
      $('section.resolutions input').val('');
    }
    console.log(data);
  },

  fetchError: function fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Resolution failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Resolution failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Resolution request failed.");
      console.log(err);
    }
  },

  buildFetchData: function buildFetchData() {
    if (!this.state.runId) {
      App.warn('You must enter a Run ID.');
      return null;
    }
    if (this.state.action !== 'ReleaseHold' && !this.state.orderId) {
      App.warn('You must enter a Order ID.');
      return null;
    }
    if (!this.state.description) {
      App.warn('You must enter a Description for internal Joyrun use.');
      return null;
    }

    var data = {
      action_taken: this.state.action,
      dasher_user_id: this.state.dasherId,
      description: this.state.description
    };

    if (this.state.action === 'ReleaseHold') {
      data.params = {
        runner_message: this.state.runnerMsg,
        buyer_message: this.state.buyerMsg
      };
    }
    return data;
  },

  //
  // Form update funcitons
  //
  updateRunId: function updateRunId(evt) {
    this.setState({
      runId: evt.target.value
    });
  },

  updateOrderId: function updateOrderId(evt) {
    this.setState({
      orderId: evt.target.value
    });
  },

  updateDesc: function updateDesc(evt) {
    this.setState({
      description: evt.target.value
    });
  },

  updateRunMsg: function updateRunMsg(evt) {
    this.setState({
      runnerMsg: evt.target.value
    });
  },

  updateBuyMsg: function updateBuyMsg(evt) {
    this.setState({
      buyerMsg: evt.target.value
    });
  },

  setAction: function setAction(evt) {
    var actionFunciton = evt.target.value;
    this.setState({
      action: actionFunciton
    });

    // Use the admin ID
    // if (actionFunciton !== 'ReleaseHold') {
    //   this.setState({
    //     dasherId: App.user.dasher_user_id
    //   });
    // }
  },

  resolveIssue: function resolveIssue() {
    this[this.state.action]();
  },

  render: function render() {
    var releaseHold = this.state.action === 'ReleaseHold';

    return React.createElement(
      'section',
      { className: 'resolutions' },
      React.createElement(
        'h1',
        null,
        'Resolve a Problem'
      ),
      React.createElement(
        'p',
        null,
        'You can mark a problem as resolved or release a hold using the form below'
      ),
      React.createElement(
        'div',
        { className: 'form' },
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Choose a resolution type:'
          ),
          React.createElement(
            'div',
            { className: 'select' },
            React.createElement(
              'select',
              { onChange: this.setAction },
              React.createElement(
                'option',
                { value: 'ResolveProblem' },
                'Resolve a Problem'
              ),
              React.createElement(
                'option',
                { value: 'ReleaseHold' },
                'Remove Hold'
              )
            )
          )
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Run ID: ',
            React.createElement(
              'span',
              { className: 'req' },
              ' * '
            )
          ),
          React.createElement('input', { type: 'text',
            name: 'run-id',
            onChange: this.updateRunId })
        ),
        React.createElement(
          'div',
          { className: releaseHold ? 'hidden' : '' },
          React.createElement(
            'label',
            null,
            'Order ID: ',
            React.createElement(
              'span',
              { className: 'req' },
              ' * '
            )
          ),
          React.createElement('input', { type: 'text',
            name: 'order-id',
            onChange: this.updateOrderId })
        ),
        React.createElement(
          'div',
          null,
          React.createElement(
            'label',
            null,
            'Resolution Description(',
            React.createElement(
              'i',
              null,
              'internal use only'
            ),
            '):',
            React.createElement(
              'span',
              { className: 'req' },
              '*'
            )
          ),
          React.createElement('input', { type: 'text', name: 'description', onChange: this.updateDesc })
        ),
        React.createElement(
          'div',
          { className: releaseHold ? '' : 'hidden' },
          React.createElement(
            'label',
            null,
            'Notification Message to Runner:'
          ),
          React.createElement('input', { type: 'text',
            name: 'runner-message',
            onChange: this.updateRunMsg })
        ),
        React.createElement(
          'div',
          { className: releaseHold ? '' : 'hidden' },
          React.createElement(
            'label',
            null,
            'Notification Message to Buyer:'
          ),
          React.createElement('input', { type: 'text', name: 'buyer-message', onChange: this.updateBuyMsg })
        ),
        React.createElement(
          'div',
          { className: 'button', onClick: this.resolveIssue },
          'Resolve Issue'
        )
      )
    );
  },

  //
  // Available actions
  //
  ResolveProblem: function ResolveProblem() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },

  ReleaseHold: function ReleaseHold() {
    var path = this.state.runId + '/action';
    this.fetch(path);
  }
});

module.exports = Resolution;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/UserOrders.js":[function(require,module,exports){
'use strict';

var UserOrders = React.createClass({
  displayName: 'UserOrders',


  basePath: function basePath() {
    return App.api() + 'v1/';
  },

  componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
    this.getOrders(nextProps.userId);
  },

  getInitialState: function getInitialState() {
    return {
      orders: []
    };
  },

  //
  // Network fetch functions
  //
  fetch: function fetch(apiPath, userId) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      this.setState({ orders: data });
    }
    console.log(data);
  },

  fetchError: function fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Order Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Order Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Order search failed.");
      console.log(err);
    }
  },

  renderOrder: function renderOrder(order) {
    var that = this;
    return React.createElement(
      'tr',
      { key: order.order_id },
      React.createElement(
        'td',
        null,
        order.run_id
      ),
      React.createElement(
        'td',
        null,
        order.order_id
      ),
      React.createElement(
        'td',
        null,
        order.restaurant_name
      ),
      React.createElement(
        'td',
        null,
        App.utcToLocal(order.order_placed)
      ),
      React.createElement(
        'td',
        null,
        order.order_status
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.order_value.toFixed(2)
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.gas_money.toFixed(2)
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.service_fees.toFixed(2)
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.tax_total.toFixed(2)
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.refund_amount.toFixed(2)
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.total__price_discounted.toFixed(2)
      )
    );
  },

  render: function render() {
    var orderRows = this.state.orders.map(this.renderOrder);

    if (orderRows.length && this.props.userId) {
      return React.createElement(
        'div',
        { className: 'user-orders' },
        React.createElement(
          'h3',
          null,
          this.props.displayName,
          ' Orders:'
        ),
        React.createElement(
          'table',
          { cellSpacing: '0' },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'Run ID'
              ),
              React.createElement(
                'th',
                null,
                'Order ID'
              ),
              React.createElement(
                'th',
                null,
                'Restaurant'
              ),
              React.createElement(
                'th',
                null,
                'Order Placed'
              ),
              React.createElement(
                'th',
                null,
                'Status'
              ),
              React.createElement(
                'th',
                null,
                'Order Value'
              ),
              React.createElement(
                'th',
                null,
                'Gas Money'
              ),
              React.createElement(
                'th',
                null,
                'Service Fees'
              ),
              React.createElement(
                'th',
                null,
                'Tax'
              ),
              React.createElement(
                'th',
                null,
                'Refund Amount'
              ),
              React.createElement(
                'th',
                null,
                'Total Price'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            orderRows
          )
        )
      );
    } else if (this.props.userId) {
      return React.createElement(
        'i',
        null,
        'No Orders Found'
      );
    } else {
      return React.createElement('div', { className: 'user-orders' });
    }
  },

  getOrders: function getOrders(userId) {
    if (!userId) {
      return;
    }
    var path = 'support/orders';
    this.fetch(path, userId);
  }
});

module.exports = UserOrders;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/UserRuns.js":[function(require,module,exports){
'use strict';

var UserRuns = React.createClass({
  displayName: 'UserRuns',


  basePath: function basePath() {
    return App.api() + 'v1/';
  },

  componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
    this.getOrders(nextProps.userId);
  },

  getInitialState: function getInitialState() {
    return {
      orders: []
    };
  },

  //
  // Network fetch functions
  //
  fetch: function fetch(apiPath, userId) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      this.setState({ orders: data });
    }
    console.log(data);
  },

  fetchError: function fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Order Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Order Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Order search failed.");
      console.log(err);
    }
  },

  renderOrder: function renderOrder(order) {
    var that = this;
    return React.createElement(
      'tr',
      { key: order.run_id },
      React.createElement(
        'td',
        null,
        order.run_id
      ),
      React.createElement(
        'td',
        null,
        App.utcToLocal(order.created_date)
      ),
      React.createElement(
        'td',
        null,
        order.run_state
      ),
      React.createElement(
        'td',
        null,
        order.run_status
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.earnings.toFixed(2)
      ),
      React.createElement(
        'td',
        null,
        '$',
        order.reimburse_amount.toFixed(2)
      )
    );
  },

  render: function render() {
    var orderRows = this.state.orders.map(this.renderOrder);

    if (orderRows.length && this.props.userId) {
      return React.createElement(
        'div',
        { className: 'user-orders' },
        React.createElement(
          'h3',
          null,
          this.props.displayName,
          ' Runs:'
        ),
        React.createElement(
          'table',
          { cellSpacing: '0' },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'Run ID'
              ),
              React.createElement(
                'th',
                null,
                'Created Date'
              ),
              React.createElement(
                'th',
                null,
                'Run State'
              ),
              React.createElement(
                'th',
                null,
                'Status'
              ),
              React.createElement(
                'th',
                null,
                'Earnings'
              ),
              React.createElement(
                'th',
                null,
                'Reimburse Value'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            orderRows
          )
        )
      );
    } else if (this.props.userId) {
      return React.createElement(
        'i',
        null,
        'No Runs Found'
      );
    } else {
      return React.createElement('div', { className: 'user-orders' });
    }
  },

  getOrders: function getOrders(userId) {
    if (!userId) {
      return;
    }
    var path = 'support/runs';
    this.fetch(path, userId);
  }
});

module.exports = UserRuns;

},{}],"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/Users.js":[function(require,module,exports){
'use strict';

var UserOrders = require('./UserOrders');
var UserRuns = require('./UserRuns');

var Users = React.createClass({
  displayName: 'Users',


  basePath: function basePath() {
    return App.api() + 'v1/';
  },

  componentWillUpdate: function componentWillUpdate() {
    App.closeMessage();
  },

  componentDidMount: function componentDidMount() {
    $("input:text").focus(function () {
      $(this).select();
    });
    $('.focus-me').focus();
  },

  getInitialState: function getInitialState() {
    return {
      firstName: '',
      lastName: '',
      users: []
    };
  },

  //
  // Network fetch functions
  //
  fetch: function fetch(apiPath) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('User search succeeded');
      this.setState({
        firstName: '', lastName: '',
        users: data
      });
      $("input:text").val('');
    }
    console.log(data);
  },

  fetchError: function fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("User search failed.");
      console.log(err);
    }
  },

  renderUser: function renderUser(user) {
    var that = this;
    var selected = '';
    if (this.state.orderUserId === user.dasher_user_id || this.state.runUserId === user.dasher_user_id) {
      selected = 'selected';
    }
    return React.createElement(
      'tr',
      { className: selected, key: user.dasher_user_id },
      React.createElement(
        'td',
        null,
        user.dasher_user_id
      ),
      React.createElement(
        'td',
        null,
        user.display_name
      ),
      React.createElement(
        'td',
        null,
        user.first_name
      ),
      React.createElement(
        'td',
        null,
        user.last_name
      ),
      React.createElement(
        'td',
        null,
        user.email_address || 'unknown'
      ),
      React.createElement(
        'td',
        null,
        user.phone_number || 'unknown'
      ),
      React.createElement(
        'td',
        null,
        user.region_name
      ),
      React.createElement(
        'td',
        null,
        user.secure_user_id
      ),
      React.createElement(
        'td',
        { className: 'link', onClick: function onClick() {
            that.setState({
              runUserId: user.secure_user_id,
              runUserDisplay: user.display_name,
              orderUserId: '',
              orderUserDisplay: ''
            });
          } },
        'Runs'
      ),
      React.createElement(
        'td',
        { className: 'link', onClick: function onClick() {
            that.setState({
              orderUserId: user.secure_user_id,
              orderUserDisplay: user.display_name,
              runUserId: '',
              runUserDisplay: ''
            });
          } },
        'Orders'
      )
    );
  },

  render: function render() {
    var userRows = this.state.users.map(this.renderUser);

    if (userRows.length) {
      var userTable = React.createElement(
        'div',
        null,
        React.createElement(
          'h3',
          null,
          'Found Users:'
        ),
        React.createElement(
          'table',
          { cellSpacing: '0' },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'User ID'
              ),
              React.createElement(
                'th',
                null,
                'Display Name'
              ),
              React.createElement(
                'th',
                null,
                'First Name'
              ),
              React.createElement(
                'th',
                null,
                'Last Name'
              ),
              React.createElement(
                'th',
                null,
                'Email Address'
              ),
              React.createElement(
                'th',
                null,
                'Phone Number'
              ),
              React.createElement(
                'th',
                null,
                'Region'
              ),
              React.createElement(
                'th',
                null,
                'Hashed User ID'
              ),
              React.createElement(
                'th',
                null,
                'Runs'
              ),
              React.createElement(
                'th',
                null,
                'Orders'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            userRows
          )
        )
      );
    }

    return React.createElement(
      'section',
      { className: 'user-search' },
      React.createElement(
        'h1',
        null,
        'Search for Users'
      ),
      React.createElement(
        'p',
        null,
        'Use this form to search for user details and find runs and orders for the given user'
      ),
      React.createElement(
        'div',
        { className: 'form' },
        React.createElement(
          'label',
          null,
          'First Name:'
        ),
        React.createElement('input', { onKeyUp: this.detectEnter, type: 'text', onChange: this.setFirstName, name: 'first-name', className: 'focus-me' }),
        React.createElement('br', null),
        React.createElement(
          'label',
          null,
          'Last Name:'
        ),
        React.createElement('input', { onKeyUp: this.detectEnter, type: 'text', onChange: this.setLastName, name: 'last-name' }),
        React.createElement('br', null),
        React.createElement(
          'div',
          { className: 'button', onClick: this.searchUsers },
          'Search User'
        )
      ),
      React.createElement('br', null),
      React.createElement('br', null),
      userTable,
      React.createElement('br', null),
      React.createElement('br', null),
      React.createElement(UserOrders, { userId: this.state.orderUserId, displayName: this.state.orderUserDisplay }),
      React.createElement(UserRuns, { userId: this.state.runUserId, displayName: this.state.runUserDisplay })
    );
  },

  setFirstName: function setFirstName(evt) {
    this.setState({ firstName: evt.target.value });
  },

  setLastName: function setLastName(evt) {
    this.setState({ lastName: evt.target.value });
  },

  detectEnter: function detectEnter(evt) {
    evt = evt || window.event;
    if (evt.keyCode === 13) {
      this.searchUsers();
      return false;
    }
  },

  searchUsers: function searchUsers() {
    if (!this.state.firstName && !this.state.lastName) {
      return App.warn('You must ender a first name and/or last name to search.');
    }

    var path = 'support/users?';

    if (this.state.firstName) {
      path += "fname=" + this.state.firstName;

      if (this.state.lastName) {
        path += '&' + ("lname=" + this.state.lastName);
      }
    } else if (this.state.lastName) {
      path += "lname=" + this.state.lastName;
    }

    this.fetch(path);

    // Hide the tables when searching a new user
    this.setState({
      orderUserId: '',
      orderUserDisplay: '',
      runUserId: '',
      runUserDisplay: ''
    });
  }
});

module.exports = Users;

},{"./UserOrders":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/UserOrders.js","./UserRuns":"/Users/jasonogasian/projects/dasher-tools/public/support/src/js/components/UserRuns.js"}]},{},["/Users/jasonogasian/projects/dasher-tools/public/support/src/js/App.js"])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvcHJvY2Vzcy9icm93c2VyLmpzIiwic3JjL2pzL0FwcC5qcyIsInNyYy9qcy9jb21wb25lbnRzL0Rhc2hib2FyZC5qcyIsInNyYy9qcy9jb21wb25lbnRzL0hlYWRlci5qcyIsInNyYy9qcy9jb21wb25lbnRzL0xvZ291dC5qcyIsInNyYy9qcy9jb21wb25lbnRzL01lc3NhZ2luZy5qcyIsInNyYy9qcy9jb21wb25lbnRzL05hdldyYXBwZXIuanMiLCJzcmMvanMvY29tcG9uZW50cy9Qcm9tb3Rpb24uanMiLCJzcmMvanMvY29tcG9uZW50cy9SZWZ1bmQuanMiLCJzcmMvanMvY29tcG9uZW50cy9SZXNvbHV0aW9uLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvVXNlck9yZGVycy5qcyIsInNyYy9qcy9jb21wb25lbnRzL1VzZXJSdW5zLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvVXNlcnMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUNwTEEsSUFBSSxhQUFhLFFBQVEseUJBQVIsQ0FBakI7O0FBRUEsZUFBZSxHQUFmLEdBQXFCLFFBQVEsR0FBUixDQUFZLEdBQWpDO0FBQ0EsZUFBZSxHQUFmLEdBQXFCLFFBQVEsR0FBUixDQUFZLEdBQWpDO0FBQ0EsZUFBZSxPQUFmLEdBQXlCLFFBQVEsR0FBUixDQUFZLE9BQXJDO0FBQ0EsZUFBZSxXQUFmLEdBQTJCLFFBQVEsR0FBUixDQUFZLFdBQXZDOztBQUVBLElBQUksSUFBSSxlQUFlLEdBQWYsR0FBcUIsZUFBZSxHQUFwQyxHQUEwQyxNQUFsRDtBQUNBLElBQUksSUFBSSxlQUFlLElBQWYsR0FBcUIsS0FBSyxLQUFMLENBQVcsZUFBZSxJQUExQixDQUFyQixHQUF1RCxFQUEvRDtBQUNBLE9BQU8sR0FBUCxHQUFhO0FBQ1osY0FBYSxDQUREO0FBRVosT0FBTSxDQUZNOztBQUlaLE1BQUssZUFBVztBQUNmLFVBQVEsT0FBTyxHQUFQLENBQVcsV0FBbkI7QUFDQyxRQUFLLE1BQUw7QUFDQyxXQUFPLGdCQUFQO0FBQ0QsUUFBSyxPQUFMO0FBQ0MsV0FBTyxpQkFBUDtBQUNELFFBQUssT0FBTDtBQUNDLFdBQU8saUJBQVA7QUFDRDtBQUNDLFdBQU8sZ0JBQVA7QUFSRjtBQVVBLEVBZlc7O0FBaUJaLFVBQVMsbUJBQVc7QUFDbkIsVUFBUSxPQUFPLEdBQVAsQ0FBVyxXQUFuQjtBQUNDLFFBQUssTUFBTDtBQUNDLFdBQU8sY0FBUDtBQUNELFFBQUssT0FBTDtBQUNDLFdBQU8sZUFBUDtBQUNELFFBQUssT0FBTDtBQUNDLFdBQU8sZUFBUDtBQUNEO0FBQ0MsV0FBTyxjQUFQO0FBUkY7QUFVQSxFQTVCVzs7QUE4QlosYUFBWSxvQkFBUyxPQUFULEVBQWtCO0FBQzdCLE1BQUksQ0FBQyxRQUFRLEtBQVIsQ0FBYyxJQUFkLENBQUwsRUFBMEI7QUFDekIsYUFBVSxVQUFVLElBQXBCO0FBQ0E7QUFDRCxNQUFJLElBQUksSUFBSSxJQUFKLENBQVMsT0FBVCxDQUFSO0FBQ0EsU0FBTyxFQUFFLFFBQUYsR0FBYSxPQUFiLENBQXFCLGdCQUFyQixFQUF1QyxFQUF2QyxDQUFQO0FBQ0EsRUFwQ1c7O0FBc0NaLE9BQU0sZ0JBQVc7QUFDaEIsU0FBTyxPQUFPLEdBQVAsQ0FBVyxJQUFYLENBQWdCLGNBQWhCLEtBQW1DLFNBQTFDO0FBQ0EsRUF4Q1c7O0FBMENaLFNBQVEsa0JBQVc7QUFDbEI7QUFDQSxFQTVDVzs7QUE4Q1osT0FBTSxnQkFBVztBQUNoQjtBQUNBLEVBaERXOztBQWtEWixlQUFjLHdCQUFXO0FBQ3hCO0FBQ0E7QUFwRFcsQ0FBYjs7QUF1REEsTUFBTSxNQUFOLENBQWEsb0JBQUMsVUFBRCxPQUFiLEVBQTZCLFNBQVMsY0FBVCxDQUF3QixLQUF4QixDQUE3Qjs7Ozs7OztBQ2hFQSxJQUFJLFNBQVMsUUFBUSxVQUFSLENBQWI7QUFDQSxJQUFJLFlBQVksUUFBUSxhQUFSLENBQWhCOztBQUVBLElBQUksWUFBWSxNQUFNLFdBQU4sQ0FBa0I7QUFBQTs7QUFDakMsbUJBQWlCLDJCQUFXO0FBQzNCLFdBQU87QUFDTixnQkFBVTtBQURKLEtBQVA7QUFHQSxHQUxnQzs7QUFPaEMsVUFBUSxrQkFBVztBQUNqQixXQUNFO0FBQUE7QUFBQSxRQUFLLFdBQVUsU0FBZjtBQUNFLDBCQUFDLE1BQUQ7QUFERixLQURGO0FBS0Q7QUFiK0IsQ0FBbEIsQ0FBaEI7O0FBZ0JBLE9BQU8sT0FBUCxHQUFpQixTQUFqQjs7Ozs7QUNsQkEsSUFBSSxTQUFTLE1BQU0sV0FBTixDQUFrQjtBQUFBOztBQUM3QixtQkFBaUIsMkJBQVc7QUFDMUIsV0FBTztBQUNMLG9CQUFjO0FBRFQsS0FBUDtBQUdELEdBTDRCOztBQU83QixnQkFBYyxzQkFBUyxNQUFULEVBQWlCO0FBQzdCLFFBQUksT0FBTyxJQUFYO0FBQ0EsV0FDRTtBQUFBO0FBQUE7QUFDRSxpQkFBVSxtQkFBVztBQUNuQixlQUFLLEtBQUwsQ0FBVyxVQUFYLENBQXNCLE1BQXRCO0FBQ0EsZUFBSyxRQUFMLENBQWMsRUFBRSxjQUFjLEtBQWhCLEVBQWQ7QUFDRCxTQUpIO0FBS0UsYUFBSyxNQUxQO0FBTUcsYUFBTyxXQUFQO0FBTkgsS0FERjtBQVVELEdBbkI0Qjs7QUFxQjdCLFVBQVEsa0JBQVc7QUFDakIsUUFBSSxPQUFPLElBQVg7QUFDQSxRQUFJLFVBQVUsRUFBZDtBQUNBLGNBQVUsS0FBSyxLQUFMLENBQVcsV0FBWCxDQUF1QixHQUF2QixDQUEyQixLQUFLLFlBQWhDLENBQVY7O0FBRUEsV0FDRTtBQUFBO0FBQUEsUUFBUSxXQUFZLEtBQUssS0FBTCxDQUFXLFlBQVgsR0FBMEIsZUFBMUIsR0FBNEMsRUFBaEU7QUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFVLFdBQWYsRUFBMkIsU0FBVSxtQkFBVztBQUFDLGlCQUFLLFFBQUwsQ0FBYyxFQUFDLGNBQWEsQ0FBQyxLQUFLLEtBQUwsQ0FBVyxZQUExQixFQUFkO0FBQXVELFdBQXhHO0FBQUE7QUFBQSxPQURGO0FBSUU7QUFBQTtBQUFBO0FBQ0c7QUFESCxPQUpGO0FBT0U7QUFBQTtBQUFBLFVBQUssV0FBVSxXQUFmO0FBQUE7QUFDSyxZQUFJLFdBRFQ7QUFBQTtBQUFBO0FBUEYsS0FERjtBQWFEO0FBdkM0QixDQUFsQixDQUFiOztBQTBDQSxPQUFPLE9BQVAsR0FBaUIsTUFBakI7Ozs7O0FDMUNBLElBQUksU0FBUyxNQUFNLFdBQU4sQ0FBa0I7QUFBQTs7O0FBRTlCLHNCQUFvQiw4QkFBVztBQUM5QixTQUFLLEdBQUwsR0FBVyxJQUFJLFFBQUosQ0FBYSwyQ0FBYixDQUFYO0FBQ0EsR0FKNkI7O0FBTzlCLGtCQUFnQiwwQkFBVztBQUMxQixTQUFLLEdBQUwsQ0FBUyxNQUFUO0FBQ0EsbUJBQWUsS0FBZjtBQUNBLFdBQU8sUUFBUCxHQUFrQixHQUFsQjtBQUNBLEdBWDZCOztBQWM3QixVQUFRLGtCQUFXO0FBQ2pCLFdBQ0U7QUFBQTtBQUFBLFFBQUssV0FBVSxnQkFBZjtBQUNFO0FBQUE7QUFBQSxVQUFRLFNBQVMsS0FBSyxjQUF0QjtBQUFBO0FBQUE7QUFERixLQURGO0FBS0Q7QUFwQjRCLENBQWxCLENBQWI7O0FBdUJBLE9BQU8sT0FBUCxHQUFpQixNQUFqQjs7Ozs7QUN4QkEsSUFBSSxZQUFZLE1BQU0sV0FBTixDQUFrQjtBQUFBOztBQUNoQyxtQkFBaUIsMkJBQVc7QUFDMUIsV0FBTztBQUNMLGNBQVEsS0FESDtBQUVMLFlBQU0sUUFGRDtBQUdMLGVBQVM7QUFISixLQUFQO0FBS0QsR0FQK0I7O0FBU2hDLHNCQUFvQiw4QkFBVztBQUM3QixRQUFJLE1BQUosR0FBYSxLQUFLLE1BQWxCO0FBQ0EsUUFBSSxJQUFKLEdBQVcsS0FBSyxJQUFoQjtBQUNBLFFBQUksWUFBSixHQUFtQixLQUFLLEtBQXhCO0FBQ0QsR0FiK0I7O0FBZWhDLFVBQVEsZ0JBQVMsR0FBVCxFQUFjO0FBQ3BCLFNBQUssUUFBTCxDQUFjO0FBQ1osY0FBUSxJQURJO0FBRVosZUFBUyxHQUZHO0FBR1osWUFBTTtBQUhNLEtBQWQ7QUFLRCxHQXJCK0I7O0FBdUJoQyxRQUFNLGNBQVMsR0FBVCxFQUFjO0FBQ2xCLFNBQUssUUFBTCxDQUFjO0FBQ1osY0FBUSxJQURJO0FBRVosZUFBUyxHQUZHO0FBR1osWUFBTTtBQUhNLEtBQWQ7QUFLRCxHQTdCK0I7O0FBK0JoQyxTQUFPLGlCQUFXO0FBQ2hCLFNBQUssUUFBTCxDQUFjO0FBQ1osY0FBUTtBQURJLEtBQWQ7QUFHRCxHQW5DK0I7O0FBcUNoQyxVQUFRLGdCQUFTLEdBQVQsRUFBYztBQUNwQixRQUFJLGNBQUo7QUFDQSxRQUFJLGVBQUo7QUFDRCxHQXhDK0I7O0FBMENoQyxVQUFRLGtCQUFXO0FBQ2pCLFFBQUksWUFBWSxLQUFLLEtBQUwsQ0FBVyxNQUFYLEdBQW9CLEVBQXBCLEdBQXlCLFlBQXpDO0FBQ0EsV0FDRTtBQUFBO0FBQUEsUUFBSyxTQUFTLEtBQUssS0FBbkIsRUFBMEIsV0FBWSxlQUFlLEtBQUssS0FBTCxDQUFXLElBQTFCLEdBQWlDLFNBQXZFO0FBQ0U7QUFBQTtBQUFBLFVBQUssV0FBVSxTQUFmLEVBQXlCLFNBQVMsS0FBSyxNQUF2QztBQUNJLGFBQUssS0FBTCxDQUFXLE9BRGY7QUFFRTtBQUFBO0FBQUEsWUFBSyxXQUFVLE9BQWYsRUFBdUIsU0FBUyxLQUFLLEtBQXJDO0FBQUE7QUFBQTtBQUZGO0FBREYsS0FERjtBQVFEO0FBcEQrQixDQUFsQixDQUFoQjs7QUF1REEsT0FBTyxPQUFQLEdBQWlCLFNBQWpCOzs7OztBQ3ZEQSxJQUFJLFNBQVMsUUFBUSxVQUFSLENBQWI7QUFDQSxJQUFJLFlBQVksUUFBUSxhQUFSLENBQWhCO0FBQ0EsSUFBSSxZQUFZLFFBQVEsYUFBUixDQUFoQjtBQUNBLElBQUksU0FBUyxRQUFRLFVBQVIsQ0FBYjtBQUNBLElBQUksYUFBYSxRQUFRLGNBQVIsQ0FBakI7QUFDQSxJQUFJLFFBQVEsUUFBUSxTQUFSLENBQVo7QUFDQSxJQUFJLFlBQVksUUFBUSxhQUFSLENBQWhCOztBQUVBLElBQUksYUFBYSxNQUFNLFdBQU4sQ0FBa0I7QUFBQTs7QUFDbEMsa0JBQWlCLDJCQUFXO0FBQzNCLFNBQU87QUFDTixhQUFVO0FBREosR0FBUDtBQUdBLEVBTGlDOztBQU9sQyxjQUFhLENBQ1YsV0FEVSxFQUVWLE9BRlUsRUFHVixTQUhVLEVBSVYsWUFKVSxFQUtWLFlBTFUsQ0FQcUI7O0FBZWxDLFdBQVUsa0JBQVMsSUFBVCxFQUFlO0FBQ3hCLE9BQUssUUFBTCxDQUFjLEVBQUMsVUFBVSxJQUFYLEVBQWQ7QUFDQSxFQWpCaUM7O0FBbUJsQyxhQUFZLHNCQUFXO0FBQ3RCLE1BQUksSUFBSjtBQUNBLFVBQU8sS0FBSyxLQUFMLENBQVcsUUFBbEI7QUFDQyxRQUFLLFdBQUw7QUFDQyxXQUFPLG9CQUFDLFNBQUQsT0FBUDtBQUNBO0FBQ0QsUUFBSyxPQUFMO0FBQ0MsV0FBTyxvQkFBQyxLQUFELE9BQVA7QUFDQTtBQUNELFFBQUssU0FBTDtBQUNDLFdBQU8sb0JBQUMsTUFBRCxPQUFQO0FBQ0E7QUFDRCxRQUFLLFlBQUw7QUFDQyxXQUFPLG9CQUFDLFVBQUQsT0FBUDtBQUNBO0FBQ0QsUUFBSyxZQUFMO0FBQ0MsV0FBTyxvQkFBQyxTQUFELE9BQVA7QUFDQTtBQWZGO0FBaUJBLFNBQU8sSUFBUDtBQUNBLEVBdkNpQzs7QUF5Q2pDLFNBQVEsa0JBQVc7QUFDbEIsTUFBSSxTQUNIO0FBQUE7QUFBQTtBQUFLLHVCQUFDLE1BQUQsSUFBUSxhQUFjLEtBQUssV0FBM0IsRUFBeUMsWUFBWSxLQUFLLFFBQTFELEdBQUw7QUFDQSx1QkFBQyxTQUFEO0FBREEsR0FERDtBQUdDLFNBQ0U7QUFBQTtBQUFBLEtBQUssV0FBVSxTQUFmO0FBQ0UsU0FERjtBQUVJLFFBQUssVUFBTDtBQUZKLEdBREY7QUFNRDtBQW5EZ0MsQ0FBbEIsQ0FBakI7O0FBc0RBLE9BQU8sT0FBUCxHQUFpQixVQUFqQjs7Ozs7QUM5REEsSUFBSSxZQUFZLE1BQU0sV0FBTixDQUFrQjtBQUFBOzs7QUFFaEMsWUFBVSxvQkFBVztBQUNuQixXQUFPLElBQUksR0FBSixLQUFZLEtBQW5CO0FBQ0QsR0FKK0I7O0FBTWhDLHVCQUFxQiwrQkFBVztBQUM5QixRQUFJLFlBQUo7QUFDRCxHQVIrQjs7QUFVaEMsbUJBQWlCLDJCQUFXO0FBQzFCLFdBQU87QUFDTCxjQUFRLFdBREgsRUFDZ0I7QUFDckIsaUJBQVcsRUFGTjtBQUdMLGNBQVMsVUFBVSxJQUFJLElBQUosQ0FBUyxjQUFuQixDQUhKO0FBSUwseUJBQW1CLEVBSmQ7QUFLTCxvQkFBYztBQUxULEtBQVA7QUFPRCxHQWxCK0I7O0FBcUJoQztBQUNBO0FBQ0E7QUFDQSxTQUFPLGVBQVMsT0FBVCxFQUFrQjtBQUN2QixZQUFRLEdBQVIsQ0FBWSxTQUFaLEVBQXVCLEtBQUssUUFBTCxLQUFrQixPQUF6QztBQUNBLFFBQUksT0FBTyxJQUFYO0FBQ0EsTUFBRSxJQUFGLENBQU87QUFDTCxXQUFLLEtBQUssUUFBTCxLQUFrQixPQURsQjtBQUVMLFlBQU0sTUFGRDtBQUdMLGdCQUFVLE1BSEw7QUFJTCxlQUFTLEtBQUssWUFKVDtBQUtMLGFBQU8sS0FBSztBQUxQLEtBQVA7QUFPRCxHQWxDK0I7O0FBb0NoQyxnQkFBYyxzQkFBUyxJQUFULEVBQWU7QUFDM0IsUUFBSSxLQUFLLE1BQVQsRUFBaUI7QUFDZixVQUFJLElBQUosQ0FBUyxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQVQ7QUFDRCxLQUZELE1BRU87QUFDTCxVQUFJLE1BQUosQ0FBVyxxQkFBWDtBQUNBLFFBQUUsMEJBQUYsRUFBOEIsR0FBOUIsQ0FBa0MsRUFBbEM7QUFDRDtBQUNELFlBQVEsR0FBUixDQUFZLElBQVo7QUFDRCxHQTVDK0I7O0FBOENoQyxjQUFZLG9CQUFTLEdBQVQsRUFBYztBQUN4QixRQUFJLElBQUksWUFBSixJQUFvQixJQUFJLFlBQUosQ0FBaUIsTUFBckMsSUFBK0MsSUFBSSxZQUFKLENBQWlCLE1BQWpCLENBQXdCLE1BQTNFLEVBQW1GO0FBQ2pGLFVBQUksSUFBSixDQUFTLHdCQUF3QixJQUFJLFlBQUosQ0FBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsQ0FBakM7QUFDRDtBQUNELFFBQUksSUFBSSxZQUFKLElBQW9CLElBQUksWUFBSixDQUFpQixVQUF6QyxFQUFxRDtBQUNuRCxVQUFJLElBQUosQ0FBUyx3QkFBd0IsSUFBSSxZQUFKLENBQWlCLFVBQXpDLEdBQXNELElBQXRELEdBQTZELElBQUksWUFBSixDQUFpQixPQUF2RjtBQUNELEtBRkQsTUFFTztBQUNMLFVBQUksSUFBSixDQUFTLDJCQUFUO0FBQ0EsY0FBUSxHQUFSLENBQVksR0FBWjtBQUNEO0FBQ0YsR0F4RCtCOztBQTJEaEM7QUFDQTtBQUNBO0FBQ0EsbUJBQWlCLHlCQUFTLEdBQVQsRUFBYztBQUM3QixTQUFLLFFBQUwsQ0FBYztBQUNaLGlCQUFXLElBQUksTUFBSixDQUFXO0FBRFYsS0FBZDtBQUdELEdBbEUrQjs7QUFvRWhDLGdCQUFjLHNCQUFTLEdBQVQsRUFBYztBQUMxQixTQUFLLFFBQUwsQ0FBYztBQUNaLHlCQUFtQixVQUFVLElBQUksTUFBSixDQUFXLEtBQXJCO0FBRFAsS0FBZDtBQUdELEdBeEUrQjs7QUEwRWhDLGlCQUFlLHVCQUFTLEdBQVQsRUFBYztBQUMzQixTQUFLLFFBQUwsQ0FBYztBQUNaLG9CQUFjLElBQUksTUFBSixDQUFXO0FBRGIsS0FBZDtBQUdELEdBOUUrQjs7QUFpRmhDLGFBQVcsbUJBQVMsR0FBVCxFQUFjO0FBQ3ZCLFFBQUksaUJBQWlCLElBQUksTUFBSixDQUFXLEtBQWhDO0FBQ0EsU0FBSyxRQUFMLENBQWM7QUFDWixjQUFRO0FBREksS0FBZDtBQUdELEdBdEYrQjs7QUF5RmhDLGNBQVksc0JBQVc7QUFDckIsU0FBSyxLQUFLLEtBQUwsQ0FBVyxNQUFoQjtBQUNELEdBM0YrQjs7QUE4RmhDLFVBQVEsa0JBQVc7QUFDakIsUUFBSSxZQUFZLEtBQUssS0FBTCxDQUFXLE1BQVgsS0FBc0IsZUFBdEM7O0FBRUEsV0FBUztBQUFBO0FBQUEsUUFBUyxXQUFZLFlBQXJCO0FBQ1A7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQURPO0FBRVA7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUZPO0FBR1A7QUFBQTtBQUFBLFVBQUssV0FBVSxNQUFmO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURGO0FBRUU7QUFBQTtBQUFBLGNBQUssV0FBVSxRQUFmO0FBQ0U7QUFBQTtBQUFBLGdCQUFRLFVBQVUsS0FBSyxTQUF2QjtBQUNFO0FBQUE7QUFBQSxrQkFBUSxPQUFNLFdBQWQ7QUFBQTtBQUFBLGVBREY7QUFJRTtBQUFBO0FBQUEsa0JBQVEsT0FBTSxlQUFkO0FBQUE7QUFBQTtBQUpGO0FBREY7QUFGRixTQURGO0FBYUU7QUFBQTtBQUFBLFlBQUssV0FBWSxZQUFZLFFBQVosR0FBdUIsRUFBeEM7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFtQjtBQUFBO0FBQUEsZ0JBQU0sV0FBVSxLQUFoQjtBQUFBO0FBQUE7QUFBbkIsV0FERjtBQUVFLHlDQUFPLE1BQUssTUFBWjtBQUNFLGtCQUFPLFlBRFQ7QUFFRSxzQkFBVyxLQUFLLGVBRmxCO0FBRkYsU0FiRjtBQW9CRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFlO0FBQUE7QUFBQSxnQkFBTSxXQUFVLEtBQWhCO0FBQUE7QUFBQTtBQUFmLFdBREY7QUFFRSx5Q0FBTyxNQUFLLE1BQVo7QUFDRSxrQkFBSyxTQURQO0FBRUUsc0JBQVcsS0FBSyxZQUZsQjtBQUZGLFNBcEJGO0FBMkJFO0FBQUE7QUFBQSxZQUFLLFdBQVksWUFBWSxFQUFaLEdBQWlCLFFBQWxDO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBc0I7QUFBQTtBQUFBLGdCQUFNLFdBQVUsS0FBaEI7QUFBQTtBQUFBO0FBQXRCLFdBREY7QUFFRSx5Q0FBTyxNQUFLLE1BQVo7QUFDRSxrQkFBSyxlQURQO0FBRUUsc0JBQVcsS0FBSyxhQUZsQjtBQUZGLFNBM0JGO0FBa0NFO0FBQUE7QUFBQSxZQUFLLFdBQVUsUUFBZixFQUF3QixTQUFZLEtBQUssVUFBekM7QUFBQTtBQUFBO0FBbENGO0FBSE8sS0FBVDtBQXdDRCxHQXpJK0I7O0FBNEloQztBQUNBO0FBQ0E7QUFDQSxhQUFXLHFCQUFXO0FBQ3BCLFFBQUksQ0FBQyxLQUFLLEtBQUwsQ0FBVyxTQUFoQixFQUEyQjtBQUN6QixhQUFPLElBQUksSUFBSixDQUFTLG9DQUFULENBQVA7QUFDRDtBQUNELFFBQUksQ0FBQyxLQUFLLEtBQUwsQ0FBVyxpQkFBaEIsRUFBbUM7QUFDakMsYUFBTyxJQUFJLElBQUosQ0FBUywyQkFBVCxDQUFQO0FBQ0Q7O0FBRUQsUUFBSSxPQUNGLDBCQUNBLEtBQUssS0FBTCxDQUFXLFNBRFgsR0FFQSxzQkFGQSxHQUdBLEtBQUssS0FBTCxDQUFXLE1BSFgsR0FJQSxxQkFKQSxHQUtBLEtBQUssS0FBTCxDQUFXLGlCQU5iO0FBT0EsU0FBSyxLQUFMLENBQVcsSUFBWDtBQUNELEdBL0orQjs7QUFpS2hDLGlCQUFlLHlCQUFXO0FBQ3hCLFFBQUksQ0FBQyxLQUFLLEtBQUwsQ0FBVyxpQkFBaEIsRUFBbUM7QUFDakMsYUFBTyxJQUFJLElBQUosQ0FBUywyQkFBVCxDQUFQO0FBQ0Q7QUFDRCxRQUFJLENBQUMsS0FBSyxLQUFMLENBQVcsWUFBaEIsRUFBOEI7QUFDNUIsYUFBTyxJQUFJLElBQUosQ0FBUyxpQ0FBVCxDQUFQO0FBQ0Q7O0FBRUQsUUFBSSxPQUNGLDBEQUNBLEtBQUssS0FBTCxDQUFXLE1BRFgsR0FFQSxnQkFGQSxHQUdBLEtBQUssS0FBTCxDQUFXLFlBSFgsR0FJQSxxQkFKQSxHQUtBLEtBQUssS0FBTCxDQUFXLGlCQU5iO0FBT0EsU0FBSyxLQUFMLENBQVcsSUFBWDtBQUNEO0FBakwrQixDQUFsQixDQUFoQjs7QUFvTEEsT0FBTyxPQUFQLEdBQWlCLFNBQWpCOzs7Ozs7O0FDcExBLElBQUksU0FBUyxNQUFNLFdBQU4sQ0FBa0I7QUFBQTs7QUFDekIsWUFBVSxvQkFBVztBQUNuQixXQUFPLElBQUksR0FBSixLQUFZLFNBQW5CO0FBQ0QsR0FId0I7O0FBS3pCLHVCQUFxQiwrQkFBVztBQUM5QjtBQUNELEdBUHdCOztBQVN6QixtQkFBaUIsMkJBQVc7QUFDMUIsV0FBTyxLQUFLLFVBQUwsRUFBUDtBQUNELEdBWHdCOztBQWF6QixjQUFZLHNCQUFXO0FBQ3JCLFdBQU87QUFDTCxjQUFRLGlCQURILEVBQ3NCO0FBQzNCLGFBQU8sRUFGRjtBQUdMLGdCQUFVLElBQUksSUFBSixDQUFTLGNBSGQsRUFHK0I7QUFDcEMsZUFBUyxFQUpKO0FBS0wsaUJBQVcsRUFMTjtBQU1MLGdCQUFVLEVBTkw7QUFPTCxtQkFBYSxFQVBSO0FBUUwsb0JBQWMsQ0FSVDtBQVNMLGVBQVM7QUFUSixLQUFQO0FBV0QsR0F6QndCOztBQTRCekI7QUFDQTtBQUNBO0FBQ0EsU0FBTyxlQUFTLE9BQVQsRUFBa0I7QUFDdkIsUUFBSSxPQUFPLEtBQUssY0FBTCxFQUFYO0FBQ0EsUUFBSSxTQUFTLElBQWIsRUFBbUI7QUFDakI7QUFDRDtBQUNELFlBQVEsR0FBUixDQUFZLFNBQVosRUFBdUIsS0FBSyxRQUFMLEtBQWtCLE9BQXpDO0FBQ0EsWUFBUSxHQUFSLENBQVksSUFBWjtBQUNBLFFBQUksT0FBTyxJQUFYO0FBQ0EsTUFBRSxJQUFGLENBQU87QUFDTCxXQUFLLEtBQUssUUFBTCxLQUFrQixPQURsQjtBQUVMLFlBQU0sS0FGRDtBQUdMLG1CQUFhLGdDQUhSO0FBSUw7QUFDQSxZQUFNLEtBQUssU0FBTCxDQUFlLElBQWYsQ0FMRDtBQU1MLGVBQVMsS0FBSyxZQU5UO0FBT0wsYUFBTyxLQUFLO0FBUFAsS0FBUDtBQVNELEdBaER3Qjs7QUFrRHpCLGdCQUFjLHNCQUFTLElBQVQsRUFBZTtBQUMzQixRQUFJLEtBQUssTUFBVCxFQUFpQjtBQUNmLFVBQUksSUFBSixDQUFTLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBVDtBQUNELEtBRkQsTUFFTztBQUNMLFVBQUksTUFBSixDQUFXLGtCQUFYO0FBQ0EsUUFBRSx3QkFBRixFQUE0QixDQUE1QixFQUErQixhQUEvQixHQUErQyxDQUEvQztBQUNBLFFBQUUsdUJBQUYsRUFBMkIsR0FBM0IsQ0FBK0IsRUFBL0I7QUFDQSxXQUFLLFFBQUwsQ0FBZSxLQUFLLFVBQUwsRUFBZjtBQUNEO0FBQ0QsWUFBUSxHQUFSLENBQVksSUFBWjtBQUNELEdBNUR3Qjs7QUE4RHpCLGNBQVksb0JBQVMsR0FBVCxFQUFjO0FBQ3hCLFFBQUksSUFBSSxZQUFKLElBQW9CLElBQUksWUFBSixDQUFpQixNQUFyQyxJQUErQyxJQUFJLFlBQUosQ0FBaUIsTUFBakIsQ0FBd0IsTUFBM0UsRUFBbUY7QUFDakYsVUFBSSxJQUFKLENBQVMscUJBQXFCLElBQUksWUFBSixDQUFpQixNQUFqQixDQUF3QixDQUF4QixDQUE5QjtBQUNEO0FBQ0QsUUFBSSxJQUFJLFlBQUosSUFBb0IsSUFBSSxZQUFKLENBQWlCLFVBQXpDLEVBQXFEO0FBQ25ELFVBQUksSUFBSixDQUFTLHFCQUFxQixJQUFJLFlBQUosQ0FBaUIsVUFBdEMsR0FBbUQsSUFBbkQsR0FBMEQsSUFBSSxZQUFKLENBQWlCLE9BQXBGO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSSxJQUFKLENBQVMsd0JBQVQ7QUFDQSxjQUFRLEdBQVIsQ0FBWSxHQUFaO0FBQ0Q7QUFDRixHQXhFd0I7O0FBMEV6QixrQkFBZ0IsMEJBQVc7QUFDekIsUUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLEtBQWhCLEVBQXVCO0FBQ3JCLFVBQUksSUFBSixDQUFTLDBCQUFUO0FBQ0EsYUFBTyxJQUFQO0FBQ0Q7QUFDRCxRQUFJLENBQUMsS0FBSyxLQUFMLENBQVcsT0FBaEIsRUFBeUI7QUFDdkIsVUFBSSxJQUFKLENBQVMsNkJBQVQ7QUFDQSxhQUFPLElBQVA7QUFDRDtBQUNELFFBQUksQ0FBQyxLQUFLLEtBQUwsQ0FBVyxXQUFoQixFQUE2QjtBQUMzQixVQUFJLElBQUosQ0FBUyx1REFBVDtBQUNBLGFBQU8sSUFBUDtBQUNEOztBQUVELFFBQUksT0FBTztBQUNULG9CQUFjLEtBQUssS0FBTCxDQUFXLE1BQVgsQ0FBa0IsT0FBbEIsQ0FBMEIsVUFBMUIsRUFBc0MsRUFBdEMsQ0FETDtBQUVULHNCQUFnQixLQUFLLEtBQUwsQ0FBVyxRQUZsQjtBQUdULG1CQUFhLEtBQUssS0FBTCxDQUFXLFdBSGY7QUFJVCxjQUFRO0FBQ04sd0JBQWdCLEtBQUssS0FBTCxDQUFXLFNBRHJCO0FBRU4sdUJBQWUsS0FBSyxLQUFMLENBQVc7QUFGcEI7QUFKQyxLQUFYO0FBU0EsUUFBSSxLQUFLLEtBQUwsQ0FBVyxPQUFmLEVBQXdCO0FBQ3RCLFdBQUssTUFBTCxDQUFZLGFBQVosR0FBNEIsT0FBTyxLQUFLLEtBQUwsQ0FBVyxZQUFsQixDQUE1QjtBQUNBLFVBQUksQ0FBQyxLQUFLLE1BQUwsQ0FBWSxhQUFqQixFQUFnQztBQUM5QixZQUFJLElBQUosQ0FBUyxpQ0FBVDtBQUNBLGVBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRCxXQUFPLElBQVA7QUFDRCxHQXpHd0I7O0FBNEd6QjtBQUNBO0FBQ0E7QUFDQSxlQUFhLHFCQUFTLEdBQVQsRUFBYztBQUN6QixTQUFLLFFBQUwsQ0FBYztBQUNaLGFBQU8sSUFBSSxNQUFKLENBQVc7QUFETixLQUFkO0FBR0QsR0FuSHdCOztBQXFIekIsaUJBQWUsdUJBQVMsR0FBVCxFQUFjO0FBQzNCLFNBQUssUUFBTCxDQUFjO0FBQ1osZUFBUyxJQUFJLE1BQUosQ0FBVztBQURSLEtBQWQ7QUFHRCxHQXpId0I7O0FBMkh6QixjQUFZLG9CQUFTLEdBQVQsRUFBYztBQUN4QixTQUFLLFFBQUwsQ0FBYztBQUNaLG1CQUFhLElBQUksTUFBSixDQUFXO0FBRFosS0FBZDtBQUdELEdBL0h3Qjs7QUFpSXpCLGdCQUFjLHNCQUFTLEdBQVQsRUFBYztBQUMxQixTQUFLLFFBQUwsQ0FBYztBQUNaLGlCQUFXLElBQUksTUFBSixDQUFXO0FBRFYsS0FBZDtBQUdELEdBckl3Qjs7QUF1SXpCLGdCQUFjLHNCQUFTLEdBQVQsRUFBYztBQUMxQixTQUFLLFFBQUwsQ0FBYztBQUNaLGdCQUFVLElBQUksTUFBSixDQUFXO0FBRFQsS0FBZDtBQUdELEdBM0l3Qjs7QUE2SXpCLG1CQUFpQix5QkFBUyxHQUFULEVBQWM7QUFDN0IsU0FBSyxRQUFMLENBQWM7QUFDWixvQkFBYyxJQUFJLE1BQUosQ0FBVztBQURiLEtBQWQ7QUFHRCxHQWpKd0I7O0FBb0p6QixhQUFXLG1CQUFTLEdBQVQsRUFBYztBQUN2QixRQUFJLGlCQUFpQixJQUFJLE1BQUosQ0FBVyxLQUFoQztBQUNBLFFBQUksVUFBVSxlQUFlLEtBQWYsQ0FBcUIsU0FBckIsS0FBbUMsSUFBakQ7QUFDQSxTQUFLLFFBQUwsQ0FBYztBQUNaLGNBQVEsY0FESTtBQUVaLGVBQVM7QUFGRyxLQUFkO0FBSUQsR0EzSndCOztBQThKekIsZUFBYSx1QkFBVztBQUN0QixTQUFLLEtBQUssS0FBTCxDQUFXLE1BQWhCO0FBQ0QsR0FoS3dCOztBQW1LekIsVUFBUSxrQkFBVztBQUFBOztBQUVqQixXQUFTO0FBQUE7QUFBQSxRQUFTLFdBQVUsU0FBbkI7QUFDUDtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BRE87QUFFUDtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BRk87QUFHUDtBQUFBO0FBQUEsVUFBSyxXQUFVLE1BQWY7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREY7QUFFRTtBQUFBO0FBQUEsY0FBSyxXQUFVLFFBQWY7QUFDRTtBQUFBO0FBQUEsZ0JBQVEsVUFBVSxLQUFLLFNBQXZCO0FBQ0U7QUFBQTtBQUFBLGtCQUFRLE9BQU0saUJBQWQ7QUFBQTtBQUFBLGVBREY7QUFFRTtBQUFBO0FBQUEsa0JBQVEsT0FBTSx1QkFBZDtBQUFBO0FBQUEsZUFGRjtBQUdFO0FBQUE7QUFBQSxrQkFBUSxPQUFNLHdCQUFkO0FBQUE7QUFBQSxlQUhGO0FBSUU7QUFBQTtBQUFBLGtCQUFRLE9BQU0sOEJBQWQ7QUFBQTtBQUFBO0FBSkY7QUFERjtBQUZGLFNBREY7QUFhRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFjO0FBQUE7QUFBQSxnQkFBTSxXQUFVLEtBQWhCO0FBQUE7QUFBQTtBQUFkLFdBREY7QUFFRSx5Q0FBTyxNQUFLLE1BQVosRUFBbUIsTUFBSyxRQUF4QixFQUFpQyxVQUFVLEtBQUssV0FBaEQ7QUFGRixTQWJGO0FBa0JFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQWdCO0FBQUE7QUFBQSxnQkFBTSxXQUFVLEtBQWhCO0FBQUE7QUFBQTtBQUFoQixXQURGO0FBRUUseUNBQU8sTUFBSyxNQUFaO0FBQ0Usa0JBQUssVUFEUDtBQUVFLHNCQUNFLEtBQUssYUFIVDtBQUZGLFNBbEJGO0FBMkJFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQTBCO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFBMUI7QUFBQTtBQUFvRDtBQUFBO0FBQUEsZ0JBQU0sV0FBVSxLQUFoQjtBQUFBO0FBQUE7QUFBcEQsV0FERjtBQUVFLHlDQUFPLE1BQUssTUFBWixFQUFtQixNQUFLLGFBQXhCLEVBQXNDLFVBQVUsS0FBSyxVQUFyRDtBQUZGLFNBM0JGO0FBZ0NFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQUVJLHlDQUFPLE1BQUssTUFBWjtBQUNBLGtCQUFLLGdCQURMO0FBRUEsc0JBQ0UsS0FBSyxZQUhQO0FBRkosU0FoQ0Y7QUF5Q0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURGO0FBRUUseUNBQU8sTUFBSyxNQUFaLEVBQW1CLE1BQUssZUFBeEIsRUFBd0MsVUFBVSxLQUFLLFlBQXZEO0FBRkYsU0F6Q0Y7QUE4Q0U7QUFBQTtBQUFBLFlBQUssV0FBWSxLQUFLLEtBQUwsQ0FBVyxPQUFYLEdBQXFCLEVBQXJCLEdBQTBCLFFBQTNDO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBcUI7QUFBQTtBQUFBLGdCQUFNLFdBQVUsS0FBaEI7QUFBQTtBQUFBO0FBQXJCLFdBREY7QUFBQTtBQUVHO0FBQ0Msa0JBQUssTUFETjtBQUVDLGtCQUFLO0FBRk4sMkRBR00sUUFITixnREFJSyxHQUpMLGlEQUtNLE1BTE4scURBTVcsS0FBSyxlQU5oQjtBQUZILFNBOUNGO0FBeURFO0FBQUE7QUFBQSxZQUFLLFdBQVUsUUFBZixFQUF3QixTQUFTLEtBQUssV0FBdEM7QUFBQTtBQUFBO0FBekRGO0FBSE8sS0FBVDtBQThEYSxHQW5PVTs7QUFzT3pCO0FBQ0E7QUFDQTtBQUNBLG1CQUFpQiwyQkFBVztBQUMxQixRQUFJLE9BQU8sS0FBSyxLQUFMLENBQVcsS0FBWCxHQUFtQixTQUFuQixHQUErQixLQUFLLEtBQUwsQ0FBVyxPQUExQyxHQUFvRCxTQUEvRDtBQUNBLFNBQUssS0FBTCxDQUFXLElBQVg7QUFDRCxHQTVPd0I7O0FBK096Qix5QkFBdUIsaUNBQVc7QUFDaEMsUUFBSSxPQUFPLEtBQUssS0FBTCxDQUFXLEtBQVgsR0FBbUIsU0FBbkIsR0FBK0IsS0FBSyxLQUFMLENBQVcsT0FBMUMsR0FBb0QsU0FBL0Q7QUFDQSxTQUFLLEtBQUwsQ0FBVyxJQUFYO0FBQ0QsR0FsUHdCOztBQXFQekIsMEJBQXdCLGtDQUFXO0FBQ2pDLFFBQUksT0FBTyxLQUFLLEtBQUwsQ0FBVyxLQUFYLEdBQW1CLFNBQW5CLEdBQStCLEtBQUssS0FBTCxDQUFXLE9BQTFDLEdBQW9ELFNBQS9EO0FBQ0EsU0FBSyxLQUFMLENBQVcsSUFBWDtBQUNELEdBeFB3Qjs7QUEyUHpCLGdDQUE4Qix3Q0FBVztBQUN2QyxRQUFJLE9BQU8sS0FBSyxLQUFMLENBQVcsS0FBWCxHQUFtQixTQUFuQixHQUErQixLQUFLLEtBQUwsQ0FBVyxPQUExQyxHQUFvRCxTQUEvRDtBQUNBLFNBQUssS0FBTCxDQUFXLElBQVg7QUFDRDtBQTlQd0IsQ0FBbEIsQ0FBYjs7QUFpUUksT0FBTyxPQUFQLEdBQWlCLE1BQWpCOzs7OztBQ2pRSixJQUFJLGFBQWEsTUFBTSxXQUFOLENBQWtCO0FBQUE7O0FBQzdCLFlBQVUsb0JBQVc7QUFDbkIsV0FBTyxJQUFJLEdBQUosS0FBWSxTQUFuQjtBQUNELEdBSDRCOztBQUs3Qix1QkFBcUIsK0JBQVc7QUFDOUIsUUFBSSxZQUFKO0FBQ0QsR0FQNEI7O0FBUzdCLG1CQUFpQiwyQkFBVztBQUMxQixXQUFPLEtBQUssVUFBTCxFQUFQO0FBQ0QsR0FYNEI7O0FBYTdCLGNBQVksc0JBQVc7QUFDckIsV0FBTztBQUNMLGNBQVEsZ0JBREgsRUFDcUI7QUFDMUIsYUFBTyxFQUZGO0FBR0wsZ0JBQVUsSUFBSSxJQUFKLENBQVMsY0FBVCxJQUEyQixFQUhoQyxFQUdxQztBQUMxQyxlQUFTLEVBSko7QUFLTCxtQkFBYTtBQUxSLEtBQVA7QUFPRCxHQXJCNEI7O0FBd0I3QjtBQUNBO0FBQ0E7QUFDQSxTQUFPLGVBQVMsT0FBVCxFQUFrQjtBQUN2QixRQUFJLE9BQU8sS0FBSyxjQUFMLEVBQVg7QUFDQSxRQUFJLFNBQVMsSUFBYixFQUFtQjtBQUNqQjtBQUNEO0FBQ0QsWUFBUSxHQUFSLENBQVksU0FBWixFQUF1QixLQUFLLFFBQUwsS0FBa0IsT0FBekM7QUFDQSxZQUFRLEdBQVIsQ0FBWSxJQUFaO0FBQ0EsUUFBSSxPQUFPLElBQVg7QUFDQSxNQUFFLElBQUYsQ0FBTztBQUNMLFdBQUssS0FBSyxRQUFMLEtBQWtCLE9BRGxCO0FBRUwsWUFBTSxLQUZEO0FBR0wsbUJBQWEsZ0NBSFI7QUFJTDtBQUNBLFlBQU0sS0FBSyxTQUFMLENBQWUsSUFBZixDQUxEO0FBTUwsZUFBUyxLQUFLLFlBTlQ7QUFPTCxhQUFPLEtBQUs7QUFQUCxLQUFQO0FBU0QsR0E1QzRCOztBQThDN0IsZ0JBQWMsc0JBQVMsSUFBVCxFQUFlO0FBQzNCLFFBQUksUUFBUSxLQUFLLE1BQWpCLEVBQXlCO0FBQ3ZCLFVBQUksSUFBSixDQUFTLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBVDtBQUNELEtBRkQsTUFFTztBQUNMLFVBQUksTUFBSixDQUFXLHNCQUFYO0FBQ0EsUUFBRSwyQkFBRixFQUErQixHQUEvQixDQUFtQyxFQUFuQztBQUNEO0FBQ0QsWUFBUSxHQUFSLENBQVksSUFBWjtBQUNELEdBdEQ0Qjs7QUF3RDdCLGNBQVksb0JBQVMsR0FBVCxFQUFjO0FBQ3hCLFFBQUksSUFBSSxZQUFKLElBQW9CLElBQUksWUFBSixDQUFpQixNQUFyQyxJQUErQyxJQUFJLFlBQUosQ0FBaUIsTUFBakIsQ0FBd0IsTUFBM0UsRUFBbUY7QUFDakYsVUFBSSxJQUFKLENBQVMseUJBQXlCLElBQUksWUFBSixDQUFpQixNQUFqQixDQUF3QixDQUF4QixDQUFsQztBQUNEO0FBQ0QsUUFBSSxJQUFJLFlBQUosSUFBb0IsSUFBSSxZQUFKLENBQWlCLFVBQXpDLEVBQXFEO0FBQ25ELFVBQUksSUFBSixDQUFTLHlCQUF5QixJQUFJLFlBQUosQ0FBaUIsVUFBMUMsR0FBdUQsSUFBdkQsR0FBOEQsSUFBSSxZQUFKLENBQWlCLE9BQXhGO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSSxJQUFKLENBQVMsNEJBQVQ7QUFDQSxjQUFRLEdBQVIsQ0FBWSxHQUFaO0FBQ0Q7QUFDRixHQWxFNEI7O0FBb0U3QixrQkFBZ0IsMEJBQVc7QUFDekIsUUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLEtBQWhCLEVBQXVCO0FBQ3JCLFVBQUksSUFBSixDQUFTLDBCQUFUO0FBQ0EsYUFBTyxJQUFQO0FBQ0Q7QUFDRCxRQUFJLEtBQUssS0FBTCxDQUFXLE1BQVgsS0FBc0IsYUFBdEIsSUFBdUMsQ0FBQyxLQUFLLEtBQUwsQ0FBVyxPQUF2RCxFQUFnRTtBQUM5RCxVQUFJLElBQUosQ0FBUyw0QkFBVDtBQUNBLGFBQU8sSUFBUDtBQUNEO0FBQ0QsUUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLFdBQWhCLEVBQTZCO0FBQzNCLFVBQUksSUFBSixDQUFTLHVEQUFUO0FBQ0EsYUFBTyxJQUFQO0FBQ0Q7O0FBRUQsUUFBSSxPQUFPO0FBQ1Qsb0JBQWMsS0FBSyxLQUFMLENBQVcsTUFEaEI7QUFFVCxzQkFBZ0IsS0FBSyxLQUFMLENBQVcsUUFGbEI7QUFHVCxtQkFBYSxLQUFLLEtBQUwsQ0FBVztBQUhmLEtBQVg7O0FBTUEsUUFBSSxLQUFLLEtBQUwsQ0FBVyxNQUFYLEtBQXNCLGFBQTFCLEVBQXlDO0FBQ3ZDLFdBQUssTUFBTCxHQUFjO0FBQ1osd0JBQWdCLEtBQUssS0FBTCxDQUFXLFNBRGY7QUFFWix1QkFBZSxLQUFLLEtBQUwsQ0FBVztBQUZkLE9BQWQ7QUFJRDtBQUNELFdBQU8sSUFBUDtBQUNELEdBL0Y0Qjs7QUFrRzdCO0FBQ0E7QUFDQTtBQUNBLGVBQWEscUJBQVMsR0FBVCxFQUFjO0FBQ3pCLFNBQUssUUFBTCxDQUFjO0FBQ1osYUFBTyxJQUFJLE1BQUosQ0FBVztBQUROLEtBQWQ7QUFHRCxHQXpHNEI7O0FBMkc3QixpQkFBZSx1QkFBUyxHQUFULEVBQWM7QUFDM0IsU0FBSyxRQUFMLENBQWM7QUFDWixlQUFTLElBQUksTUFBSixDQUFXO0FBRFIsS0FBZDtBQUdELEdBL0c0Qjs7QUFpSDdCLGNBQVksb0JBQVMsR0FBVCxFQUFjO0FBQ3hCLFNBQUssUUFBTCxDQUFjO0FBQ1osbUJBQWEsSUFBSSxNQUFKLENBQVc7QUFEWixLQUFkO0FBR0QsR0FySDRCOztBQXVIN0IsZ0JBQWMsc0JBQVMsR0FBVCxFQUFjO0FBQzFCLFNBQUssUUFBTCxDQUFjO0FBQ1osaUJBQVcsSUFBSSxNQUFKLENBQVc7QUFEVixLQUFkO0FBR0QsR0EzSDRCOztBQTZIN0IsZ0JBQWMsc0JBQVMsR0FBVCxFQUFjO0FBQzFCLFNBQUssUUFBTCxDQUFjO0FBQ1osZ0JBQVUsSUFBSSxNQUFKLENBQVc7QUFEVCxLQUFkO0FBR0QsR0FqSTRCOztBQW9JN0IsYUFBVyxtQkFBUyxHQUFULEVBQWM7QUFDdkIsUUFBSSxpQkFBaUIsSUFBSSxNQUFKLENBQVcsS0FBaEM7QUFDQSxTQUFLLFFBQUwsQ0FBYztBQUNaLGNBQVE7QUFESSxLQUFkOztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNELEdBaEo0Qjs7QUFtSjdCLGdCQUFjLHdCQUFXO0FBQ3ZCLFNBQUssS0FBSyxLQUFMLENBQVcsTUFBaEI7QUFDRCxHQXJKNEI7O0FBd0o3QixVQUFRLGtCQUFXO0FBQ2pCLFFBQUksY0FBYyxLQUFLLEtBQUwsQ0FBVyxNQUFYLEtBQXNCLGFBQXhDOztBQUVBLFdBQVM7QUFBQTtBQUFBLFFBQVMsV0FBVSxhQUFuQjtBQUNQO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FETztBQUVQO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FGTztBQUdQO0FBQUE7QUFBQSxVQUFLLFdBQVUsTUFBZjtBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQUVFO0FBQUE7QUFBQSxjQUFLLFdBQVUsUUFBZjtBQUNFO0FBQUE7QUFBQSxnQkFBUSxVQUFVLEtBQUssU0FBdkI7QUFDRTtBQUFBO0FBQUEsa0JBQVEsT0FBTSxnQkFBZDtBQUFBO0FBQUEsZUFERjtBQUVFO0FBQUE7QUFBQSxrQkFBUSxPQUFNLGFBQWQ7QUFBQTtBQUFBO0FBRkY7QUFERjtBQUZGLFNBREY7QUFXRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFlO0FBQUE7QUFBQSxnQkFBTSxXQUFVLEtBQWhCO0FBQUE7QUFBQTtBQUFmLFdBREY7QUFFRSx5Q0FBTyxNQUFLLE1BQVo7QUFDRSxrQkFBSyxRQURQO0FBRUUsc0JBQ0UsS0FBSyxXQUhUO0FBRkYsU0FYRjtBQW9CRTtBQUFBO0FBQUEsWUFBSyxXQUFZLGNBQWMsUUFBZCxHQUF5QixFQUExQztBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQWlCO0FBQUE7QUFBQSxnQkFBTSxXQUFVLEtBQWhCO0FBQUE7QUFBQTtBQUFqQixXQURGO0FBRUUseUNBQU8sTUFBSyxNQUFaO0FBQ0Usa0JBQUssVUFEUDtBQUVFLHNCQUNFLEtBQUssYUFIVDtBQUZGLFNBcEJGO0FBNkJFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQThCO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFBOUI7QUFBQTtBQUF3RDtBQUFBO0FBQUEsZ0JBQU0sV0FBVSxLQUFoQjtBQUFBO0FBQUE7QUFBeEQsV0FERjtBQUVFLHlDQUFPLE1BQUssTUFBWixFQUFtQixNQUFLLGFBQXhCLEVBQXNDLFVBQVUsS0FBSyxVQUFyRDtBQUZGLFNBN0JGO0FBa0NFO0FBQUE7QUFBQSxZQUFLLFdBQVksY0FBYyxFQUFkLEdBQW1CLFFBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURGO0FBRUkseUNBQU8sTUFBSyxNQUFaO0FBQ0Esa0JBQUssZ0JBREw7QUFFQSxzQkFDRSxLQUFLLFlBSFA7QUFGSixTQWxDRjtBQTJDRTtBQUFBO0FBQUEsWUFBSyxXQUFZLGNBQWMsRUFBZCxHQUFtQixRQUFwQztBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQUVFLHlDQUFPLE1BQUssTUFBWixFQUFtQixNQUFLLGVBQXhCLEVBQXdDLFVBQVUsS0FBSyxZQUF2RDtBQUZGLFNBM0NGO0FBZ0RFO0FBQUE7QUFBQSxZQUFLLFdBQVUsUUFBZixFQUF3QixTQUFTLEtBQUssWUFBdEM7QUFBQTtBQUFBO0FBaERGO0FBSE8sS0FBVDtBQXFEYSxHQWhOYzs7QUFtTjdCO0FBQ0E7QUFDQTtBQUNBLGtCQUFnQiwwQkFBVztBQUN6QixRQUFJLE9BQU8sS0FBSyxLQUFMLENBQVcsS0FBWCxHQUFtQixTQUFuQixHQUErQixLQUFLLEtBQUwsQ0FBVyxPQUExQyxHQUFvRCxTQUEvRDtBQUNBLFNBQUssS0FBTCxDQUFXLElBQVg7QUFDRCxHQXpONEI7O0FBNE43QixlQUFhLHVCQUFXO0FBQ3RCLFFBQUksT0FBTyxLQUFLLEtBQUwsQ0FBVyxLQUFYLEdBQW1CLFNBQTlCO0FBQ0EsU0FBSyxLQUFMLENBQVcsSUFBWDtBQUNEO0FBL040QixDQUFsQixDQUFqQjs7QUFrT0ksT0FBTyxPQUFQLEdBQWlCLFVBQWpCOzs7OztBQ2xPSixJQUFJLGFBQWEsTUFBTSxXQUFOLENBQWtCO0FBQUE7OztBQUVqQyxZQUFVLG9CQUFXO0FBQ25CLFdBQU8sSUFBSSxHQUFKLEtBQVksS0FBbkI7QUFDRCxHQUpnQzs7QUFPakMsNkJBQTJCLG1DQUFTLFNBQVQsRUFBb0I7QUFDN0MsU0FBSyxTQUFMLENBQWUsVUFBVSxNQUF6QjtBQUNELEdBVGdDOztBQVlqQyxtQkFBaUIsMkJBQVc7QUFDMUIsV0FBTztBQUNMLGNBQVE7QUFESCxLQUFQO0FBR0QsR0FoQmdDOztBQW1CakM7QUFDQTtBQUNBO0FBQ0EsU0FBTyxlQUFTLE9BQVQsRUFBa0IsTUFBbEIsRUFBMEI7QUFDL0IsWUFBUSxHQUFSLENBQVksU0FBWixFQUF1QixLQUFLLFFBQUwsS0FBa0IsT0FBekM7QUFDQSxRQUFJLE9BQU8sSUFBWDtBQUNBLE1BQUUsSUFBRixDQUFPO0FBQ0wsV0FBSyxLQUFLLFFBQUwsS0FBa0IsT0FEbEI7QUFFTCxZQUFNLEtBRkQ7QUFHTCxnQkFBVSxNQUhMO0FBSUwsZUFBUyxLQUFLLFlBSlQ7QUFLTCxhQUFPLEtBQUs7QUFMUCxLQUFQO0FBT0QsR0FoQ2dDOztBQW1DakMsZ0JBQWMsc0JBQVMsSUFBVCxFQUFlO0FBQzNCLFFBQUksS0FBSyxNQUFULEVBQWlCO0FBQ2YsVUFBSSxJQUFKLENBQVMsS0FBSyxNQUFMLENBQVksQ0FBWixDQUFUO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsV0FBSyxRQUFMLENBQWMsRUFBQyxRQUFRLElBQVQsRUFBZDtBQUNEO0FBQ0QsWUFBUSxHQUFSLENBQVksSUFBWjtBQUNELEdBMUNnQzs7QUE2Q2pDLGNBQVksb0JBQVMsR0FBVCxFQUFjO0FBQ3hCLFFBQUksSUFBSSxZQUFKLElBQW9CLElBQUksWUFBSixDQUFpQixNQUFyQyxJQUErQyxJQUFJLFlBQUosQ0FBaUIsTUFBakIsQ0FBd0IsTUFBM0UsRUFBbUY7QUFDakYsVUFBSSxJQUFKLENBQVMsMkJBQTJCLElBQUksWUFBSixDQUFpQixNQUFqQixDQUF3QixDQUF4QixDQUFwQztBQUNEO0FBQ0QsUUFBSSxJQUFJLFlBQUosSUFBb0IsSUFBSSxZQUFKLENBQWlCLFVBQXpDLEVBQXFEO0FBQ25ELFVBQUksSUFBSixDQUFTLDJCQUEyQixJQUFJLFlBQUosQ0FBaUIsVUFBNUMsR0FBeUQsSUFBekQsR0FBZ0UsSUFBSSxZQUFKLENBQWlCLE9BQTFGO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSSxJQUFKLENBQVMsc0JBQVQ7QUFDQSxjQUFRLEdBQVIsQ0FBWSxHQUFaO0FBQ0Q7QUFDRixHQXZEZ0M7O0FBMERqQyxlQUFhLHFCQUFTLEtBQVQsRUFBZ0I7QUFDM0IsUUFBSSxPQUFPLElBQVg7QUFDQSxXQUNFO0FBQUE7QUFBQSxRQUFJLEtBQU0sTUFBTSxRQUFoQjtBQUNFO0FBQUE7QUFBQTtBQUFNLGNBQU07QUFBWixPQURGO0FBRUU7QUFBQTtBQUFBO0FBQU0sY0FBTTtBQUFaLE9BRkY7QUFHRTtBQUFBO0FBQUE7QUFBTSxjQUFNO0FBQVosT0FIRjtBQUlFO0FBQUE7QUFBQTtBQUFNLFlBQUksVUFBSixDQUFlLE1BQU0sWUFBckI7QUFBTixPQUpGO0FBS0U7QUFBQTtBQUFBO0FBQU0sY0FBTTtBQUFaLE9BTEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sV0FBTixDQUFrQixPQUFsQixDQUEwQixDQUExQjtBQUFQLE9BTkY7QUFPRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sU0FBTixDQUFnQixPQUFoQixDQUF3QixDQUF4QjtBQUFQLE9BUEY7QUFRRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sWUFBTixDQUFtQixPQUFuQixDQUEyQixDQUEzQjtBQUFQLE9BUkY7QUFTRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sU0FBTixDQUFnQixPQUFoQixDQUF3QixDQUF4QjtBQUFQLE9BVEY7QUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sYUFBTixDQUFvQixPQUFwQixDQUE0QixDQUE1QjtBQUFQLE9BVkY7QUFXRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sdUJBQU4sQ0FBOEIsT0FBOUIsQ0FBc0MsQ0FBdEM7QUFBUDtBQVhGLEtBREY7QUFlRCxHQTNFZ0M7O0FBOEVqQyxVQUFRLGtCQUFXO0FBQ2pCLFFBQUksWUFBWSxLQUFLLEtBQUwsQ0FBVyxNQUFYLENBQWtCLEdBQWxCLENBQXNCLEtBQUssV0FBM0IsQ0FBaEI7O0FBRUEsUUFBSSxVQUFVLE1BQVYsSUFBb0IsS0FBSyxLQUFMLENBQVcsTUFBbkMsRUFBMkM7QUFDekMsYUFBUztBQUFBO0FBQUEsVUFBSyxXQUFVLGFBQWY7QUFDUDtBQUFBO0FBQUE7QUFBTSxlQUFLLEtBQUwsQ0FBVyxXQUFqQjtBQUFBO0FBQUEsU0FETztBQUVQO0FBQUE7QUFBQSxZQUFPLGFBQVksR0FBbkI7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSEY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSkY7QUFLRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBTEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBTkY7QUFPRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUEY7QUFRRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUkY7QUFTRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBVEY7QUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBVkY7QUFXRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWEY7QUFERixXQURGO0FBZ0JFO0FBQUE7QUFBQTtBQUNJO0FBREo7QUFoQkY7QUFGTyxPQUFUO0FBdUJELEtBeEJELE1BeUJLLElBQUksS0FBSyxLQUFMLENBQVcsTUFBZixFQUF1QjtBQUMxQixhQUFRO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBUjtBQUNELEtBRkksTUFHQTtBQUNILGFBQVEsNkJBQUssV0FBVSxhQUFmLEdBQVI7QUFDRDtBQUNGLEdBaEhnQzs7QUFtSGpDLGFBQVcsbUJBQVMsTUFBVCxFQUFpQjtBQUMxQixRQUFJLENBQUMsTUFBTCxFQUFhO0FBQ1g7QUFDRDtBQUNELFFBQUksT0FBTyxnQkFBWDtBQUNBLFNBQUssS0FBTCxDQUFXLElBQVgsRUFBaUIsTUFBakI7QUFDRDtBQXpIZ0MsQ0FBbEIsQ0FBakI7O0FBNEhBLE9BQU8sT0FBUCxHQUFpQixVQUFqQjs7Ozs7QUM1SEEsSUFBSSxXQUFXLE1BQU0sV0FBTixDQUFrQjtBQUFBOzs7QUFFL0IsWUFBVSxvQkFBVztBQUNuQixXQUFPLElBQUksR0FBSixLQUFZLEtBQW5CO0FBQ0QsR0FKOEI7O0FBTy9CLDZCQUEyQixtQ0FBUyxTQUFULEVBQW9CO0FBQzdDLFNBQUssU0FBTCxDQUFlLFVBQVUsTUFBekI7QUFDRCxHQVQ4Qjs7QUFZL0IsbUJBQWlCLDJCQUFXO0FBQzFCLFdBQU87QUFDTCxjQUFRO0FBREgsS0FBUDtBQUdELEdBaEI4Qjs7QUFtQi9CO0FBQ0E7QUFDQTtBQUNBLFNBQU8sZUFBUyxPQUFULEVBQWtCLE1BQWxCLEVBQTBCO0FBQy9CLFlBQVEsR0FBUixDQUFZLFNBQVosRUFBdUIsS0FBSyxRQUFMLEtBQWtCLE9BQXpDO0FBQ0EsUUFBSSxPQUFPLElBQVg7QUFDQSxNQUFFLElBQUYsQ0FBTztBQUNMLFdBQUssS0FBSyxRQUFMLEtBQWtCLE9BRGxCO0FBRUwsWUFBTSxLQUZEO0FBR0wsZ0JBQVUsTUFITDtBQUlMLGVBQVMsS0FBSyxZQUpUO0FBS0wsYUFBTyxLQUFLO0FBTFAsS0FBUDtBQU9ELEdBaEM4Qjs7QUFtQy9CLGdCQUFjLHNCQUFTLElBQVQsRUFBZTtBQUMzQixRQUFJLEtBQUssTUFBVCxFQUFpQjtBQUNmLFVBQUksSUFBSixDQUFTLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBVDtBQUNELEtBRkQsTUFFTztBQUNMLFdBQUssUUFBTCxDQUFjLEVBQUMsUUFBUSxJQUFULEVBQWQ7QUFDRDtBQUNELFlBQVEsR0FBUixDQUFZLElBQVo7QUFDRCxHQTFDOEI7O0FBNkMvQixjQUFZLG9CQUFTLEdBQVQsRUFBYztBQUN4QixRQUFJLElBQUksWUFBSixJQUFvQixJQUFJLFlBQUosQ0FBaUIsTUFBckMsSUFBK0MsSUFBSSxZQUFKLENBQWlCLE1BQWpCLENBQXdCLE1BQTNFLEVBQW1GO0FBQ2pGLFVBQUksSUFBSixDQUFTLDJCQUEyQixJQUFJLFlBQUosQ0FBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsQ0FBcEM7QUFDRDtBQUNELFFBQUksSUFBSSxZQUFKLElBQW9CLElBQUksWUFBSixDQUFpQixVQUF6QyxFQUFxRDtBQUNuRCxVQUFJLElBQUosQ0FBUywyQkFBMkIsSUFBSSxZQUFKLENBQWlCLFVBQTVDLEdBQXlELElBQXpELEdBQWdFLElBQUksWUFBSixDQUFpQixPQUExRjtBQUNELEtBRkQsTUFFTztBQUNMLFVBQUksSUFBSixDQUFTLHNCQUFUO0FBQ0EsY0FBUSxHQUFSLENBQVksR0FBWjtBQUNEO0FBQ0YsR0F2RDhCOztBQTBEL0IsZUFBYSxxQkFBUyxLQUFULEVBQWdCO0FBQzNCLFFBQUksT0FBTyxJQUFYO0FBQ0EsV0FDRTtBQUFBO0FBQUEsUUFBSSxLQUFNLE1BQU0sTUFBaEI7QUFDRTtBQUFBO0FBQUE7QUFBTSxjQUFNO0FBQVosT0FERjtBQUVFO0FBQUE7QUFBQTtBQUFNLFlBQUksVUFBSixDQUFlLE1BQU0sWUFBckI7QUFBTixPQUZGO0FBR0U7QUFBQTtBQUFBO0FBQU0sY0FBTTtBQUFaLE9BSEY7QUFJRTtBQUFBO0FBQUE7QUFBTSxjQUFNO0FBQVosT0FKRjtBQUtFO0FBQUE7QUFBQTtBQUFBO0FBQU8sY0FBTSxRQUFOLENBQWUsT0FBZixDQUF1QixDQUF2QjtBQUFQLE9BTEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFPLGNBQU0sZ0JBQU4sQ0FBdUIsT0FBdkIsQ0FBK0IsQ0FBL0I7QUFBUDtBQU5GLEtBREY7QUFVRCxHQXRFOEI7O0FBeUUvQixVQUFRLGtCQUFXO0FBQ2pCLFFBQUksWUFBWSxLQUFLLEtBQUwsQ0FBVyxNQUFYLENBQWtCLEdBQWxCLENBQXNCLEtBQUssV0FBM0IsQ0FBaEI7O0FBRUEsUUFBSSxVQUFVLE1BQVYsSUFBb0IsS0FBSyxLQUFMLENBQVcsTUFBbkMsRUFBMkM7QUFDekMsYUFBUztBQUFBO0FBQUEsVUFBSyxXQUFVLGFBQWY7QUFDUDtBQUFBO0FBQUE7QUFBTSxlQUFLLEtBQUwsQ0FBVyxXQUFqQjtBQUFBO0FBQUEsU0FETztBQUVQO0FBQUE7QUFBQSxZQUFPLGFBQVksR0FBbkI7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSEY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSkY7QUFLRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBTEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTkY7QUFERixXQURGO0FBV0U7QUFBQTtBQUFBO0FBQ0k7QUFESjtBQVhGO0FBRk8sT0FBVDtBQWtCRCxLQW5CRCxNQW9CSyxJQUFJLEtBQUssS0FBTCxDQUFXLE1BQWYsRUFBdUI7QUFDMUIsYUFBUTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQVI7QUFDRCxLQUZJLE1BR0E7QUFDSCxhQUFRLDZCQUFLLFdBQVUsYUFBZixHQUFSO0FBQ0Q7QUFDRixHQXRHOEI7O0FBeUcvQixhQUFXLG1CQUFTLE1BQVQsRUFBaUI7QUFDMUIsUUFBSSxDQUFDLE1BQUwsRUFBYTtBQUNYO0FBQ0Q7QUFDRCxRQUFJLE9BQU8sY0FBWDtBQUNBLFNBQUssS0FBTCxDQUFXLElBQVgsRUFBaUIsTUFBakI7QUFDRDtBQS9HOEIsQ0FBbEIsQ0FBZjs7QUFrSEEsT0FBTyxPQUFQLEdBQWlCLFFBQWpCOzs7OztBQ2xIQSxJQUFJLGFBQWEsUUFBUSxjQUFSLENBQWpCO0FBQ0EsSUFBSSxXQUFXLFFBQVEsWUFBUixDQUFmOztBQUVBLElBQUksUUFBUSxNQUFNLFdBQU4sQ0FBa0I7QUFBQTs7O0FBRTVCLFlBQVUsb0JBQVc7QUFDbkIsV0FBTyxJQUFJLEdBQUosS0FBWSxLQUFuQjtBQUNELEdBSjJCOztBQU81Qix1QkFBcUIsK0JBQVc7QUFDOUIsUUFBSSxZQUFKO0FBQ0QsR0FUMkI7O0FBWTVCLHFCQUFtQiw2QkFBVztBQUM1QixNQUFFLFlBQUYsRUFBZ0IsS0FBaEIsQ0FBc0IsWUFBVztBQUFFLFFBQUUsSUFBRixFQUFRLE1BQVI7QUFBbUIsS0FBdEQ7QUFDQSxNQUFFLFdBQUYsRUFBZSxLQUFmO0FBQ0QsR0FmMkI7O0FBa0I1QixtQkFBaUIsMkJBQVc7QUFDMUIsV0FBTztBQUNMLGlCQUFXLEVBRE47QUFFTCxnQkFBVSxFQUZMO0FBR0wsYUFBTztBQUhGLEtBQVA7QUFLRCxHQXhCMkI7O0FBMkI1QjtBQUNBO0FBQ0E7QUFDQSxTQUFPLGVBQVMsT0FBVCxFQUFrQjtBQUN2QixZQUFRLEdBQVIsQ0FBWSxTQUFaLEVBQXVCLEtBQUssUUFBTCxLQUFrQixPQUF6QztBQUNBLFFBQUksT0FBTyxJQUFYO0FBQ0EsTUFBRSxJQUFGLENBQU87QUFDTCxXQUFLLEtBQUssUUFBTCxLQUFrQixPQURsQjtBQUVMLFlBQU0sS0FGRDtBQUdMLGdCQUFVLE1BSEw7QUFJTCxlQUFTLEtBQUssWUFKVDtBQUtMLGFBQU8sS0FBSztBQUxQLEtBQVA7QUFPRCxHQXhDMkI7O0FBMkM1QixnQkFBYyxzQkFBUyxJQUFULEVBQWU7QUFDM0IsUUFBSSxLQUFLLE1BQVQsRUFBaUI7QUFDZixVQUFJLElBQUosQ0FBUyxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQVQ7QUFDRCxLQUZELE1BRU87QUFDTCxVQUFJLE1BQUosQ0FBVyx1QkFBWDtBQUNBLFdBQUssUUFBTCxDQUFjO0FBQ1osbUJBQVcsRUFEQyxFQUNHLFVBQVUsRUFEYjtBQUVaLGVBQU87QUFGSyxPQUFkO0FBSUEsUUFBRSxZQUFGLEVBQWdCLEdBQWhCLENBQW9CLEVBQXBCO0FBQ0Q7QUFDRCxZQUFRLEdBQVIsQ0FBWSxJQUFaO0FBQ0QsR0F2RDJCOztBQTBENUIsY0FBWSxvQkFBUyxHQUFULEVBQWM7QUFDeEIsUUFBSSxJQUFJLFlBQUosSUFBb0IsSUFBSSxZQUFKLENBQWlCLE1BQXJDLElBQStDLElBQUksWUFBSixDQUFpQixNQUFqQixDQUF3QixNQUEzRSxFQUFtRjtBQUNqRixVQUFJLElBQUosQ0FBUyxxQkFBcUIsSUFBSSxZQUFKLENBQWlCLE1BQWpCLENBQXdCLENBQXhCLENBQTlCO0FBQ0Q7QUFDRCxRQUFJLElBQUksWUFBSixJQUFvQixJQUFJLFlBQUosQ0FBaUIsVUFBekMsRUFBcUQ7QUFDbkQsVUFBSSxJQUFKLENBQVMscUJBQXFCLElBQUksWUFBSixDQUFpQixVQUF0QyxHQUFtRCxJQUFuRCxHQUEwRCxJQUFJLFlBQUosQ0FBaUIsT0FBcEY7QUFDRCxLQUZELE1BRU87QUFDTCxVQUFJLElBQUosQ0FBUyxxQkFBVDtBQUNBLGNBQVEsR0FBUixDQUFZLEdBQVo7QUFDRDtBQUNGLEdBcEUyQjs7QUF1RTVCLGNBQVksb0JBQVMsSUFBVCxFQUFlO0FBQ3pCLFFBQUksT0FBTyxJQUFYO0FBQ0EsUUFBSSxXQUFXLEVBQWY7QUFDQSxRQUFJLEtBQUssS0FBTCxDQUFXLFdBQVgsS0FBMkIsS0FBSyxjQUFoQyxJQUFrRCxLQUFLLEtBQUwsQ0FBVyxTQUFYLEtBQXlCLEtBQUssY0FBcEYsRUFBb0c7QUFDbEcsaUJBQVcsVUFBWDtBQUNEO0FBQ0QsV0FDRTtBQUFBO0FBQUEsUUFBSSxXQUFZLFFBQWhCLEVBQTJCLEtBQU0sS0FBSyxjQUF0QztBQUNFO0FBQUE7QUFBQTtBQUFNLGFBQUs7QUFBWCxPQURGO0FBRUU7QUFBQTtBQUFBO0FBQU0sYUFBSztBQUFYLE9BRkY7QUFHRTtBQUFBO0FBQUE7QUFBTSxhQUFLO0FBQVgsT0FIRjtBQUlFO0FBQUE7QUFBQTtBQUFNLGFBQUs7QUFBWCxPQUpGO0FBS0U7QUFBQTtBQUFBO0FBQU0sYUFBSyxhQUFMLElBQXNCO0FBQTVCLE9BTEY7QUFNRTtBQUFBO0FBQUE7QUFBTSxhQUFLLFlBQUwsSUFBcUI7QUFBM0IsT0FORjtBQU9FO0FBQUE7QUFBQTtBQUFNLGFBQUs7QUFBWCxPQVBGO0FBUUU7QUFBQTtBQUFBO0FBQU0sYUFBSztBQUFYLE9BUkY7QUFTRTtBQUFBO0FBQUEsVUFBSSxXQUFVLE1BQWQsRUFBcUIsU0FBVSxtQkFBVztBQUN4QyxpQkFBSyxRQUFMLENBQWM7QUFDWix5QkFBVyxLQUFLLGNBREo7QUFFWiw4QkFBZ0IsS0FBSyxZQUZUO0FBR1osMkJBQWEsRUFIRDtBQUlaLGdDQUFrQjtBQUpOLGFBQWQ7QUFLRyxXQU5MO0FBQUE7QUFBQSxPQVRGO0FBaUJFO0FBQUE7QUFBQSxVQUFJLFdBQVUsTUFBZCxFQUFxQixTQUFVLG1CQUFXO0FBQ3hDLGlCQUFLLFFBQUwsQ0FBYztBQUNaLDJCQUFhLEtBQUssY0FETjtBQUVaLGdDQUFrQixLQUFLLFlBRlg7QUFHWix5QkFBVyxFQUhDO0FBSVosOEJBQWdCO0FBSkosYUFBZDtBQUtHLFdBTkw7QUFBQTtBQUFBO0FBakJGLEtBREY7QUE0QkQsR0F6RzJCOztBQTRHNUIsVUFBUSxrQkFBVztBQUNqQixRQUFJLFdBQVcsS0FBSyxLQUFMLENBQVcsS0FBWCxDQUFpQixHQUFqQixDQUFxQixLQUFLLFVBQTFCLENBQWY7O0FBRUEsUUFBSSxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsVUFBSSxZQUNGO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FERjtBQUVFO0FBQUE7QUFBQSxZQUFPLGFBQVksR0FBbkI7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSEY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSkY7QUFLRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBTEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBTkY7QUFPRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUEY7QUFRRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUkY7QUFTRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBVEY7QUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVkY7QUFERixXQURGO0FBZUU7QUFBQTtBQUFBO0FBQ0k7QUFESjtBQWZGO0FBRkYsT0FERjtBQXVCRDs7QUFFRCxXQUFTO0FBQUE7QUFBQSxRQUFTLFdBQVUsYUFBbkI7QUFDUDtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BRE87QUFFUDtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BRk87QUFJUDtBQUFBO0FBQUEsVUFBSyxXQUFVLE1BQWY7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBREY7QUFFRSx1Q0FBTyxTQUFVLEtBQUssV0FBdEIsRUFBb0MsTUFBSyxNQUF6QyxFQUFnRCxVQUFXLEtBQUssWUFBaEUsRUFBK0UsTUFBSyxZQUFwRixFQUFpRyxXQUFVLFVBQTNHLEdBRkY7QUFHRSx1Q0FIRjtBQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FKRjtBQUtFLHVDQUFPLFNBQVUsS0FBSyxXQUF0QixFQUFvQyxNQUFLLE1BQXpDLEVBQWdELFVBQVcsS0FBSyxXQUFoRSxFQUE4RSxNQUFLLFdBQW5GLEdBTEY7QUFNRSx1Q0FORjtBQU9FO0FBQUE7QUFBQSxZQUFLLFdBQVUsUUFBZixFQUF3QixTQUFVLEtBQUssV0FBdkM7QUFBQTtBQUFBO0FBUEYsT0FKTztBQWFQLHFDQWJPO0FBY1AscUNBZE87QUFlTCxlQWZLO0FBZ0JQLHFDQWhCTztBQWlCUCxxQ0FqQk87QUFrQlAsMEJBQUMsVUFBRCxJQUFZLFFBQVMsS0FBSyxLQUFMLENBQVcsV0FBaEMsRUFBOEMsYUFBYyxLQUFLLEtBQUwsQ0FBVyxnQkFBdkUsR0FsQk87QUFtQlAsMEJBQUMsUUFBRCxJQUFVLFFBQVMsS0FBSyxLQUFMLENBQVcsU0FBOUIsRUFBMEMsYUFBYyxLQUFLLEtBQUwsQ0FBVyxjQUFuRTtBQW5CTyxLQUFUO0FBcUJELEdBOUoyQjs7QUFpSzVCLGdCQUFjLHNCQUFTLEdBQVQsRUFBYztBQUMxQixTQUFLLFFBQUwsQ0FBYyxFQUFDLFdBQVcsSUFBSSxNQUFKLENBQVcsS0FBdkIsRUFBZDtBQUNELEdBbksyQjs7QUFzSzVCLGVBQWEscUJBQVMsR0FBVCxFQUFjO0FBQ3pCLFNBQUssUUFBTCxDQUFjLEVBQUMsVUFBVSxJQUFJLE1BQUosQ0FBVyxLQUF0QixFQUFkO0FBQ0QsR0F4SzJCOztBQTJLNUIsZUFBYSxxQkFBUyxHQUFULEVBQWM7QUFDekIsVUFBTSxPQUFPLE9BQU8sS0FBcEI7QUFDQSxRQUFJLElBQUksT0FBSixLQUFnQixFQUFwQixFQUF3QjtBQUN0QixXQUFLLFdBQUw7QUFDQSxhQUFPLEtBQVA7QUFDRDtBQUNGLEdBakwyQjs7QUFvTDVCLGVBQWEsdUJBQVc7QUFDdEIsUUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLFNBQVosSUFBeUIsQ0FBQyxLQUFLLEtBQUwsQ0FBVyxRQUF6QyxFQUFtRDtBQUNqRCxhQUFPLElBQUksSUFBSixDQUFTLHlEQUFULENBQVA7QUFDRDs7QUFFRCxRQUFJLE9BQU8sZ0JBQVg7O0FBRUEsUUFBSSxLQUFLLEtBQUwsQ0FBVyxTQUFmLEVBQTBCO0FBQ3hCLGNBQVMsV0FBVyxLQUFLLEtBQUwsQ0FBVyxTQUEvQjs7QUFFQSxVQUFJLEtBQUssS0FBTCxDQUFXLFFBQWYsRUFBeUI7QUFDdkIsZ0JBQVEsT0FBTyxXQUFXLEtBQUssS0FBTCxDQUFXLFFBQTdCLENBQVI7QUFDRDtBQUNGLEtBTkQsTUFPSyxJQUFJLEtBQUssS0FBTCxDQUFXLFFBQWYsRUFBeUI7QUFDNUIsY0FBUyxXQUFXLEtBQUssS0FBTCxDQUFXLFFBQS9CO0FBQ0Q7O0FBRUQsU0FBSyxLQUFMLENBQVcsSUFBWDs7QUFFQTtBQUNBLFNBQUssUUFBTCxDQUFjO0FBQ1osbUJBQWEsRUFERDtBQUVaLHdCQUFrQixFQUZOO0FBR1osaUJBQVcsRUFIQztBQUlaLHNCQUFnQjtBQUpKLEtBQWQ7QUFNRDtBQS9NMkIsQ0FBbEIsQ0FBWjs7QUFrTkEsT0FBTyxPQUFQLEdBQWlCLEtBQWpCIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8vIHNoaW0gZm9yIHVzaW5nIHByb2Nlc3MgaW4gYnJvd3NlclxudmFyIHByb2Nlc3MgPSBtb2R1bGUuZXhwb3J0cyA9IHt9O1xuXG4vLyBjYWNoZWQgZnJvbSB3aGF0ZXZlciBnbG9iYWwgaXMgcHJlc2VudCBzbyB0aGF0IHRlc3QgcnVubmVycyB0aGF0IHN0dWIgaXRcbi8vIGRvbid0IGJyZWFrIHRoaW5ncy4gIEJ1dCB3ZSBuZWVkIHRvIHdyYXAgaXQgaW4gYSB0cnkgY2F0Y2ggaW4gY2FzZSBpdCBpc1xuLy8gd3JhcHBlZCBpbiBzdHJpY3QgbW9kZSBjb2RlIHdoaWNoIGRvZXNuJ3QgZGVmaW5lIGFueSBnbG9iYWxzLiAgSXQncyBpbnNpZGUgYVxuLy8gZnVuY3Rpb24gYmVjYXVzZSB0cnkvY2F0Y2hlcyBkZW9wdGltaXplIGluIGNlcnRhaW4gZW5naW5lcy5cblxudmFyIGNhY2hlZFNldFRpbWVvdXQ7XG52YXIgY2FjaGVkQ2xlYXJUaW1lb3V0O1xuXG5mdW5jdGlvbiBkZWZhdWx0U2V0VGltb3V0KCkge1xuICAgIHRocm93IG5ldyBFcnJvcignc2V0VGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO1xufVxuZnVuY3Rpb24gZGVmYXVsdENsZWFyVGltZW91dCAoKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdjbGVhclRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbihmdW5jdGlvbiAoKSB7XG4gICAgdHJ5IHtcbiAgICAgICAgaWYgKHR5cGVvZiBzZXRUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBkZWZhdWx0U2V0VGltb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgaWYgKHR5cGVvZiBjbGVhclRpbWVvdXQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGRlZmF1bHRDbGVhclRpbWVvdXQ7XG4gICAgICAgIH1cbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGRlZmF1bHRDbGVhclRpbWVvdXQ7XG4gICAgfVxufSAoKSlcbmZ1bmN0aW9uIHJ1blRpbWVvdXQoZnVuKSB7XG4gICAgaWYgKGNhY2hlZFNldFRpbWVvdXQgPT09IHNldFRpbWVvdXQpIHtcbiAgICAgICAgLy9ub3JtYWwgZW52aXJvbWVudHMgaW4gc2FuZSBzaXR1YXRpb25zXG4gICAgICAgIHJldHVybiBzZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfVxuICAgIC8vIGlmIHNldFRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRTZXRUaW1lb3V0ID09PSBkZWZhdWx0U2V0VGltb3V0IHx8ICFjYWNoZWRTZXRUaW1lb3V0KSAmJiBzZXRUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBzZXRUaW1lb3V0O1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfSBjYXRjaChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCB0cnVzdCB0aGUgZ2xvYmFsIG9iamVjdCB3aGVuIGNhbGxlZCBub3JtYWxseVxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQuY2FsbChudWxsLCBmdW4sIDApO1xuICAgICAgICB9IGNhdGNoKGUpe1xuICAgICAgICAgICAgLy8gc2FtZSBhcyBhYm92ZSBidXQgd2hlbiBpdCdzIGEgdmVyc2lvbiBvZiBJLkUuIHRoYXQgbXVzdCBoYXZlIHRoZSBnbG9iYWwgb2JqZWN0IGZvciAndGhpcycsIGhvcGZ1bGx5IG91ciBjb250ZXh0IGNvcnJlY3Qgb3RoZXJ3aXNlIGl0IHdpbGwgdGhyb3cgYSBnbG9iYWwgZXJyb3JcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwodGhpcywgZnVuLCAwKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG59XG5mdW5jdGlvbiBydW5DbGVhclRpbWVvdXQobWFya2VyKSB7XG4gICAgaWYgKGNhY2hlZENsZWFyVGltZW91dCA9PT0gY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gY2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfVxuICAgIC8vIGlmIGNsZWFyVGltZW91dCB3YXNuJ3QgYXZhaWxhYmxlIGJ1dCB3YXMgbGF0dGVyIGRlZmluZWRcbiAgICBpZiAoKGNhY2hlZENsZWFyVGltZW91dCA9PT0gZGVmYXVsdENsZWFyVGltZW91dCB8fCAhY2FjaGVkQ2xlYXJUaW1lb3V0KSAmJiBjbGVhclRpbWVvdXQpIHtcbiAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xuICAgICAgICByZXR1cm4gY2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIC8vIHdoZW4gd2hlbiBzb21lYm9keSBoYXMgc2NyZXdlZCB3aXRoIHNldFRpbWVvdXQgYnV0IG5vIEkuRS4gbWFkZG5lc3NcbiAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH0gY2F0Y2ggKGUpe1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gV2hlbiB3ZSBhcmUgaW4gSS5FLiBidXQgdGhlIHNjcmlwdCBoYXMgYmVlbiBldmFsZWQgc28gSS5FLiBkb2Vzbid0ICB0cnVzdCB0aGUgZ2xvYmFsIG9iamVjdCB3aGVuIGNhbGxlZCBub3JtYWxseVxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKG51bGwsIG1hcmtlcik7XG4gICAgICAgIH0gY2F0Y2ggKGUpe1xuICAgICAgICAgICAgLy8gc2FtZSBhcyBhYm92ZSBidXQgd2hlbiBpdCdzIGEgdmVyc2lvbiBvZiBJLkUuIHRoYXQgbXVzdCBoYXZlIHRoZSBnbG9iYWwgb2JqZWN0IGZvciAndGhpcycsIGhvcGZ1bGx5IG91ciBjb250ZXh0IGNvcnJlY3Qgb3RoZXJ3aXNlIGl0IHdpbGwgdGhyb3cgYSBnbG9iYWwgZXJyb3IuXG4gICAgICAgICAgICAvLyBTb21lIHZlcnNpb25zIG9mIEkuRS4gaGF2ZSBkaWZmZXJlbnQgcnVsZXMgZm9yIGNsZWFyVGltZW91dCB2cyBzZXRUaW1lb3V0XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwodGhpcywgbWFya2VyKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG5cbn1cbnZhciBxdWV1ZSA9IFtdO1xudmFyIGRyYWluaW5nID0gZmFsc2U7XG52YXIgY3VycmVudFF1ZXVlO1xudmFyIHF1ZXVlSW5kZXggPSAtMTtcblxuZnVuY3Rpb24gY2xlYW5VcE5leHRUaWNrKCkge1xuICAgIGlmICghZHJhaW5pbmcgfHwgIWN1cnJlbnRRdWV1ZSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgaWYgKGN1cnJlbnRRdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgcXVldWUgPSBjdXJyZW50UXVldWUuY29uY2F0KHF1ZXVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XG4gICAgfVxuICAgIGlmIChxdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgZHJhaW5RdWV1ZSgpO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gZHJhaW5RdWV1ZSgpIHtcbiAgICBpZiAoZHJhaW5pbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgdGltZW91dCA9IHJ1blRpbWVvdXQoY2xlYW5VcE5leHRUaWNrKTtcbiAgICBkcmFpbmluZyA9IHRydWU7XG5cbiAgICB2YXIgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIHdoaWxlKGxlbikge1xuICAgICAgICBjdXJyZW50UXVldWUgPSBxdWV1ZTtcbiAgICAgICAgcXVldWUgPSBbXTtcbiAgICAgICAgd2hpbGUgKCsrcXVldWVJbmRleCA8IGxlbikge1xuICAgICAgICAgICAgaWYgKGN1cnJlbnRRdWV1ZSkge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRRdWV1ZVtxdWV1ZUluZGV4XS5ydW4oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XG4gICAgICAgIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB9XG4gICAgY3VycmVudFF1ZXVlID0gbnVsbDtcbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIHJ1bkNsZWFyVGltZW91dCh0aW1lb3V0KTtcbn1cblxucHJvY2Vzcy5uZXh0VGljayA9IGZ1bmN0aW9uIChmdW4pIHtcbiAgICB2YXIgYXJncyA9IG5ldyBBcnJheShhcmd1bWVudHMubGVuZ3RoIC0gMSk7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBhcmdzW2kgLSAxXSA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBxdWV1ZS5wdXNoKG5ldyBJdGVtKGZ1biwgYXJncykpO1xuICAgIGlmIChxdWV1ZS5sZW5ndGggPT09IDEgJiYgIWRyYWluaW5nKSB7XG4gICAgICAgIHJ1blRpbWVvdXQoZHJhaW5RdWV1ZSk7XG4gICAgfVxufTtcblxuLy8gdjggbGlrZXMgcHJlZGljdGlibGUgb2JqZWN0c1xuZnVuY3Rpb24gSXRlbShmdW4sIGFycmF5KSB7XG4gICAgdGhpcy5mdW4gPSBmdW47XG4gICAgdGhpcy5hcnJheSA9IGFycmF5O1xufVxuSXRlbS5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuZnVuLmFwcGx5KG51bGwsIHRoaXMuYXJyYXkpO1xufTtcbnByb2Nlc3MudGl0bGUgPSAnYnJvd3Nlcic7XG5wcm9jZXNzLmJyb3dzZXIgPSB0cnVlO1xucHJvY2Vzcy5lbnYgPSB7fTtcbnByb2Nlc3MuYXJndiA9IFtdO1xucHJvY2Vzcy52ZXJzaW9uID0gJyc7IC8vIGVtcHR5IHN0cmluZyB0byBhdm9pZCByZWdleHAgaXNzdWVzXG5wcm9jZXNzLnZlcnNpb25zID0ge307XG5cbmZ1bmN0aW9uIG5vb3AoKSB7fVxuXG5wcm9jZXNzLm9uID0gbm9vcDtcbnByb2Nlc3MuYWRkTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5vbmNlID0gbm9vcDtcbnByb2Nlc3Mub2ZmID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVBbGxMaXN0ZW5lcnMgPSBub29wO1xucHJvY2Vzcy5lbWl0ID0gbm9vcDtcblxucHJvY2Vzcy5iaW5kaW5nID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuYmluZGluZyBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xuXG5wcm9jZXNzLmN3ZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICcvJyB9O1xucHJvY2Vzcy5jaGRpciA9IGZ1bmN0aW9uIChkaXIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuY2hkaXIgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcbnByb2Nlc3MudW1hc2sgPSBmdW5jdGlvbigpIHsgcmV0dXJuIDA7IH07XG4iLCJ2YXIgTmF2V3JhcHBlciA9IHJlcXVpcmUoJy4vY29tcG9uZW50cy9OYXZXcmFwcGVyJyk7XG5cbnNlc3Npb25TdG9yYWdlLmVudiA9IHByb2Nlc3MuZW52LmVudjtcbnNlc3Npb25TdG9yYWdlLmFwaSA9IHByb2Nlc3MuZW52LmFwaTtcbnNlc3Npb25TdG9yYWdlLm1lbnVhcGkgPSBwcm9jZXNzLmVudi5tZW51YXBpO1xuc2Vzc2lvblN0b3JhZ2UubWVudXZpZXdhcGk9cHJvY2Vzcy5lbnYubWVudXZpZXdhcGk7XG5cbnZhciBlID0gc2Vzc2lvblN0b3JhZ2UuZW52ID8gc2Vzc2lvblN0b3JhZ2UuZW52IDogJ3Byb2QnO1xudmFyIHUgPSBzZXNzaW9uU3RvcmFnZS51c2VyPyBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLnVzZXIpIDoge307XG53aW5kb3cuQXBwID0ge1xuXHRlbnZpcm9ubWVudDogZSxcblx0dXNlcjogdSxcblxuXHRhcGk6IGZ1bmN0aW9uKCkge1xuXHRcdHN3aXRjaCAod2luZG93LkFwcC5lbnZpcm9ubWVudCkge1xuXHRcdFx0Y2FzZSAnZGV2Mic6XG5cdFx0XHRcdHJldHVybiAnZGV2MmNsaWVudGFwaS8nO1xuXHRcdFx0Y2FzZSAnc3RhZ2UnOlxuXHRcdFx0XHRyZXR1cm4gJ3N0YWdlY2xpZW50YXBpLyc7XG5cdFx0XHRjYXNlICdsb2NhbCc6XG5cdFx0XHRcdHJldHVybiAnbG9jYWxjbGllbnRhcGkvJztcblx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdHJldHVybiAncHJvZGNsaWVudGFwaS8nO1xuXHRcdH1cblx0fSxcblxuXHRtZW51YXBpOiBmdW5jdGlvbigpIHtcblx0XHRzd2l0Y2ggKHdpbmRvdy5BcHAuZW52aXJvbm1lbnQpIHtcblx0XHRcdGNhc2UgJ2RldjInOlxuXHRcdFx0XHRyZXR1cm4gJ21lbnVkZXYyYXBpLyc7XG5cdFx0XHRjYXNlICdzdGFnZSc6XG5cdFx0XHRcdHJldHVybiAnbWVudXN0YWdlYXBpLyc7XG5cdFx0XHRjYXNlICdsb2NhbCc6XG5cdFx0XHRcdHJldHVybiAnbWVudWxvY2FsYXBpLyc7XG5cdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRyZXR1cm4gJ21lbnVwcm9kYXBpLyc7XG5cdFx0fVxuXHR9LFxuXG5cdHV0Y1RvTG9jYWw6IGZ1bmN0aW9uKHV0Y1RpbWUpIHtcblx0XHRpZiAoIXV0Y1RpbWUubWF0Y2goL1okLykpIHtcblx0XHRcdHV0Y1RpbWUgPSB1dGNUaW1lICsgJyBaJztcblx0XHR9XG5cdFx0dmFyIGQgPSBuZXcgRGF0ZSh1dGNUaW1lKTtcblx0XHRyZXR1cm4gZC50b1N0cmluZygpLnJlcGxhY2UoL1xcc0dNVC1cXGRcXGRcXGRcXGQvLCAnJyk7XG5cdH0sXG5cblx0YXV0aDogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHdpbmRvdy5BcHAudXNlci5kYXNoZXJfdXNlcl9pZCAhPT0gdW5kZWZpbmVkO1xuXHR9LFxuXG5cdG5vdGlmeTogZnVuY3Rpb24oKSB7XG5cdFx0Ly8gT3ZlcmxvYWQgdGhpcyBmdW5jdGlvblxuXHR9LFxuXG5cdHdhcm46IGZ1bmN0aW9uKCkge1xuXHRcdC8vIE92ZXJsb2FkIHRoaXMgZnVuY3Rpb25cblx0fSxcblxuXHRjbG9zZU1lc3NhZ2U6IGZ1bmN0aW9uKCkge1xuXHRcdC8vIE92ZXJsb2FkIHRoaXMgZnVuY2l0b25cblx0fVxufTtcblxuUmVhY3QucmVuZGVyKDxOYXZXcmFwcGVyIC8+LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYXBwJykpOyIsInZhciBMb2dvdXQgPSByZXF1aXJlKCcuL0xvZ291dCcpO1xudmFyIERhc2hib2FyZCA9IHJlcXVpcmUoJy4vRGFzaGJvYXJkJyk7XG5cbnZhciBEYXNoYm9hcmQgPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG5cdGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGFwcFN0YXRlOiAnbG9naW4nXG5cdFx0fVxuXHR9LFxuXG4gIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwid3JhcHBlclwiPlxuICAgICAgICA8TG9nb3V0IC8+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IERhc2hib2FyZDtcbiIsIlxudmFyIEhlYWRlciA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcbiAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbW9iaWxlQWN0aXZlOiBmYWxzZVxuICAgIH0gIFxuICB9LFxuXG4gIHJlbmRlck9wdGlvbjogZnVuY3Rpb24ob3B0aW9uKSB7XG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgIHJldHVybiAoXG4gICAgICA8bGkgXG4gICAgICAgIG9uQ2xpY2s9eyBmdW5jdGlvbigpIHtcbiAgICAgICAgICB0aGF0LnByb3BzLmNoYW5nZVZpZXcob3B0aW9uKTtcbiAgICAgICAgICB0aGF0LnNldFN0YXRlKHsgbW9iaWxlQWN0aXZlOiBmYWxzZSB9KTtcbiAgICAgICAgfX0gXG4gICAgICAgIGtleT17b3B0aW9ufT5cbiAgICAgICAge29wdGlvbi50b1VwcGVyQ2FzZSgpfVxuICAgICAgPC9saT5cbiAgICApO1xuICB9LFxuXG4gIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgIHZhciBvcHRpb25zID0gW107XG4gICAgb3B0aW9ucyA9IHRoaXMucHJvcHMubWVudU9wdGlvbnMubWFwKHRoaXMucmVuZGVyT3B0aW9uKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8aGVhZGVyIGNsYXNzTmFtZT17IHRoYXQuc3RhdGUubW9iaWxlQWN0aXZlID8gJ21vYmlsZS1hY3RpdmUnIDogJycgfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtZW51LWxpbmtcIiBvbkNsaWNrPXsgZnVuY3Rpb24oKSB7dGhhdC5zZXRTdGF0ZSh7bW9iaWxlQWN0aXZlOiF0aGF0LnN0YXRlLm1vYmlsZUFjdGl2ZX0pfSB9PlxuICAgICAgICAgIE1FTlVcbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDx1bD5cbiAgICAgICAgICB7b3B0aW9uc31cbiAgICAgICAgPC91bD5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJlbnYtcmlnaHRcIj5cbiAgICAgICAgICBbeyBBcHAuZW52aXJvbm1lbnQgfV1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2hlYWRlcj5cbiAgICApXG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IEhlYWRlcjtcbiIsIlxudmFyIExvZ291dCA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcblxuXHRjb21wb25lbnRXaWxsTW91bnQ6IGZ1bmN0aW9uKCkge1xuXHRcdHRoaXMucmVmID0gbmV3IEZpcmViYXNlKCdodHRwczovL2Rhc2hlci1hcHAtc3RhZ2luZy5maXJlYmFzZWlvLmNvbScpO1xuXHR9LFxuXG5cblx0dW5hdXRoZW50aWNhdGU6IGZ1bmN0aW9uKCkge1xuXHRcdHRoaXMucmVmLnVuYXV0aCgpO1xuXHRcdHNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XG5cdFx0d2luZG93LmxvY2F0aW9uID0gXCIvXCI7XG5cdH0sXG5cblxuICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImxvZ291dC13cmFwcGVyXCI+XG4gICAgICAgIDxidXR0b24gb25DbGljaz17dGhpcy51bmF1dGhlbnRpY2F0ZX0+TG9nIE91dDwvYnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBMb2dvdXQ7XG4iLCJ2YXIgTWVzc2FnaW5nID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xuICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICBhY3RpdmU6IGZhbHNlLFxuICAgICAgdHlwZTogJ25vdGlmeScsXG4gICAgICBtZXNzYWdlOiAnJ1xuICAgIH07XG4gIH0sXG5cbiAgY29tcG9uZW50V2lsbE1vdW50OiBmdW5jdGlvbigpIHtcbiAgICBBcHAubm90aWZ5ID0gdGhpcy5ub3RpZnk7XG4gICAgQXBwLndhcm4gPSB0aGlzLndhcm47XG4gICAgQXBwLmNsb3NlTWVzc2FnZSA9IHRoaXMuY2xvc2U7XG4gIH0sXG5cbiAgbm90aWZ5OiBmdW5jdGlvbihtc2cpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGFjdGl2ZTogdHJ1ZSxcbiAgICAgIG1lc3NhZ2U6IG1zZyxcbiAgICAgIHR5cGU6ICdub3RpZnknXG4gICAgfSk7XG4gIH0sXG5cbiAgd2FybjogZnVuY3Rpb24obXNnKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBhY3RpdmU6IHRydWUsXG4gICAgICBtZXNzYWdlOiBtc2csXG4gICAgICB0eXBlOiAnd2FybidcbiAgICB9KTtcbiAgfSxcblxuICBjbG9zZTogZnVuY3Rpb24oKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBhY3RpdmU6IGZhbHNlXG4gICAgfSk7XG4gIH0sXG5cbiAgaWdub3JlOiBmdW5jdGlvbihldnQpIHtcbiAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gIH0sXG5cbiAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgY2xhc3NOYW1lID0gdGhpcy5zdGF0ZS5hY3RpdmUgPyAnJyA6ICcgaW52aXNpYmxlJztcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBvbkNsaWNrPXt0aGlzLmNsb3NlfSBjbGFzc05hbWU9eyBcIm1lc3NhZ2luZyBcIiArIHRoaXMuc3RhdGUudHlwZSArIGNsYXNzTmFtZSB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1lc3NhZ2VcIiBvbkNsaWNrPXt0aGlzLmlnbm9yZX0+XG4gICAgICAgICAgeyB0aGlzLnN0YXRlLm1lc3NhZ2UgfVxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2xvc2VcIiBvbkNsaWNrPXt0aGlzLmNsb3NlfT54PC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBNZXNzYWdpbmc7XG4iLCJ2YXIgSGVhZGVyID0gcmVxdWlyZSgnLi9IZWFkZXInKTtcbnZhciBNZXNzYWdpbmcgPSByZXF1aXJlKCcuL01lc3NhZ2luZycpO1xudmFyIERhc2hib2FyZCA9IHJlcXVpcmUoJy4vRGFzaGJvYXJkJyk7XG52YXIgUmVmdW5kID0gcmVxdWlyZSgnLi9SZWZ1bmQnKTtcbnZhciBSZXNvbHV0aW9uID0gcmVxdWlyZSgnLi9SZXNvbHV0aW9uJyk7XG52YXIgVXNlcnMgPSByZXF1aXJlKCcuL1VzZXJzJyk7XG52YXIgUHJvbW90aW9uID0gcmVxdWlyZSgnLi9Qcm9tb3Rpb24nKTtcblxudmFyIE5hdldyYXBwZXIgPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG5cdGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGFwcFN0YXRlOiAnZGFzaGJvYXJkJyxcblx0XHR9XG5cdH0sXG5cblx0bWVudU9wdGlvbnM6IFtcbiAgICAnZGFzaGJvYXJkJyxcbiAgICAndXNlcnMnLFxuICAgICdyZWZ1bmRzJyxcbiAgICAncmVzb2x1dGlvbicsXG4gICAgJ3Byb21vdGlvbnMnXG4gIF0sXG5cblx0c2hvd1ZpZXc6IGZ1bmN0aW9uKHZpZXcpIHtcblx0XHR0aGlzLnNldFN0YXRlKHthcHBTdGF0ZTogdmlld30pO1xuXHR9LFxuXG5cdHJlbmRlclZpZXc6IGZ1bmN0aW9uKCkge1xuXHRcdHZhciB2aWV3O1xuXHRcdHN3aXRjaCh0aGlzLnN0YXRlLmFwcFN0YXRlKSB7XG5cdFx0XHRjYXNlICdkYXNoYm9hcmQnOlxuXHRcdFx0XHR2aWV3ID0gPERhc2hib2FyZCAvPjtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICd1c2Vycyc6XG5cdFx0XHRcdHZpZXcgPSA8VXNlcnMgLz47XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAncmVmdW5kcyc6XG5cdFx0XHRcdHZpZXcgPSA8UmVmdW5kIC8+O1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgJ3Jlc29sdXRpb24nOlxuXHRcdFx0XHR2aWV3ID0gPFJlc29sdXRpb24gLz47XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAncHJvbW90aW9ucyc6XG5cdFx0XHRcdHZpZXcgPSA8UHJvbW90aW9uIC8+O1xuXHRcdFx0XHRicmVhaztcblx0XHR9XG5cdFx0cmV0dXJuIHZpZXc7XG5cdH0sXG5cbiAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgXHR2YXIgaGVhZGVyID0gXG4gIFx0XHQ8ZGl2PjxIZWFkZXIgbWVudU9wdGlvbnM9eyB0aGlzLm1lbnVPcHRpb25zIH0gY2hhbmdlVmlldz17dGhpcy5zaG93Vmlld30gLz5cblx0ICBcdDxNZXNzYWdpbmcgLz48L2Rpdj47XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwid3JhcHBlclwiPlxuICAgIFx0XHR7IGhlYWRlciB9ICAgICAgXHRcbiAgICAgICAgeyB0aGlzLnJlbmRlclZpZXcoKSB9XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE5hdldyYXBwZXI7XG4iLCJ2YXIgUHJvbW90aW9uID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xuXG4gIGJhc2VQYXRoOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gQXBwLmFwaSgpICsgJ3YyLyc7XG4gIH0sXG5cbiAgY29tcG9uZW50V2lsbFVwZGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgQXBwLmNsb3NlTWVzc2FnZSgpO1xuICB9LFxuXG4gIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGFjdGlvbjogJ2dpdmVQcm9tbycsIC8vIE1hdGNoIGZ1bmN0aW9uIG5hbWVcbiAgICAgIHByb21vQ29kZTogJycsXG4gICAgICB1c2VySWQ6ICBlbmNvZGVVUkkoQXBwLnVzZXIuZGFzaGVyX3VzZXJfaWQpLFxuICAgICAgYmVuZWZpY2lhcnlVc2VySWQ6ICcnLFxuICAgICAgYmVuZWZpdFZhbHVlOiAnJ1xuICAgIH07XG4gIH0sXG5cblxuICAvL1xuICAvLyBOZXR3b3JrIGZldGNoIGZ1bmN0aW9uc1xuICAvL1xuICBmZXRjaDogZnVuY3Rpb24oYXBpUGF0aCkge1xuICAgIGNvbnNvbGUubG9nKCdGZXRjaDogJywgdGhpcy5iYXNlUGF0aCgpICsgYXBpUGF0aCk7XG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgICQuYWpheCh7XG4gICAgICB1cmw6IHRoaXMuYmFzZVBhdGgoKSArIGFwaVBhdGgsXG4gICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgc3VjY2VzczogdGhhdC5mZXRjaFN1Y2Nlc3MsXG4gICAgICBlcnJvcjogdGhhdC5mZXRjaEVycm9yXG4gICAgfSk7XG4gIH0sXG5cbiAgZmV0Y2hTdWNjZXNzOiBmdW5jdGlvbihkYXRhKSB7XG4gICAgaWYgKGRhdGEuZXJyb3JzKSB7XG4gICAgICBBcHAud2FybihkYXRhLmVycm9yc1swXSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIEFwcC5ub3RpZnkoJ1Byb21vdGlvbiBzdWNjZWVkZWQnKTtcbiAgICAgICQoJ3NlY3Rpb24ucHJvbW90aW9ucyBpbnB1dCcpLnZhbCgnJyk7XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICB9LFxuXG4gIGZldGNoRXJyb3I6IGZ1bmN0aW9uKGVycikge1xuICAgIGlmIChlcnIucmVzcG9uc2VKU09OICYmIGVyci5yZXNwb25zZUpTT04uZXJyb3JzICYmIGVyci5yZXNwb25zZUpTT04uZXJyb3JzLmxlbmd0aCkge1xuICAgICAgQXBwLndhcm4oXCJQcm9tb3Rpb24gZmFpbGVkOlxcblwiICsgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcnNbMF0pO1xuICAgIH1cbiAgICBpZiAoZXJyLnJlc3BvbnNlSlNPTiAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9yX2NvZGUpIHtcbiAgICAgIEFwcC53YXJuKFwiUHJvbW90aW9uIGZhaWxlZDpcXG5cIiArIGVyci5yZXNwb25zZUpTT04uZXJyb3JfY29kZSArICc6ICcgKyBlcnIucmVzcG9uc2VKU09OLm1lc3NhZ2UpO1xuICAgIH0gZWxzZSB7XG4gICAgICBBcHAud2FybihcIlByb21vdGlvbiByZXF1ZXN0IGZhaWxlZC5cIik7XG4gICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgIH1cbiAgfSxcblxuXG4gIC8vXG4gIC8vIEZvcm0gdXBkYXRlIGZ1bmNpdG9uc1xuICAvL1xuICB1cGRhdGVQcm9tb0NvZGU6IGZ1bmN0aW9uKGV2dCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgcHJvbW9Db2RlOiBldnQudGFyZ2V0LnZhbHVlXG4gICAgfSk7XG4gIH0sXG5cbiAgdXBkYXRlVXNlcklkOiBmdW5jdGlvbihldnQpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGJlbmVmaWNpYXJ5VXNlcklkOiBlbmNvZGVVUkkoZXZ0LnRhcmdldC52YWx1ZSlcbiAgICB9KTtcbiAgfSxcblxuICB1cGRhdGVCZW5lZml0OiBmdW5jdGlvbihldnQpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGJlbmVmaXRWYWx1ZTogZXZ0LnRhcmdldC52YWx1ZVxuICAgIH0pO1xuICB9LFxuXG5cbiAgc2V0QWN0aW9uOiBmdW5jdGlvbihldnQpIHtcbiAgICB2YXIgYWN0aW9uRnVuY2l0b24gPSBldnQudGFyZ2V0LnZhbHVlXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBhY3Rpb246IGFjdGlvbkZ1bmNpdG9uXG4gICAgfSk7XG4gIH0sXG5cblxuICBpc3N1ZVByb21vOiBmdW5jdGlvbigpIHtcbiAgICB0aGlzW3RoaXMuc3RhdGUuYWN0aW9uXSgpO1xuICB9LFxuXG5cbiAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgcHJvbW9DYXNoID0gdGhpcy5zdGF0ZS5hY3Rpb24gPT09ICdnaXZlUHJvbW9DYXNoJztcblxuICAgIHJldHVybiAoIDxzZWN0aW9uIGNsYXNzTmFtZSA9IFwicHJvbW90aW9uc1wiPlxuICAgICAgPGgxPkdpdmUgYSBQcm9tb3Rpb248L2gxPlxuICAgICAgPHA+WW91IGNhbiBnaXZlIHVzZXJzIHByb21vdGlvbnMgdXNpbmcgdGhlIGZvcm1zIGJlbG93PC9wPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtXCI+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGxhYmVsPkNob29zZSBhIHJlZnVuZCB0eXBlOjwvbGFiZWw+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzZWxlY3RcIj5cbiAgICAgICAgICAgIDxzZWxlY3Qgb25DaGFuZ2U9e3RoaXMuc2V0QWN0aW9ufT5cbiAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImdpdmVQcm9tb1wiPlxuICAgICAgICAgICAgICAgIEdpdmUgdXNlciBwcm9tbyBjb2RlXG4gICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiZ2l2ZVByb21vQ2FzaFwiPkdpdmUgdXNlciBwcm9tbyBjYXNoPC9vcHRpb24+XG4gICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9eyBwcm9tb0Nhc2ggPyAnaGlkZGVuJyA6ICcnIH0+XG4gICAgICAgICAgPGxhYmVsPlByb21vIENvZGU6IDxzcGFuIGNsYXNzTmFtZT1cInJlcVwiPio8L3NwYW4+PC9sYWJlbD5cbiAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgbmFtZSA9IFwicHJvbW8tY29kZVwiXG4gICAgICAgICAgICBvbkNoYW5nZT17IHRoaXMudXBkYXRlUHJvbW9Db2RlIH0gLz5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8bGFiZWw+VXNlciBJRDo8c3BhbiBjbGFzc05hbWU9XCJyZXFcIj4qPC9zcGFuPjwvbGFiZWw+XG4gICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgIG5hbWU9XCJ1c2VyLWlkXCJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsgdGhpcy51cGRhdGVVc2VySWQgfS8+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgcHJvbW9DYXNoID8gJycgOiAnaGlkZGVuJyB9PlxuICAgICAgICAgIDxsYWJlbD5CZW5lZml0IFZhbHVlOiA8c3BhbiBjbGFzc05hbWU9XCJyZXFcIiA+Kjwvc3Bhbj48L2xhYmVsPlxuICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICBuYW1lPVwiYmVuZWZpdC12YWx1ZVwiXG4gICAgICAgICAgICBvbkNoYW5nZT17IHRoaXMudXBkYXRlQmVuZWZpdCB9Lz5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXR0b25cIiBvbkNsaWNrID0geyB0aGlzLmlzc3VlUHJvbW8gfT5Jc3N1ZSBQcm9tbzwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9zZWN0aW9uPik7XG4gIH0sXG5cblxuICAvL1xuICAvLyBBdmFpbGFibGUgYWN0aW9uc1xuICAvL1xuICBnaXZlUHJvbW86IGZ1bmN0aW9uKCkge1xuICAgIGlmICghdGhpcy5zdGF0ZS5wcm9tb0NvZGUpIHtcbiAgICAgIHJldHVybiBBcHAud2FybignWW91IG11c3QgZW5kZXIgYSB2YWxpZCBwcm9tbyBjb2RlLicpO1xuICAgIH1cbiAgICBpZiAoIXRoaXMuc3RhdGUuYmVuZWZpY2lhcnlVc2VySWQpIHtcbiAgICAgIHJldHVybiBBcHAud2FybignWW91IG11c3QgZW5kZXIgYSBVc2VyIElELicpO1xuICAgIH1cblxuICAgIHZhciBwYXRoID1cbiAgICAgICdwcm9tb3Rpb24vY291cG9uQ29kZS8nICtcbiAgICAgIHRoaXMuc3RhdGUucHJvbW9Db2RlICtcbiAgICAgICcvYXBwbHk/ZGFzaGVyVXNlcklkPScgK1xuICAgICAgdGhpcy5zdGF0ZS51c2VySWQgKyBcbiAgICAgICcmYmVuZWZpY2lhcnlVc2VySWQ9JyArXG4gICAgICB0aGlzLnN0YXRlLmJlbmVmaWNpYXJ5VXNlcklkO1xuICAgIHRoaXMuZmV0Y2gocGF0aCk7XG4gIH0sXG5cbiAgZ2l2ZVByb21vQ2FzaDogZnVuY3Rpb24oKSB7XG4gICAgaWYgKCF0aGlzLnN0YXRlLmJlbmVmaWNpYXJ5VXNlcklkKSB7XG4gICAgICByZXR1cm4gQXBwLndhcm4oJ1lvdSBtdXN0IGVuZGVyIGEgdXNlciBJRC4nKTtcbiAgICB9XG4gICAgaWYgKCF0aGlzLnN0YXRlLmJlbmVmaXRWYWx1ZSkge1xuICAgICAgcmV0dXJuIEFwcC53YXJuKCdZb3UgbXVzdCBlbmRlciBhIGJlbmVmaXQgdmFsdWUuJyk7XG4gICAgfVxuXG4gICAgdmFyIHBhdGggPVxuICAgICAgJ3Byb21vdGlvbi9jb3Vwb25Db2RlL0RZTkFNSUNfQ0FTSC9hcHBseT9kYXNoZXJVc2VySWQ9JyArXG4gICAgICB0aGlzLnN0YXRlLnVzZXJJZCArXG4gICAgICAnJmJlbmVmaXRWYWx1ZT0nICtcbiAgICAgIHRoaXMuc3RhdGUuYmVuZWZpdFZhbHVlKyBcbiAgICAgICcmYmVuZWZpY2lhcnlVc2VySWQ9JyArXG4gICAgICB0aGlzLnN0YXRlLmJlbmVmaWNpYXJ5VXNlcklkO1xuICAgIHRoaXMuZmV0Y2gocGF0aCk7XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFByb21vdGlvbjtcbiIsInZhciBSZWZ1bmQgPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG4gICAgICBiYXNlUGF0aDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBBcHAuYXBpKCkgKyAndjEvcnVuLyc7XG4gICAgICB9LFxuXG4gICAgICBjb21wb25lbnRXaWxsVXBkYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgLy8gQXBwLmNsb3NlTWVzc2FnZSgpO1xuICAgICAgfSxcblxuICAgICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzZXRTdGF0ZSgpO1xuICAgICAgfSxcblxuICAgICAgcmVzZXRTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgYWN0aW9uOiAnT25seVJlZnVuZEJ1eWVyJywgLy8gTWF0Y2ggZnVuY3Rpb24gbmFtZVxuICAgICAgICAgIHJ1bklkOiAnJyxcbiAgICAgICAgICBkYXNoZXJJZDogQXBwLnVzZXIuZGFzaGVyX3VzZXJfaWQgLCAvLyBEZWZhdWx0IHRvIEFkbWluIElEXG4gICAgICAgICAgb3JkZXJJZDogJycsXG4gICAgICAgICAgcnVubmVyTXNnOiAnJyxcbiAgICAgICAgICBidXllck1zZzogJycsXG4gICAgICAgICAgZGVzY3JpcHRpb246ICcnLFxuICAgICAgICAgIHJlZnVuZEFtb3VudDogMCxcbiAgICAgICAgICBwYXJ0aWFsOiBmYWxzZVxuICAgICAgICB9O1xuICAgICAgfSxcblxuXG4gICAgICAvL1xuICAgICAgLy8gTmV0d29yayBmZXRjaCBmdW5jdGlvbnNcbiAgICAgIC8vXG4gICAgICBmZXRjaDogZnVuY3Rpb24oYXBpUGF0aCkge1xuICAgICAgICB2YXIgZGF0YSA9IHRoaXMuYnVpbGRGZXRjaERhdGEoKTtcbiAgICAgICAgaWYgKGRhdGEgPT09IG51bGwpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY29uc29sZS5sb2coJ0ZldGNoOiAnLCB0aGlzLmJhc2VQYXRoKCkgKyBhcGlQYXRoKTtcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICB1cmw6IHRoaXMuYmFzZVBhdGgoKSArIGFwaVBhdGgsXG4gICAgICAgICAgdHlwZTogJ1BVVCcsXG4gICAgICAgICAgY29udGVudFR5cGU6ICdhcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ9dXRmLTgnLFxuICAgICAgICAgIC8vIGRhdGFUeXBlOiAnanNvbicsXG4gICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZGF0YSksXG4gICAgICAgICAgc3VjY2VzczogdGhhdC5mZXRjaFN1Y2Nlc3MsXG4gICAgICAgICAgZXJyb3I6IHRoYXQuZmV0Y2hFcnJvclxuICAgICAgICB9KTtcbiAgICAgIH0sXG5cbiAgICAgIGZldGNoU3VjY2VzczogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICBpZiAoZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICBBcHAud2FybihkYXRhLmVycm9yc1swXSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgQXBwLm5vdGlmeSgnUmVmdW5kIHN1Y2NlZWRlZCcpO1xuICAgICAgICAgICQoXCJzZWN0aW9uLnJlZnVuZHMgc2VsZWN0XCIpWzBdLnNlbGVjdGVkSW5kZXggPSAwO1xuICAgICAgICAgICQoJ3NlY3Rpb24ucmVmdW5kcyBpbnB1dCcpLnZhbCgnJyk7XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSggdGhpcy5yZXNldFN0YXRlKCkgKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgIH0sXG5cbiAgICAgIGZldGNoRXJyb3I6IGZ1bmN0aW9uKGVycikge1xuICAgICAgICBpZiAoZXJyLnJlc3BvbnNlSlNPTiAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9ycyAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9ycy5sZW5ndGgpIHtcbiAgICAgICAgICBBcHAud2FybihcIlJlZnVuZCBmYWlsZWQ6XFxuXCIgKyBlcnIucmVzcG9uc2VKU09OLmVycm9yc1swXSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGVyci5yZXNwb25zZUpTT04gJiYgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcl9jb2RlKSB7XG4gICAgICAgICAgQXBwLndhcm4oXCJSZWZ1bmQgZmFpbGVkOlxcblwiICsgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcl9jb2RlICsgJzogJyArIGVyci5yZXNwb25zZUpTT04ubWVzc2FnZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgQXBwLndhcm4oXCJSZWZ1bmQgcmVxdWVzdCBmYWlsZWQuXCIpO1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgIH1cbiAgICAgIH0sXG5cbiAgICAgIGJ1aWxkRmV0Y2hEYXRhOiBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJ1bklkKSB7XG4gICAgICAgICAgQXBwLndhcm4oJ1lvdSBtdXN0IGVudGVyIGEgUnVuIElELicpO1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5vcmRlcklkKSB7XG4gICAgICAgICAgQXBwLndhcm4oJ1lvdSBtdXN0IGVudGVyIGFuIE9yZGVyIElELicpO1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5zdGF0ZS5kZXNjcmlwdGlvbikge1xuICAgICAgICAgIEFwcC53YXJuKCdZb3UgbXVzdCBlbnRlciBhIGRlc2NyaXB0aW9uIGZvciBpbnRlcm5hbCBKb3lydW4gdXNlLicpO1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGRhdGEgPSB7XG4gICAgICAgICAgYWN0aW9uX3Rha2VuOiB0aGlzLnN0YXRlLmFjdGlvbi5yZXBsYWNlKC9eUGFydGlhbC8sICcnKSxcbiAgICAgICAgICBkYXNoZXJfdXNlcl9pZDogdGhpcy5zdGF0ZS5kYXNoZXJJZCxcbiAgICAgICAgICBkZXNjcmlwdGlvbjogdGhpcy5zdGF0ZS5kZXNjcmlwdGlvbixcbiAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgIHJ1bm5lcl9tZXNzYWdlOiB0aGlzLnN0YXRlLnJ1bm5lck1zZyxcbiAgICAgICAgICAgIGJ1eWVyX21lc3NhZ2U6IHRoaXMuc3RhdGUuYnV5ZXJNc2dcbiAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnBhcnRpYWwpIHtcbiAgICAgICAgICBkYXRhLnBhcmFtcy5yZWZ1bmRfYW1vdW50ID0gTnVtYmVyKHRoaXMuc3RhdGUucmVmdW5kQW1vdW50KTtcbiAgICAgICAgICBpZiAoIWRhdGEucGFyYW1zLnJlZnVuZF9hbW91bnQpIHtcbiAgICAgICAgICAgIEFwcC53YXJuKCdZb3UgbXVzdCBlbnRlciBhIHJlZnVuZCBhbW91bnQuJyk7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICB9LFxuXG5cbiAgICAgIC8vXG4gICAgICAvLyBGb3JtIHVwZGF0ZSBmdW5jaXRvbnNcbiAgICAgIC8vXG4gICAgICB1cGRhdGVSdW5JZDogZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHJ1bklkOiBldnQudGFyZ2V0LnZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgfSxcblxuICAgICAgdXBkYXRlT3JkZXJJZDogZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIG9yZGVySWQ6IGV2dC50YXJnZXQudmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9LFxuXG4gICAgICB1cGRhdGVEZXNjOiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgZGVzY3JpcHRpb246IGV2dC50YXJnZXQudmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9LFxuXG4gICAgICB1cGRhdGVSdW5Nc2c6IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBydW5uZXJNc2c6IGV2dC50YXJnZXQudmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9LFxuXG4gICAgICB1cGRhdGVCdXlNc2c6IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBidXllck1zZzogZXZ0LnRhcmdldC52YWx1ZVxuICAgICAgICB9KTtcbiAgICAgIH0sXG5cbiAgICAgIHVwZGF0ZVJlZnVuZEFtdDogZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHJlZnVuZEFtb3VudDogZXZ0LnRhcmdldC52YWx1ZVxuICAgICAgICB9KTtcbiAgICAgIH0sXG5cblxuICAgICAgc2V0QWN0aW9uOiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgdmFyIGFjdGlvbkZ1bmNpdG9uID0gZXZ0LnRhcmdldC52YWx1ZVxuICAgICAgICB2YXIgcGFydGlhbCA9IGFjdGlvbkZ1bmNpdG9uLm1hdGNoKCdQYXJ0aWFsJykgIT0gbnVsbDtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgYWN0aW9uOiBhY3Rpb25GdW5jaXRvbixcbiAgICAgICAgICBwYXJ0aWFsOiBwYXJ0aWFsXG4gICAgICAgIH0pO1xuICAgICAgfSxcblxuXG4gICAgICBpc3N1ZVJlZnVuZDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXNbdGhpcy5zdGF0ZS5hY3Rpb25dKCk7XG4gICAgICB9LFxuXG5cbiAgICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgcmV0dXJuICggPHNlY3Rpb24gY2xhc3NOYW1lPVwicmVmdW5kc1wiPlxuICAgICAgICAgIDxoMT5Jc3N1ZSBhIHJlZnVuZDwvaDE+XG4gICAgICAgICAgPHA+WW91IGNhbiBpc3N1ZSBmdWxsIG9yIHBhcnRpYWwgcmVmdW5kcyB1c2luZyB0aGUgZm9ybSBiZWxvdzwvcD5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm1cIj5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxsYWJlbD5DaG9vc2UgYSByZWZ1bmQgdHlwZTo8L2xhYmVsPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNlbGVjdFwiPlxuICAgICAgICAgICAgICAgIDxzZWxlY3Qgb25DaGFuZ2U9e3RoaXMuc2V0QWN0aW9ufT5cbiAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJPbmx5UmVmdW5kQnV5ZXJcIj5GdWxsIHJlZnVuZCB0byBidXllcjwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlJlZnVuZEJ1eWVyRnJvbVJ1bm5lclwiPkZ1bGwgcmVmdW5kIHRvIGJ1eWVyIGZyb20gcnVubmVyPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiUGFydGlhbE9ubHlSZWZ1bmRCdXllclwiPlBhcnRpYWwgcmVmdW5kIHRvIGJ1eWVyPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiUGFydGlhbFJlZnVuZEJ1eWVyRnJvbVJ1bm5lclwiPlBhcnRpYWwgcmVmdW5kIHRvIGJ1eWVyIGZyb20gcnVubmVyPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgPC9zZWxlY3Q+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxsYWJlbD5SdW4gSUQ6PHNwYW4gY2xhc3NOYW1lPVwicmVxXCI+Kjwvc3Bhbj48L2xhYmVsPlxuICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBuYW1lPVwicnVuLWlkXCIgb25DaGFuZ2U9e3RoaXMudXBkYXRlUnVuSWR9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPGxhYmVsPk9yZGVyIElEOjxzcGFuIGNsYXNzTmFtZT1cInJlcVwiPiAqIDwvc3Bhbj48L2xhYmVsPlxuICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgIG5hbWU9XCJvcmRlci1pZFwiXG4gICAgICAgICAgICAgICAgb25DaGFuZ2UgPSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZU9yZGVySWRcbiAgICAgICAgICAgICAgICB9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPGxhYmVsPlJlZnVuZCBEZXNjcmlwdGlvbig8aT5pbnRlcm5hbCB1c2Ugb25seTwvaT4pOjxzcGFuIGNsYXNzTmFtZT1cInJlcVwiPio8L3NwYW4+PC9sYWJlbD5cbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cImRlc2NyaXB0aW9uXCIgb25DaGFuZ2U9e3RoaXMudXBkYXRlRGVzY30gLz5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8bGFiZWw+Tm90aWZpY2F0aW9uIE1lc3NhZ2UgdG8gUnVubmVyOjwvbGFiZWw+XG4gICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICBuYW1lPVwicnVubmVyLW1lc3NhZ2VcIlxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlID0ge1xuICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVSdW5Nc2dcbiAgICAgICAgICAgICAgICB9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPGxhYmVsPk5vdGlmaWNhdGlvbiBNZXNzYWdlIHRvIEJ1eWVyOjwvbGFiZWw+XG4gICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJidXllci1tZXNzYWdlXCIgb25DaGFuZ2U9e3RoaXMudXBkYXRlQnV5TXNnfSAvPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgdGhpcy5zdGF0ZS5wYXJ0aWFsID8gJycgOiAnaGlkZGVuJyB9PlxuICAgICAgICAgICAgICA8bGFiZWw+UmVmdW5kIEFtb3VudDo8c3BhbiBjbGFzc05hbWU9XCJyZXFcIj4qPC9zcGFuPjwvbGFiZWw+XG4gICAgICAgICAgICAgICQ8aW5wdXQgXG4gICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgIG5hbWU9XCJyZWZ1bmQtYW1vdW50XCJcbiAgICAgICAgICAgICAgICB0eXBlPVwibnVtYmVyXCJcbiAgICAgICAgICAgICAgICBtaW49XCIwXCJcbiAgICAgICAgICAgICAgICBzdGVwPVwiMC4wMVwiXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMudXBkYXRlUmVmdW5kQW10fSAvPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uXCIgb25DbGljaz17dGhpcy5pc3N1ZVJlZnVuZH0+UmVmdW5kPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvc2VjdGlvbj4gKX0sXG5cblxuICAgICAgLy9cbiAgICAgIC8vIEF2YWlsYWJsZSBhY3Rpb25zIChmdW5jdGlvbiBuYW1lIG1hdGNoZXMgcmVxdWVzdCBwYXJhbSBleGNsdWRpbmcgXCJQYXJ0aWFsXCIpXG4gICAgICAvL1xuICAgICAgT25seVJlZnVuZEJ1eWVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHBhdGggPSB0aGlzLnN0YXRlLnJ1bklkICsgJy9vcmRlci8nICsgdGhpcy5zdGF0ZS5vcmRlcklkICsgJy9hY3Rpb24nO1xuICAgICAgICB0aGlzLmZldGNoKHBhdGgpO1xuICAgICAgfSxcblxuXG4gICAgICBSZWZ1bmRCdXllckZyb21SdW5uZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgcGF0aCA9IHRoaXMuc3RhdGUucnVuSWQgKyAnL29yZGVyLycgKyB0aGlzLnN0YXRlLm9yZGVySWQgKyAnL2FjdGlvbic7XG4gICAgICAgIHRoaXMuZmV0Y2gocGF0aCk7XG4gICAgICB9LFxuXG5cbiAgICAgIFBhcnRpYWxPbmx5UmVmdW5kQnV5ZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgcGF0aCA9IHRoaXMuc3RhdGUucnVuSWQgKyAnL29yZGVyLycgKyB0aGlzLnN0YXRlLm9yZGVySWQgKyAnL2FjdGlvbic7XG4gICAgICAgIHRoaXMuZmV0Y2gocGF0aCk7XG4gICAgICB9LFxuXG5cbiAgICAgIFBhcnRpYWxSZWZ1bmRCdXllckZyb21SdW5uZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgcGF0aCA9IHRoaXMuc3RhdGUucnVuSWQgKyAnL29yZGVyLycgKyB0aGlzLnN0YXRlLm9yZGVySWQgKyAnL2FjdGlvbic7XG4gICAgICAgIHRoaXMuZmV0Y2gocGF0aCk7XG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBSZWZ1bmQ7IiwidmFyIFJlc29sdXRpb24gPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG4gICAgICBiYXNlUGF0aDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBBcHAuYXBpKCkgKyAndjEvcnVuLyc7XG4gICAgICB9LFxuXG4gICAgICBjb21wb25lbnRXaWxsVXBkYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgQXBwLmNsb3NlTWVzc2FnZSgpO1xuICAgICAgfSxcblxuICAgICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzZXRTdGF0ZSgpO1xuICAgICAgfSxcblxuICAgICAgcmVzZXRTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgYWN0aW9uOiAnUmVzb2x2ZVByb2JsZW0nLCAvLyBNYXRjaCBmdW5jdGlvbiBuYW1lXG4gICAgICAgICAgcnVuSWQ6ICcnLFxuICAgICAgICAgIGRhc2hlcklkOiBBcHAudXNlci5kYXNoZXJfdXNlcl9pZCB8fCAnJyAsIC8vIERlZmF1bHQgdG8gQWRtaW4gSURcbiAgICAgICAgICBvcmRlcklkOiAnJyxcbiAgICAgICAgICBkZXNjcmlwdGlvbjogJydcbiAgICAgICAgfTtcbiAgICAgIH0sXG5cblxuICAgICAgLy9cbiAgICAgIC8vIE5ldHdvcmsgZmV0Y2ggZnVuY3Rpb25zXG4gICAgICAvL1xuICAgICAgZmV0Y2g6IGZ1bmN0aW9uKGFwaVBhdGgpIHtcbiAgICAgICAgdmFyIGRhdGEgPSB0aGlzLmJ1aWxkRmV0Y2hEYXRhKCk7XG4gICAgICAgIGlmIChkYXRhID09PSBudWxsKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKCdGZXRjaDogJywgdGhpcy5iYXNlUGF0aCgpICsgYXBpUGF0aCk7XG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICAgICAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgdXJsOiB0aGlzLmJhc2VQYXRoKCkgKyBhcGlQYXRoLFxuICAgICAgICAgIHR5cGU6ICdQVVQnLFxuICAgICAgICAgIGNvbnRlbnRUeXBlOiAnYXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04JyxcbiAgICAgICAgICAvLyBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgIGRhdGE6IEpTT04uc3RyaW5naWZ5KGRhdGEpLFxuICAgICAgICAgIHN1Y2Nlc3M6IHRoYXQuZmV0Y2hTdWNjZXNzLFxuICAgICAgICAgIGVycm9yOiB0aGF0LmZldGNoRXJyb3JcbiAgICAgICAgfSk7XG4gICAgICB9LFxuXG4gICAgICBmZXRjaFN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICBBcHAud2FybihkYXRhLmVycm9yc1swXSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgQXBwLm5vdGlmeSgnUmVzb2x1dGlvbiBzdWNjZWVkZWQnKTtcbiAgICAgICAgICAkKCdzZWN0aW9uLnJlc29sdXRpb25zIGlucHV0JykudmFsKCcnKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgIH0sXG5cbiAgICAgIGZldGNoRXJyb3I6IGZ1bmN0aW9uKGVycikge1xuICAgICAgICBpZiAoZXJyLnJlc3BvbnNlSlNPTiAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9ycyAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9ycy5sZW5ndGgpIHtcbiAgICAgICAgICBBcHAud2FybihcIlJlc29sdXRpb24gZmFpbGVkOlxcblwiICsgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcnNbMF0pO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlcnIucmVzcG9uc2VKU09OICYmIGVyci5yZXNwb25zZUpTT04uZXJyb3JfY29kZSkge1xuICAgICAgICAgIEFwcC53YXJuKFwiUmVzb2x1dGlvbiBmYWlsZWQ6XFxuXCIgKyBlcnIucmVzcG9uc2VKU09OLmVycm9yX2NvZGUgKyAnOiAnICsgZXJyLnJlc3BvbnNlSlNPTi5tZXNzYWdlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBBcHAud2FybihcIlJlc29sdXRpb24gcmVxdWVzdCBmYWlsZWQuXCIpO1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgIH1cbiAgICAgIH0sXG5cbiAgICAgIGJ1aWxkRmV0Y2hEYXRhOiBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnJ1bklkKSB7XG4gICAgICAgICAgQXBwLndhcm4oJ1lvdSBtdXN0IGVudGVyIGEgUnVuIElELicpO1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmFjdGlvbiAhPT0gJ1JlbGVhc2VIb2xkJyAmJiAhdGhpcy5zdGF0ZS5vcmRlcklkKSB7XG4gICAgICAgICAgQXBwLndhcm4oJ1lvdSBtdXN0IGVudGVyIGEgT3JkZXIgSUQuJyk7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmRlc2NyaXB0aW9uKSB7XG4gICAgICAgICAgQXBwLndhcm4oJ1lvdSBtdXN0IGVudGVyIGEgRGVzY3JpcHRpb24gZm9yIGludGVybmFsIEpveXJ1biB1c2UuJyk7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgZGF0YSA9IHtcbiAgICAgICAgICBhY3Rpb25fdGFrZW46IHRoaXMuc3RhdGUuYWN0aW9uLFxuICAgICAgICAgIGRhc2hlcl91c2VyX2lkOiB0aGlzLnN0YXRlLmRhc2hlcklkLFxuICAgICAgICAgIGRlc2NyaXB0aW9uOiB0aGlzLnN0YXRlLmRlc2NyaXB0aW9uXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuYWN0aW9uID09PSAnUmVsZWFzZUhvbGQnKSB7XG4gICAgICAgICAgZGF0YS5wYXJhbXMgPSB7XG4gICAgICAgICAgICBydW5uZXJfbWVzc2FnZTogdGhpcy5zdGF0ZS5ydW5uZXJNc2csXG4gICAgICAgICAgICBidXllcl9tZXNzYWdlOiB0aGlzLnN0YXRlLmJ1eWVyTXNnXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgICAgfSxcblxuXG4gICAgICAvL1xuICAgICAgLy8gRm9ybSB1cGRhdGUgZnVuY2l0b25zXG4gICAgICAvL1xuICAgICAgdXBkYXRlUnVuSWQ6IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBydW5JZDogZXZ0LnRhcmdldC52YWx1ZVxuICAgICAgICB9KTtcbiAgICAgIH0sXG5cbiAgICAgIHVwZGF0ZU9yZGVySWQ6IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBvcmRlcklkOiBldnQudGFyZ2V0LnZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgfSxcblxuICAgICAgdXBkYXRlRGVzYzogZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGRlc2NyaXB0aW9uOiBldnQudGFyZ2V0LnZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgfSxcblxuICAgICAgdXBkYXRlUnVuTXNnOiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgcnVubmVyTXNnOiBldnQudGFyZ2V0LnZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgfSxcblxuICAgICAgdXBkYXRlQnV5TXNnOiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgYnV5ZXJNc2c6IGV2dC50YXJnZXQudmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9LFxuXG5cbiAgICAgIHNldEFjdGlvbjogZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIHZhciBhY3Rpb25GdW5jaXRvbiA9IGV2dC50YXJnZXQudmFsdWVcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgYWN0aW9uOiBhY3Rpb25GdW5jaXRvbixcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gVXNlIHRoZSBhZG1pbiBJRFxuICAgICAgICAvLyBpZiAoYWN0aW9uRnVuY2l0b24gIT09ICdSZWxlYXNlSG9sZCcpIHtcbiAgICAgICAgLy8gICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgLy8gICAgIGRhc2hlcklkOiBBcHAudXNlci5kYXNoZXJfdXNlcl9pZFxuICAgICAgICAvLyAgIH0pO1xuICAgICAgICAvLyB9XG4gICAgICB9LFxuXG5cbiAgICAgIHJlc29sdmVJc3N1ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXNbdGhpcy5zdGF0ZS5hY3Rpb25dKCk7XG4gICAgICB9LFxuXG5cbiAgICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciByZWxlYXNlSG9sZCA9IHRoaXMuc3RhdGUuYWN0aW9uID09PSAnUmVsZWFzZUhvbGQnO1xuXG4gICAgICAgIHJldHVybiAoIDxzZWN0aW9uIGNsYXNzTmFtZT1cInJlc29sdXRpb25zXCI+XG4gICAgICAgICAgPGgxPlJlc29sdmUgYSBQcm9ibGVtPC9oMT5cbiAgICAgICAgICA8cD5Zb3UgY2FuIG1hcmsgYSBwcm9ibGVtIGFzIHJlc29sdmVkIG9yIHJlbGVhc2UgYSBob2xkIHVzaW5nIHRoZSBmb3JtIGJlbG93PC9wPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybVwiPlxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPGxhYmVsPkNob29zZSBhIHJlc29sdXRpb24gdHlwZTo8L2xhYmVsPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNlbGVjdFwiPlxuICAgICAgICAgICAgICAgIDxzZWxlY3Qgb25DaGFuZ2U9e3RoaXMuc2V0QWN0aW9ufT5cbiAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJSZXNvbHZlUHJvYmxlbVwiPlJlc29sdmUgYSBQcm9ibGVtPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiUmVsZWFzZUhvbGRcIj5SZW1vdmUgSG9sZDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8bGFiZWw+UnVuIElEOiA8c3BhbiBjbGFzc05hbWU9XCJyZXFcIj4gKiA8L3NwYW4+PC9sYWJlbD5cbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICBuYW1lPVwicnVuLWlkXCJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZSA9IHtcbiAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUnVuSWRcbiAgICAgICAgICAgICAgICB9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9eyByZWxlYXNlSG9sZCA/ICdoaWRkZW4nIDogJycgfT5cbiAgICAgICAgICAgICAgPGxhYmVsPk9yZGVyIElEOiA8c3BhbiBjbGFzc05hbWU9XCJyZXFcIj4gKiA8L3NwYW4+PC9sYWJlbD5cbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICBuYW1lPVwib3JkZXItaWRcIlxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlID0ge1xuICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVPcmRlcklkXG4gICAgICAgICAgICAgICAgfSAvPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxsYWJlbD5SZXNvbHV0aW9uIERlc2NyaXB0aW9uKDxpPmludGVybmFsIHVzZSBvbmx5PC9pPik6PHNwYW4gY2xhc3NOYW1lPVwicmVxXCI+Kjwvc3Bhbj48L2xhYmVsPlxuICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBuYW1lPVwiZGVzY3JpcHRpb25cIiBvbkNoYW5nZT17dGhpcy51cGRhdGVEZXNjfSAvPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgcmVsZWFzZUhvbGQgPyAnJyA6ICdoaWRkZW4nIH0+XG4gICAgICAgICAgICAgIDxsYWJlbD5Ob3RpZmljYXRpb24gTWVzc2FnZSB0byBSdW5uZXI6PC9sYWJlbD5cbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgIG5hbWU9XCJydW5uZXItbWVzc2FnZVwiXG4gICAgICAgICAgICAgICAgb25DaGFuZ2UgPSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVJ1bk1zZ1xuICAgICAgICAgICAgICAgIH0gLz5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17IHJlbGVhc2VIb2xkID8gJycgOiAnaGlkZGVuJyB9PlxuICAgICAgICAgICAgICA8bGFiZWw+Tm90aWZpY2F0aW9uIE1lc3NhZ2UgdG8gQnV5ZXI6PC9sYWJlbD5cbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cImJ1eWVyLW1lc3NhZ2VcIiBvbkNoYW5nZT17dGhpcy51cGRhdGVCdXlNc2d9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXR0b25cIiBvbkNsaWNrPXt0aGlzLnJlc29sdmVJc3N1ZX0+UmVzb2x2ZSBJc3N1ZTwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L3NlY3Rpb24+ICl9LFxuXG5cbiAgICAgIC8vXG4gICAgICAvLyBBdmFpbGFibGUgYWN0aW9uc1xuICAgICAgLy9cbiAgICAgIFJlc29sdmVQcm9ibGVtOiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHBhdGggPSB0aGlzLnN0YXRlLnJ1bklkICsgJy9vcmRlci8nICsgdGhpcy5zdGF0ZS5vcmRlcklkICsgJy9hY3Rpb24nO1xuICAgICAgICB0aGlzLmZldGNoKHBhdGgpO1xuICAgICAgfSxcblxuXG4gICAgICBSZWxlYXNlSG9sZDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBwYXRoID0gdGhpcy5zdGF0ZS5ydW5JZCArICcvYWN0aW9uJztcbiAgICAgICAgdGhpcy5mZXRjaChwYXRoKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIG1vZHVsZS5leHBvcnRzID0gUmVzb2x1dGlvbjsiLCJ2YXIgVXNlck9yZGVycyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcblxuICBiYXNlUGF0aDogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIEFwcC5hcGkoKSArICd2MS8nO1xuICB9LFxuXG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV4dFByb3BzKSB7XG4gICAgdGhpcy5nZXRPcmRlcnMobmV4dFByb3BzLnVzZXJJZCk7XG4gIH0sXG4gIFxuXG4gIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG9yZGVyczogW10sXG4gICAgfTtcbiAgfSxcblxuXG4gIC8vXG4gIC8vIE5ldHdvcmsgZmV0Y2ggZnVuY3Rpb25zXG4gIC8vXG4gIGZldGNoOiBmdW5jdGlvbihhcGlQYXRoLCB1c2VySWQpIHtcbiAgICBjb25zb2xlLmxvZygnRmV0Y2g6ICcsIHRoaXMuYmFzZVBhdGgoKSArIGFwaVBhdGgpO1xuICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAkLmFqYXgoe1xuICAgICAgdXJsOiB0aGlzLmJhc2VQYXRoKCkgKyBhcGlQYXRoLFxuICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgc3VjY2VzczogdGhhdC5mZXRjaFN1Y2Nlc3MsXG4gICAgICBlcnJvcjogdGhhdC5mZXRjaEVycm9yXG4gICAgfSk7XG4gIH0sXG5cblxuICBmZXRjaFN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICBpZiAoZGF0YS5lcnJvcnMpIHtcbiAgICAgIEFwcC53YXJuKGRhdGEuZXJyb3JzWzBdKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7b3JkZXJzOiBkYXRhfSk7XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICB9LFxuXG5cbiAgZmV0Y2hFcnJvcjogZnVuY3Rpb24oZXJyKSB7XG4gICAgaWYgKGVyci5yZXNwb25zZUpTT04gJiYgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcnMgJiYgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcnMubGVuZ3RoKSB7XG4gICAgICBBcHAud2FybihcIk9yZGVyIFNlYXJjaCBmYWlsZWQ6XFxuXCIgKyBlcnIucmVzcG9uc2VKU09OLmVycm9yc1swXSk7XG4gICAgfVxuICAgIGlmIChlcnIucmVzcG9uc2VKU09OICYmIGVyci5yZXNwb25zZUpTT04uZXJyb3JfY29kZSkge1xuICAgICAgQXBwLndhcm4oXCJPcmRlciBTZWFyY2ggZmFpbGVkOlxcblwiICsgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcl9jb2RlICsgJzogJyArIGVyci5yZXNwb25zZUpTT04ubWVzc2FnZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIEFwcC53YXJuKFwiT3JkZXIgc2VhcmNoIGZhaWxlZC5cIik7XG4gICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgIH1cbiAgfSxcblxuXG4gIHJlbmRlck9yZGVyOiBmdW5jdGlvbihvcmRlcikge1xuICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICByZXR1cm4gKFxuICAgICAgPHRyIGtleT17IG9yZGVyLm9yZGVyX2lkIH0+XG4gICAgICAgIDx0ZD57IG9yZGVyLnJ1bl9pZCB9PC90ZD5cbiAgICAgICAgPHRkPnsgb3JkZXIub3JkZXJfaWQgfTwvdGQ+XG4gICAgICAgIDx0ZD57IG9yZGVyLnJlc3RhdXJhbnRfbmFtZSB9PC90ZD5cbiAgICAgICAgPHRkPnsgQXBwLnV0Y1RvTG9jYWwob3JkZXIub3JkZXJfcGxhY2VkKSB9PC90ZD5cbiAgICAgICAgPHRkPnsgb3JkZXIub3JkZXJfc3RhdHVzIH08L3RkPlxuICAgICAgICA8dGQ+JHsgb3JkZXIub3JkZXJfdmFsdWUudG9GaXhlZCgyKSB9PC90ZD5cbiAgICAgICAgPHRkPiR7IG9yZGVyLmdhc19tb25leS50b0ZpeGVkKDIpIH08L3RkPlxuICAgICAgICA8dGQ+JHsgb3JkZXIuc2VydmljZV9mZWVzLnRvRml4ZWQoMikgfTwvdGQ+XG4gICAgICAgIDx0ZD4keyBvcmRlci50YXhfdG90YWwudG9GaXhlZCgyKSB9PC90ZD5cbiAgICAgICAgPHRkPiR7IG9yZGVyLnJlZnVuZF9hbW91bnQudG9GaXhlZCgyKSB9PC90ZD5cbiAgICAgICAgPHRkPiR7IG9yZGVyLnRvdGFsX19wcmljZV9kaXNjb3VudGVkLnRvRml4ZWQoMikgfTwvdGQ+XG4gICAgICA8L3RyPlxuICAgICk7XG4gIH0sXG5cblxuICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgIHZhciBvcmRlclJvd3MgPSB0aGlzLnN0YXRlLm9yZGVycy5tYXAodGhpcy5yZW5kZXJPcmRlcik7XG5cbiAgICBpZiAob3JkZXJSb3dzLmxlbmd0aCAmJiB0aGlzLnByb3BzLnVzZXJJZCkge1xuICAgICAgcmV0dXJuICggPGRpdiBjbGFzc05hbWU9XCJ1c2VyLW9yZGVyc1wiPlxuICAgICAgICA8aDM+eyB0aGlzLnByb3BzLmRpc3BsYXlOYW1lIH0gT3JkZXJzOjwvaDM+XG4gICAgICAgIDx0YWJsZSBjZWxsU3BhY2luZz1cIjBcIj5cbiAgICAgICAgICA8dGhlYWQ+XG4gICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgIDx0aD5SdW4gSUQ8L3RoPlxuICAgICAgICAgICAgICA8dGg+T3JkZXIgSUQ8L3RoPlxuICAgICAgICAgICAgICA8dGg+UmVzdGF1cmFudDwvdGg+XG4gICAgICAgICAgICAgIDx0aD5PcmRlciBQbGFjZWQ8L3RoPlxuICAgICAgICAgICAgICA8dGg+U3RhdHVzPC90aD5cbiAgICAgICAgICAgICAgPHRoPk9yZGVyIFZhbHVlPC90aD5cbiAgICAgICAgICAgICAgPHRoPkdhcyBNb25leTwvdGg+XG4gICAgICAgICAgICAgIDx0aD5TZXJ2aWNlIEZlZXM8L3RoPlxuICAgICAgICAgICAgICA8dGg+VGF4PC90aD5cbiAgICAgICAgICAgICAgPHRoPlJlZnVuZCBBbW91bnQ8L3RoPlxuICAgICAgICAgICAgICA8dGg+VG90YWwgUHJpY2U8L3RoPlxuICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICA8L3RoZWFkPlxuICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICAgIHsgb3JkZXJSb3dzIH1cbiAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICA8L3RhYmxlPlxuICAgICAgPC9kaXY+KTtcbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5wcm9wcy51c2VySWQpIHtcbiAgICAgIHJldHVybiAoPGk+Tm8gT3JkZXJzIEZvdW5kPC9pPik7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT1cInVzZXItb3JkZXJzXCI+PC9kaXY+KTtcbiAgICB9XG4gIH0sXG5cblxuICBnZXRPcmRlcnM6IGZ1bmN0aW9uKHVzZXJJZCkge1xuICAgIGlmICghdXNlcklkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhciBwYXRoID0gJ3N1cHBvcnQvb3JkZXJzJztcbiAgICB0aGlzLmZldGNoKHBhdGgsIHVzZXJJZCk7XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFVzZXJPcmRlcnM7XG4iLCJ2YXIgVXNlclJ1bnMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG5cbiAgYmFzZVBhdGg6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBBcHAuYXBpKCkgKyAndjEvJztcbiAgfSxcbiAgXG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV4dFByb3BzKSB7XG4gICAgdGhpcy5nZXRPcmRlcnMobmV4dFByb3BzLnVzZXJJZCk7XG4gIH0sXG5cblxuICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICBvcmRlcnM6IFtdLFxuICAgIH07XG4gIH0sXG5cblxuICAvL1xuICAvLyBOZXR3b3JrIGZldGNoIGZ1bmN0aW9uc1xuICAvL1xuICBmZXRjaDogZnVuY3Rpb24oYXBpUGF0aCwgdXNlcklkKSB7XG4gICAgY29uc29sZS5sb2coJ0ZldGNoOiAnLCB0aGlzLmJhc2VQYXRoKCkgKyBhcGlQYXRoKTtcbiAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgJC5hamF4KHtcbiAgICAgIHVybDogdGhpcy5iYXNlUGF0aCgpICsgYXBpUGF0aCxcbiAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgIHN1Y2Nlc3M6IHRoYXQuZmV0Y2hTdWNjZXNzLFxuICAgICAgZXJyb3I6IHRoYXQuZmV0Y2hFcnJvclxuICAgIH0pO1xuICB9LFxuXG5cbiAgZmV0Y2hTdWNjZXNzOiBmdW5jdGlvbihkYXRhKSB7XG4gICAgaWYgKGRhdGEuZXJyb3JzKSB7XG4gICAgICBBcHAud2FybihkYXRhLmVycm9yc1swXSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe29yZGVyczogZGF0YX0pO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgfSxcblxuXG4gIGZldGNoRXJyb3I6IGZ1bmN0aW9uKGVycikge1xuICAgIGlmIChlcnIucmVzcG9uc2VKU09OICYmIGVyci5yZXNwb25zZUpTT04uZXJyb3JzICYmIGVyci5yZXNwb25zZUpTT04uZXJyb3JzLmxlbmd0aCkge1xuICAgICAgQXBwLndhcm4oXCJPcmRlciBTZWFyY2ggZmFpbGVkOlxcblwiICsgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcnNbMF0pO1xuICAgIH1cbiAgICBpZiAoZXJyLnJlc3BvbnNlSlNPTiAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9yX2NvZGUpIHtcbiAgICAgIEFwcC53YXJuKFwiT3JkZXIgU2VhcmNoIGZhaWxlZDpcXG5cIiArIGVyci5yZXNwb25zZUpTT04uZXJyb3JfY29kZSArICc6ICcgKyBlcnIucmVzcG9uc2VKU09OLm1lc3NhZ2UpO1xuICAgIH0gZWxzZSB7XG4gICAgICBBcHAud2FybihcIk9yZGVyIHNlYXJjaCBmYWlsZWQuXCIpO1xuICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICB9XG4gIH0sXG5cblxuICByZW5kZXJPcmRlcjogZnVuY3Rpb24ob3JkZXIpIHtcbiAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgcmV0dXJuIChcbiAgICAgIDx0ciBrZXk9eyBvcmRlci5ydW5faWQgfT5cbiAgICAgICAgPHRkPnsgb3JkZXIucnVuX2lkIH08L3RkPlxuICAgICAgICA8dGQ+eyBBcHAudXRjVG9Mb2NhbChvcmRlci5jcmVhdGVkX2RhdGUpIH08L3RkPlxuICAgICAgICA8dGQ+eyBvcmRlci5ydW5fc3RhdGUgfTwvdGQ+XG4gICAgICAgIDx0ZD57IG9yZGVyLnJ1bl9zdGF0dXMgfTwvdGQ+XG4gICAgICAgIDx0ZD4keyBvcmRlci5lYXJuaW5ncy50b0ZpeGVkKDIpIH08L3RkPlxuICAgICAgICA8dGQ+JHsgb3JkZXIucmVpbWJ1cnNlX2Ftb3VudC50b0ZpeGVkKDIpIH08L3RkPlxuICAgICAgPC90cj5cbiAgICApO1xuICB9LFxuXG5cbiAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgb3JkZXJSb3dzID0gdGhpcy5zdGF0ZS5vcmRlcnMubWFwKHRoaXMucmVuZGVyT3JkZXIpO1xuXG4gICAgaWYgKG9yZGVyUm93cy5sZW5ndGggJiYgdGhpcy5wcm9wcy51c2VySWQpIHtcbiAgICAgIHJldHVybiAoIDxkaXYgY2xhc3NOYW1lPVwidXNlci1vcmRlcnNcIj5cbiAgICAgICAgPGgzPnsgdGhpcy5wcm9wcy5kaXNwbGF5TmFtZSB9IFJ1bnM6PC9oMz5cbiAgICAgICAgPHRhYmxlIGNlbGxTcGFjaW5nPVwiMFwiPlxuICAgICAgICAgIDx0aGVhZD5cbiAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgPHRoPlJ1biBJRDwvdGg+XG4gICAgICAgICAgICAgIDx0aD5DcmVhdGVkIERhdGU8L3RoPlxuICAgICAgICAgICAgICA8dGg+UnVuIFN0YXRlPC90aD5cbiAgICAgICAgICAgICAgPHRoPlN0YXR1czwvdGg+XG4gICAgICAgICAgICAgIDx0aD5FYXJuaW5nczwvdGg+XG4gICAgICAgICAgICAgIDx0aD5SZWltYnVyc2UgVmFsdWU8L3RoPlxuICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICA8L3RoZWFkPlxuICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICAgIHsgb3JkZXJSb3dzIH1cbiAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICA8L3RhYmxlPlxuICAgICAgPC9kaXY+KTtcbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5wcm9wcy51c2VySWQpIHtcbiAgICAgIHJldHVybiAoPGk+Tm8gUnVucyBGb3VuZDwvaT4pO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9XCJ1c2VyLW9yZGVyc1wiPjwvZGl2Pik7XG4gICAgfVxuICB9LFxuXG5cbiAgZ2V0T3JkZXJzOiBmdW5jdGlvbih1c2VySWQpIHtcbiAgICBpZiAoIXVzZXJJZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgcGF0aCA9ICdzdXBwb3J0L3J1bnMnO1xuICAgIHRoaXMuZmV0Y2gocGF0aCwgdXNlcklkKTtcbiAgfVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gVXNlclJ1bnM7XG4iLCJ2YXIgVXNlck9yZGVycyA9IHJlcXVpcmUoJy4vVXNlck9yZGVycycpO1xudmFyIFVzZXJSdW5zID0gcmVxdWlyZSgnLi9Vc2VyUnVucycpO1xuXG52YXIgVXNlcnMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG5cbiAgYmFzZVBhdGg6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBBcHAuYXBpKCkgKyAndjEvJztcbiAgfSxcblxuXG4gIGNvbXBvbmVudFdpbGxVcGRhdGU6IGZ1bmN0aW9uKCkge1xuICAgIEFwcC5jbG9zZU1lc3NhZ2UoKTtcbiAgfSxcblxuXG4gIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcbiAgICAkKFwiaW5wdXQ6dGV4dFwiKS5mb2N1cyhmdW5jdGlvbigpIHsgJCh0aGlzKS5zZWxlY3QoKTsgfSApO1xuICAgICQoJy5mb2N1cy1tZScpLmZvY3VzKCk7XG4gIH0sXG5cblxuICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICBmaXJzdE5hbWU6ICcnLFxuICAgICAgbGFzdE5hbWU6ICcnLFxuICAgICAgdXNlcnM6IFtdXG4gICAgfTtcbiAgfSxcblxuXG4gIC8vXG4gIC8vIE5ldHdvcmsgZmV0Y2ggZnVuY3Rpb25zXG4gIC8vXG4gIGZldGNoOiBmdW5jdGlvbihhcGlQYXRoKSB7XG4gICAgY29uc29sZS5sb2coJ0ZldGNoOiAnLCB0aGlzLmJhc2VQYXRoKCkgKyBhcGlQYXRoKTtcbiAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgJC5hamF4KHtcbiAgICAgIHVybDogdGhpcy5iYXNlUGF0aCgpICsgYXBpUGF0aCxcbiAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgIHN1Y2Nlc3M6IHRoYXQuZmV0Y2hTdWNjZXNzLFxuICAgICAgZXJyb3I6IHRoYXQuZmV0Y2hFcnJvclxuICAgIH0pO1xuICB9LFxuXG5cbiAgZmV0Y2hTdWNjZXNzOiBmdW5jdGlvbihkYXRhKSB7XG4gICAgaWYgKGRhdGEuZXJyb3JzKSB7XG4gICAgICBBcHAud2FybihkYXRhLmVycm9yc1swXSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIEFwcC5ub3RpZnkoJ1VzZXIgc2VhcmNoIHN1Y2NlZWRlZCcpO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGZpcnN0TmFtZTogJycsIGxhc3ROYW1lOiAnJyxcbiAgICAgICAgdXNlcnM6IGRhdGFcbiAgICAgIH0pO1xuICAgICAgJChcImlucHV0OnRleHRcIikudmFsKCcnKTtcbiAgICB9XG4gICAgY29uc29sZS5sb2coZGF0YSk7XG4gIH0sXG5cblxuICBmZXRjaEVycm9yOiBmdW5jdGlvbihlcnIpIHtcbiAgICBpZiAoZXJyLnJlc3BvbnNlSlNPTiAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9ycyAmJiBlcnIucmVzcG9uc2VKU09OLmVycm9ycy5sZW5ndGgpIHtcbiAgICAgIEFwcC53YXJuKFwiU2VhcmNoIGZhaWxlZDpcXG5cIiArIGVyci5yZXNwb25zZUpTT04uZXJyb3JzWzBdKTtcbiAgICB9XG4gICAgaWYgKGVyci5yZXNwb25zZUpTT04gJiYgZXJyLnJlc3BvbnNlSlNPTi5lcnJvcl9jb2RlKSB7XG4gICAgICBBcHAud2FybihcIlNlYXJjaCBmYWlsZWQ6XFxuXCIgKyBlcnIucmVzcG9uc2VKU09OLmVycm9yX2NvZGUgKyAnOiAnICsgZXJyLnJlc3BvbnNlSlNPTi5tZXNzYWdlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgQXBwLndhcm4oXCJVc2VyIHNlYXJjaCBmYWlsZWQuXCIpO1xuICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICB9XG4gIH0sXG5cblxuICByZW5kZXJVc2VyOiBmdW5jdGlvbih1c2VyKSB7XG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgIHZhciBzZWxlY3RlZCA9ICcnO1xuICAgIGlmICh0aGlzLnN0YXRlLm9yZGVyVXNlcklkID09PSB1c2VyLmRhc2hlcl91c2VyX2lkIHx8IHRoaXMuc3RhdGUucnVuVXNlcklkID09PSB1c2VyLmRhc2hlcl91c2VyX2lkKSB7XG4gICAgICBzZWxlY3RlZCA9ICdzZWxlY3RlZCc7XG4gICAgfVxuICAgIHJldHVybiAoXG4gICAgICA8dHIgY2xhc3NOYW1lPXsgc2VsZWN0ZWQgfSBrZXk9eyB1c2VyLmRhc2hlcl91c2VyX2lkIH0+XG4gICAgICAgIDx0ZD57IHVzZXIuZGFzaGVyX3VzZXJfaWQgfTwvdGQ+XG4gICAgICAgIDx0ZD57IHVzZXIuZGlzcGxheV9uYW1lIH08L3RkPlxuICAgICAgICA8dGQ+eyB1c2VyLmZpcnN0X25hbWUgfTwvdGQ+XG4gICAgICAgIDx0ZD57IHVzZXIubGFzdF9uYW1lIH08L3RkPlxuICAgICAgICA8dGQ+eyB1c2VyLmVtYWlsX2FkZHJlc3MgfHwgJ3Vua25vd24nIH08L3RkPlxuICAgICAgICA8dGQ+eyB1c2VyLnBob25lX251bWJlciB8fCAndW5rbm93bicgfTwvdGQ+XG4gICAgICAgIDx0ZD57IHVzZXIucmVnaW9uX25hbWUgfTwvdGQ+XG4gICAgICAgIDx0ZD57IHVzZXIuc2VjdXJlX3VzZXJfaWQgfTwvdGQ+XG4gICAgICAgIDx0ZCBjbGFzc05hbWU9XCJsaW5rXCIgb25DbGljaz17IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHRoYXQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgcnVuVXNlcklkOiB1c2VyLnNlY3VyZV91c2VyX2lkLFxuICAgICAgICAgICAgcnVuVXNlckRpc3BsYXk6IHVzZXIuZGlzcGxheV9uYW1lLFxuICAgICAgICAgICAgb3JkZXJVc2VySWQ6ICcnLFxuICAgICAgICAgICAgb3JkZXJVc2VyRGlzcGxheTogJydcbiAgICAgICAgICB9KX0gfT5cbiAgICAgICAgICBSdW5zPC90ZD5cbiAgICAgICAgPHRkIGNsYXNzTmFtZT1cImxpbmtcIiBvbkNsaWNrPXsgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgdGhhdC5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBvcmRlclVzZXJJZDogdXNlci5zZWN1cmVfdXNlcl9pZCwgXG4gICAgICAgICAgICBvcmRlclVzZXJEaXNwbGF5OiB1c2VyLmRpc3BsYXlfbmFtZSxcbiAgICAgICAgICAgIHJ1blVzZXJJZDogJycsXG4gICAgICAgICAgICBydW5Vc2VyRGlzcGxheTogJydcbiAgICAgICAgICB9KX0gfT5cbiAgICAgICAgICBPcmRlcnM8L3RkPlxuICAgICAgPC90cj5cbiAgICApO1xuICB9LFxuXG5cbiAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgdXNlclJvd3MgPSB0aGlzLnN0YXRlLnVzZXJzLm1hcCh0aGlzLnJlbmRlclVzZXIpO1xuXG4gICAgaWYgKHVzZXJSb3dzLmxlbmd0aCkge1xuICAgICAgdmFyIHVzZXJUYWJsZSA9IChcbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8aDM+Rm91bmQgVXNlcnM6PC9oMz5cbiAgICAgICAgICA8dGFibGUgY2VsbFNwYWNpbmc9XCIwXCI+XG4gICAgICAgICAgICA8dGhlYWQ+XG4gICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICA8dGg+VXNlciBJRDwvdGg+XG4gICAgICAgICAgICAgICAgPHRoPkRpc3BsYXkgTmFtZTwvdGg+XG4gICAgICAgICAgICAgICAgPHRoPkZpcnN0IE5hbWU8L3RoPlxuICAgICAgICAgICAgICAgIDx0aD5MYXN0IE5hbWU8L3RoPlxuICAgICAgICAgICAgICAgIDx0aD5FbWFpbCBBZGRyZXNzPC90aD5cbiAgICAgICAgICAgICAgICA8dGg+UGhvbmUgTnVtYmVyPC90aD5cbiAgICAgICAgICAgICAgICA8dGg+UmVnaW9uPC90aD5cbiAgICAgICAgICAgICAgICA8dGg+SGFzaGVkIFVzZXIgSUQ8L3RoPlxuICAgICAgICAgICAgICAgIDx0aD5SdW5zPC90aD5cbiAgICAgICAgICAgICAgICA8dGg+T3JkZXJzPC90aD5cbiAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgIDwvdGhlYWQ+XG4gICAgICAgICAgICA8dGJvZHk+XG4gICAgICAgICAgICAgIHsgdXNlclJvd3MgfVxuICAgICAgICAgICAgPC90Ym9keT5cbiAgICAgICAgICA8L3RhYmxlPlxuICAgICAgICA8L2Rpdj4pO1xuICAgIH1cblxuICAgIHJldHVybiAoIDxzZWN0aW9uIGNsYXNzTmFtZT1cInVzZXItc2VhcmNoXCI+XG4gICAgICA8aDE+U2VhcmNoIGZvciBVc2VyczwvaDE+XG4gICAgICA8cD5Vc2UgdGhpcyBmb3JtIHRvIHNlYXJjaCBmb3IgdXNlciBkZXRhaWxzIGFuZCBmaW5kIHJ1bnMgYW5kIG9yZGVycyBmb3IgdGhlIGdpdmVuIHVzZXI8L3A+XG5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybVwiPlxuICAgICAgICA8bGFiZWw+Rmlyc3QgTmFtZTo8L2xhYmVsPlxuICAgICAgICA8aW5wdXQgb25LZXlVcD17IHRoaXMuZGV0ZWN0RW50ZXIgfSB0eXBlPVwidGV4dFwiIG9uQ2hhbmdlPXsgdGhpcy5zZXRGaXJzdE5hbWUgfSBuYW1lPVwiZmlyc3QtbmFtZVwiIGNsYXNzTmFtZT1cImZvY3VzLW1lXCIgLz5cbiAgICAgICAgPGJyIC8+XG4gICAgICAgIDxsYWJlbD5MYXN0IE5hbWU6PC9sYWJlbD5cbiAgICAgICAgPGlucHV0IG9uS2V5VXA9eyB0aGlzLmRldGVjdEVudGVyIH0gdHlwZT1cInRleHRcIiBvbkNoYW5nZT17IHRoaXMuc2V0TGFzdE5hbWUgfSBuYW1lPVwibGFzdC1uYW1lXCIgLz5cbiAgICAgICAgPGJyIC8+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uXCIgb25DbGljaz17IHRoaXMuc2VhcmNoVXNlcnMgfT5TZWFyY2ggVXNlcjwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgICA8YnIgLz5cbiAgICAgIDxiciAvPlxuICAgICAgeyB1c2VyVGFibGUgfVxuICAgICAgPGJyIC8+XG4gICAgICA8YnIgLz5cbiAgICAgIDxVc2VyT3JkZXJzIHVzZXJJZD17IHRoaXMuc3RhdGUub3JkZXJVc2VySWQgfSBkaXNwbGF5TmFtZT17IHRoaXMuc3RhdGUub3JkZXJVc2VyRGlzcGxheSB9IC8+XG4gICAgICA8VXNlclJ1bnMgdXNlcklkPXsgdGhpcy5zdGF0ZS5ydW5Vc2VySWQgfSBkaXNwbGF5TmFtZT17IHRoaXMuc3RhdGUucnVuVXNlckRpc3BsYXkgfSAvPlxuICAgIDwvc2VjdGlvbj4pO1xuICB9LFxuXG5cbiAgc2V0Rmlyc3ROYW1lOiBmdW5jdGlvbihldnQpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtmaXJzdE5hbWU6IGV2dC50YXJnZXQudmFsdWV9KTtcbiAgfSxcblxuXG4gIHNldExhc3ROYW1lOiBmdW5jdGlvbihldnQpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtsYXN0TmFtZTogZXZ0LnRhcmdldC52YWx1ZX0pO1xuICB9LFxuXG5cbiAgZGV0ZWN0RW50ZXI6IGZ1bmN0aW9uKGV2dCkge1xuICAgIGV2dCA9IGV2dCB8fCB3aW5kb3cuZXZlbnQ7XG4gICAgaWYgKGV2dC5rZXlDb2RlID09PSAxMykge1xuICAgICAgdGhpcy5zZWFyY2hVc2VycygpO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfSxcblxuXG4gIHNlYXJjaFVzZXJzOiBmdW5jdGlvbigpIHtcbiAgICBpZiAoIXRoaXMuc3RhdGUuZmlyc3ROYW1lICYmICF0aGlzLnN0YXRlLmxhc3ROYW1lKSB7XG4gICAgICByZXR1cm4gQXBwLndhcm4oJ1lvdSBtdXN0IGVuZGVyIGEgZmlyc3QgbmFtZSBhbmQvb3IgbGFzdCBuYW1lIHRvIHNlYXJjaC4nKTtcbiAgICB9XG5cbiAgICB2YXIgcGF0aCA9ICdzdXBwb3J0L3VzZXJzPyc7XG4gICAgXG4gICAgaWYgKHRoaXMuc3RhdGUuZmlyc3ROYW1lKSB7XG4gICAgICBwYXRoICs9IChcImZuYW1lPVwiICsgdGhpcy5zdGF0ZS5maXJzdE5hbWUpO1xuXG4gICAgICBpZiAodGhpcy5zdGF0ZS5sYXN0TmFtZSkge1xuICAgICAgICBwYXRoICs9ICcmJyArIChcImxuYW1lPVwiICsgdGhpcy5zdGF0ZS5sYXN0TmFtZSk7XG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKHRoaXMuc3RhdGUubGFzdE5hbWUpIHtcbiAgICAgIHBhdGggKz0gKFwibG5hbWU9XCIgKyB0aGlzLnN0YXRlLmxhc3ROYW1lKTtcbiAgICB9XG5cbiAgICB0aGlzLmZldGNoKHBhdGgpO1xuICAgIFxuICAgIC8vIEhpZGUgdGhlIHRhYmxlcyB3aGVuIHNlYXJjaGluZyBhIG5ldyB1c2VyXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBvcmRlclVzZXJJZDogJycsXG4gICAgICBvcmRlclVzZXJEaXNwbGF5OiAnJyxcbiAgICAgIHJ1blVzZXJJZDogJycsXG4gICAgICBydW5Vc2VyRGlzcGxheTogJydcbiAgICB9KVxuICB9LFxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gVXNlcnM7XG4iXX0=
