#!/bin/bash
#pass in the appropriate environment (dev2/stage/prod)
dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
git submodule init
git submodule update
cd src/dasher
gulp build --env $1
cd ../..
gulp release --env $1
if zip -r ./release.zip ./release; then
  echo "release.zip created! go and scp it to the server"
else
  echo "Hmmm, something went wrong"
fi