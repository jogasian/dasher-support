var Logout = require('./Logout');
var Dashboard = require('./Dashboard');

var Dashboard = React.createClass({
	getInitialState: function() {
		return {
			appState: 'login'
		}
	},

  render: function() {
    return (
      <div className="wrapper">
        <Logout />
      </div>
    )
  }
});

module.exports = Dashboard;
