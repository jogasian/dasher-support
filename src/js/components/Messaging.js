var Messaging = React.createClass({
  getInitialState: function() {
    return {
      active: false,
      type: 'notify',
      message: ''
    };
  },

  componentWillMount: function() {
    App.notify = this.notify;
    App.warn = this.warn;
    App.closeMessage = this.close;
  },

  notify: function(msg) {
    this.setState({
      active: true,
      message: msg,
      type: 'notify'
    });
  },

  warn: function(msg) {
    this.setState({
      active: true,
      message: msg,
      type: 'warn'
    });
  },

  close: function() {
    this.setState({
      active: false
    });
  },

  ignore: function(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  },

  render: function() {
    var className = this.state.active ? '' : ' invisible';
    return (
      <div onClick={this.close} className={ "messaging " + this.state.type + className }>
        <div className="message" onClick={this.ignore}>
          { this.state.message }
          <div className="close" onClick={this.close}>x</div>
        </div>
      </div>
    )
  }
});

module.exports = Messaging;
