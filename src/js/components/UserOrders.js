var UserOrders = React.createClass({

  basePath: function() {
    return App.api() + 'v1/';
  },


  componentWillReceiveProps: function(nextProps) {
    this.getOrders(nextProps.userId);
  },
  

  getInitialState: function() {
    return {
      orders: [],
    };
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath, userId) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },


  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      this.setState({orders: data});
    }
    console.log(data);
  },


  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Order Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Order Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Order search failed.");
      console.log(err);
    }
  },


  renderOrder: function(order) {
    var that = this;
    return (
      <tr key={ order.order_id }>
        <td>{ order.run_id }</td>
        <td>{ order.order_id }</td>
        <td>{ order.restaurant_name }</td>
        <td>{ App.utcToLocal(order.order_placed) }</td>
        <td>{ order.order_status }</td>
        <td>${ order.order_value.toFixed(2) }</td>
        <td>${ order.gas_money.toFixed(2) }</td>
        <td>${ order.service_fees.toFixed(2) }</td>
        <td>${ order.tax_total.toFixed(2) }</td>
        <td>${ order.refund_amount.toFixed(2) }</td>
        <td>${ order.total__price_discounted.toFixed(2) }</td>
      </tr>
    );
  },


  render: function() {
    var orderRows = this.state.orders.map(this.renderOrder);

    if (orderRows.length && this.props.userId) {
      return ( <div className="user-orders">
        <h3>{ this.props.displayName } Orders:</h3>
        <table cellSpacing="0">
          <thead>
            <tr>
              <th>Run ID</th>
              <th>Order ID</th>
              <th>Restaurant</th>
              <th>Order Placed</th>
              <th>Status</th>
              <th>Order Value</th>
              <th>Gas Money</th>
              <th>Service Fees</th>
              <th>Tax</th>
              <th>Refund Amount</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>
            { orderRows }
          </tbody>
        </table>
      </div>);
    }
    else if (this.props.userId) {
      return (<i>No Orders Found</i>);
    }
    else {
      return (<div className="user-orders"></div>);
    }
  },


  getOrders: function(userId) {
    if (!userId) {
      return;
    }
    var path = 'support/orders';
    this.fetch(path, userId);
  }
});

module.exports = UserOrders;
