
var Header = React.createClass({
  getInitialState: function() {
    return {
      mobileActive: false
    }  
  },

  renderOption: function(option) {
    var that = this;
    return (
      <li 
        onClick={ function() {
          that.props.changeView(option);
          that.setState({ mobileActive: false });
        }} 
        key={option}>
        {option.toUpperCase()}
      </li>
    );
  },

  render: function() {
    var that = this;
    var options = [];
    options = this.props.menuOptions.map(this.renderOption);

    return (
      <header className={ that.state.mobileActive ? 'mobile-active' : '' }>
        <div className="menu-link" onClick={ function() {that.setState({mobileActive:!that.state.mobileActive})} }>
          MENU
        </div>
        <ul>
          {options}
        </ul>
        <div className="env-right">
          [{ App.environment }]
        </div>
      </header>
    )
  }
});

module.exports = Header;
