
var Logout = React.createClass({

	componentWillMount: function() {
		this.ref = new Firebase('https://dasher-app-staging.firebaseio.com');
	},


	unauthenticate: function() {
		this.ref.unauth();
		sessionStorage.clear();
		window.location = "/";
	},


  render: function() {
    return (
      <div className="logout-wrapper">
        <button onClick={this.unauthenticate}>Log Out</button>
      </div>
    )
  }
});

module.exports = Logout;
