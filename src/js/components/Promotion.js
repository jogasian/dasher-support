var Promotion = React.createClass({

  basePath: function() {
    return App.api() + 'v2/';
  },

  componentWillUpdate: function() {
    App.closeMessage();
  },

  getInitialState: function() {
    return {
      action: 'givePromo', // Match function name
      promoCode: '',
      userId:  encodeURI(App.user.dasher_user_id),
      beneficiaryUserId: '',
      benefitValue: ''
    };
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'POST',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Promotion succeeded');
      $('section.promotions input').val('');
    }
    console.log(data);
  },

  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Promotion failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Promotion failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Promotion request failed.");
      console.log(err);
    }
  },


  //
  // Form update funcitons
  //
  updatePromoCode: function(evt) {
    this.setState({
      promoCode: evt.target.value
    });
  },

  updateUserId: function(evt) {
    this.setState({
      beneficiaryUserId: encodeURI(evt.target.value)
    });
  },

  updateBenefit: function(evt) {
    this.setState({
      benefitValue: evt.target.value
    });
  },


  setAction: function(evt) {
    var actionFunciton = evt.target.value
    this.setState({
      action: actionFunciton
    });
  },


  issuePromo: function() {
    this[this.state.action]();
  },


  render: function() {
    var promoCash = this.state.action === 'givePromoCash';

    return ( <section className = "promotions">
      <h1>Give a Promotion</h1>
      <p>You can give users promotions using the forms below</p>
      <div className="form">
        <div>
          <label>Choose a refund type:</label>
          <div className="select">
            <select onChange={this.setAction}>
              <option value="givePromo">
                Give user promo code
              </option>
              <option value="givePromoCash">Give user promo cash</option>
            </select>
          </div>
        </div>

        <div className={ promoCash ? 'hidden' : '' }>
          <label>Promo Code: <span className="req">*</span></label>
          <input type="text"
            name = "promo-code"
            onChange={ this.updatePromoCode } />
        </div>

        <div>
          <label>User ID:<span className="req">*</span></label>
          <input type="text"
            name="user-id"
            onChange={ this.updateUserId }/>
        </div>

        <div className={ promoCash ? '' : 'hidden' }>
          <label>Benefit Value: <span className="req" >*</span></label>
          <input type="text"
            name="benefit-value"
            onChange={ this.updateBenefit }/>
        </div>

        <div className="button" onClick = { this.issuePromo }>Issue Promo</div>
      </div>
    </section>);
  },


  //
  // Available actions
  //
  givePromo: function() {
    if (!this.state.promoCode) {
      return App.warn('You must ender a valid promo code.');
    }
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must ender a User ID.');
    }

    var path =
      'promotion/couponCode/' +
      this.state.promoCode +
      '/apply?dasherUserId=' +
      this.state.userId + 
      '&beneficiaryUserId=' +
      this.state.beneficiaryUserId;
    this.fetch(path);
  },

  givePromoCash: function() {
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must ender a user ID.');
    }
    if (!this.state.benefitValue) {
      return App.warn('You must ender a benefit value.');
    }

    var path =
      'promotion/couponCode/DYNAMIC_CASH/apply?dasherUserId=' +
      this.state.userId +
      '&benefitValue=' +
      this.state.benefitValue+ 
      '&beneficiaryUserId=' +
      this.state.beneficiaryUserId;
    this.fetch(path);
  }
});

module.exports = Promotion;
