var UserRuns = React.createClass({

  basePath: function() {
    return App.api() + 'v1/';
  },
  

  componentWillReceiveProps: function(nextProps) {
    this.getOrders(nextProps.userId);
  },


  getInitialState: function() {
    return {
      orders: [],
    };
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath, userId) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },


  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      this.setState({orders: data});
    }
    console.log(data);
  },


  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Order Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Order Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Order search failed.");
      console.log(err);
    }
  },


  renderOrder: function(order) {
    var that = this;
    return (
      <tr key={ order.run_id }>
        <td>{ order.run_id }</td>
        <td>{ App.utcToLocal(order.created_date) }</td>
        <td>{ order.run_state }</td>
        <td>{ order.run_status }</td>
        <td>${ order.earnings.toFixed(2) }</td>
        <td>${ order.reimburse_amount.toFixed(2) }</td>
      </tr>
    );
  },


  render: function() {
    var orderRows = this.state.orders.map(this.renderOrder);

    if (orderRows.length && this.props.userId) {
      return ( <div className="user-orders">
        <h3>{ this.props.displayName } Runs:</h3>
        <table cellSpacing="0">
          <thead>
            <tr>
              <th>Run ID</th>
              <th>Created Date</th>
              <th>Run State</th>
              <th>Status</th>
              <th>Earnings</th>
              <th>Reimburse Value</th>
            </tr>
          </thead>
          <tbody>
            { orderRows }
          </tbody>
        </table>
      </div>);
    }
    else if (this.props.userId) {
      return (<i>No Runs Found</i>);
    }
    else {
      return (<div className="user-orders"></div>);
    }
  },


  getOrders: function(userId) {
    if (!userId) {
      return;
    }
    var path = 'support/runs';
    this.fetch(path, userId);
  }
});

module.exports = UserRuns;
