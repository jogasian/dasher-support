var UserOrders = require('./UserOrders');
var UserRuns = require('./UserRuns');

var Users = React.createClass({

  basePath: function() {
    return App.api() + 'v1/';
  },


  componentWillUpdate: function() {
    App.closeMessage();
  },


  componentDidMount: function() {
    $("input:text").focus(function() { $(this).select(); } );
    $('.focus-me').focus();
  },


  getInitialState: function() {
    return {
      firstName: '',
      lastName: '',
      users: []
    };
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },


  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('User search succeeded');
      this.setState({
        firstName: '', lastName: '',
        users: data
      });
      $("input:text").val('');
    }
    console.log(data);
  },


  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("User search failed.");
      console.log(err);
    }
  },


  renderUser: function(user) {
    var that = this;
    var selected = '';
    if (this.state.orderUserId === user.dasher_user_id || this.state.runUserId === user.dasher_user_id) {
      selected = 'selected';
    }
    return (
      <tr className={ selected } key={ user.dasher_user_id }>
        <td>{ user.dasher_user_id }</td>
        <td>{ user.display_name }</td>
        <td>{ user.first_name }</td>
        <td>{ user.last_name }</td>
        <td>{ user.email_address || 'unknown' }</td>
        <td>{ user.phone_number || 'unknown' }</td>
        <td>{ user.region_name }</td>
        <td>{ user.secure_user_id }</td>
        <td className="link" onClick={ function() {
          that.setState({
            runUserId: user.secure_user_id,
            runUserDisplay: user.display_name,
            orderUserId: '',
            orderUserDisplay: ''
          })} }>
          Runs</td>
        <td className="link" onClick={ function() {
          that.setState({
            orderUserId: user.secure_user_id, 
            orderUserDisplay: user.display_name,
            runUserId: '',
            runUserDisplay: ''
          })} }>
          Orders</td>
      </tr>
    );
  },


  render: function() {
    var userRows = this.state.users.map(this.renderUser);

    if (userRows.length) {
      var userTable = (
        <div>
          <h3>Found Users:</h3>
          <table cellSpacing="0">
            <thead>
              <tr>
                <th>User ID</th>
                <th>Display Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email Address</th>
                <th>Phone Number</th>
                <th>Region</th>
                <th>Hashed User ID</th>
                <th>Runs</th>
                <th>Orders</th>
              </tr>
            </thead>
            <tbody>
              { userRows }
            </tbody>
          </table>
        </div>);
    }

    return ( <section className="user-search">
      <h1>Search for Users</h1>
      <p>Use this form to search for user details and find runs and orders for the given user</p>

      <div className="form">
        <label>First Name:</label>
        <input onKeyUp={ this.detectEnter } type="text" onChange={ this.setFirstName } name="first-name" className="focus-me" />
        <br />
        <label>Last Name:</label>
        <input onKeyUp={ this.detectEnter } type="text" onChange={ this.setLastName } name="last-name" />
        <br />
        <div className="button" onClick={ this.searchUsers }>Search User</div>
      </div>
      <br />
      <br />
      { userTable }
      <br />
      <br />
      <UserOrders userId={ this.state.orderUserId } displayName={ this.state.orderUserDisplay } />
      <UserRuns userId={ this.state.runUserId } displayName={ this.state.runUserDisplay } />
    </section>);
  },


  setFirstName: function(evt) {
    this.setState({firstName: evt.target.value});
  },


  setLastName: function(evt) {
    this.setState({lastName: evt.target.value});
  },


  detectEnter: function(evt) {
    evt = evt || window.event;
    if (evt.keyCode === 13) {
      this.searchUsers();
      return false;
    }
  },


  searchUsers: function() {
    if (!this.state.firstName && !this.state.lastName) {
      return App.warn('You must ender a first name and/or last name to search.');
    }

    var path = 'support/users?';
    
    if (this.state.firstName) {
      path += ("fname=" + this.state.firstName);

      if (this.state.lastName) {
        path += '&' + ("lname=" + this.state.lastName);
      }
    }
    else if (this.state.lastName) {
      path += ("lname=" + this.state.lastName);
    }

    this.fetch(path);
    
    // Hide the tables when searching a new user
    this.setState({
      orderUserId: '',
      orderUserDisplay: '',
      runUserId: '',
      runUserDisplay: ''
    })
  },
});

module.exports = Users;
