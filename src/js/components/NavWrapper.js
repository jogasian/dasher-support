var Header = require('./Header');
var Messaging = require('./Messaging');
var Dashboard = require('./Dashboard');
var Refund = require('./Refund');
var Resolution = require('./Resolution');
var Users = require('./Users');
var Promotion = require('./Promotion');

var NavWrapper = React.createClass({
	getInitialState: function() {
		return {
			appState: 'dashboard',
		}
	},

	menuOptions: [
    'dashboard',
    'users',
    'refunds',
    'resolution',
    'promotions'
  ],

	showView: function(view) {
		this.setState({appState: view});
	},

	renderView: function() {
		var view;
		switch(this.state.appState) {
			case 'dashboard':
				view = <Dashboard />;
				break;
			case 'users':
				view = <Users />;
				break;
			case 'refunds':
				view = <Refund />;
				break;
			case 'resolution':
				view = <Resolution />;
				break;
			case 'promotions':
				view = <Promotion />;
				break;
		}
		return view;
	},

  render: function() {
  	var header = 
  		<div><Header menuOptions={ this.menuOptions } changeView={this.showView} />
	  	<Messaging /></div>;
    return (
      <div className="wrapper">
    		{ header }      	
        { this.renderView() }
      </div>
    )
  }
});

module.exports = NavWrapper;
