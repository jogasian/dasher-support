var NavWrapper = require('./components/NavWrapper');

sessionStorage.env = process.env.env;
sessionStorage.api = process.env.api;
sessionStorage.menuapi = process.env.menuapi;
sessionStorage.menuviewapi=process.env.menuviewapi;

var e = sessionStorage.env ? sessionStorage.env : 'prod';
var u = sessionStorage.user? JSON.parse(sessionStorage.user) : {};
window.App = {
	environment: e,
	user: u,

	api: function() {
		switch (window.App.environment) {
			case 'dev2':
				return 'dev2clientapi/';
			case 'stage':
				return 'stageclientapi/';
			case 'local':
				return 'localclientapi/';
			default:
				return 'prodclientapi/';
		}
	},

	menuapi: function() {
		switch (window.App.environment) {
			case 'dev2':
				return 'menudev2api/';
			case 'stage':
				return 'menustageapi/';
			case 'local':
				return 'menulocalapi/';
			default:
				return 'menuprodapi/';
		}
	},

	utcToLocal: function(utcTime) {
		if (!utcTime.match(/Z$/)) {
			utcTime = utcTime + ' Z';
		}
		var d = new Date(utcTime);
		return d.toString().replace(/\sGMT-\d\d\d\d/, '');
	},

	auth: function() {
		return window.App.user.dasher_user_id !== undefined;
	},

	notify: function() {
		// Overload this function
	},

	warn: function() {
		// Overload this function
	},

	closeMessage: function() {
		// Overload this funciton
	}
};

React.render(<NavWrapper />, document.getElementById('app'));